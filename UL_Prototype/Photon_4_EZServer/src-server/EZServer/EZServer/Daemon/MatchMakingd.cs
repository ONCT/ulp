﻿using ExitGames.Concurrency.Fibers;
using Photon.SocketServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZServer
{
    class MatchMackingd
    {
        private const byte FUNCTION_POINTER = 99;
        private const byte START_MATCHING = 1;
        private const byte CANCEL_MATCHING = 2;
        PoolFiber poolFiber;

        public static MatchMackingd Instance
        {
            get
            {
                if (m_instance == null)
                {
                    m_instance = new MatchMackingd();
                }
                return m_instance;
            }
        }
        private static MatchMackingd m_instance;


        private MatchMackingd()
        {
            Initial();    
        }

        private void Initial()
        {
            poolFiber = new PoolFiber();
        }

        public void Main(ServerPlayerData player, OperationRequest operationRequest)
        {
            object tmp = operationRequest.Parameters[FUNCTION_POINTER];
            Dictionary<byte, object> args = new Dictionary<byte, object>();
            foreach (KeyValuePair<byte, object> kvp in operationRequest.Parameters)
            {
                args.Add(kvp.Key, kvp.Value);
            }
            args.Remove(FUNCTION_POINTER);
            byte fun_pointer = Convert.ToByte(tmp);

            switch (fun_pointer)
            {
                case START_MATCHING:
                    StartMatchMaking(player, args);
                    break;
                case CANCEL_MATCHING:
                    CancelMatchMaking(player, args);
                    break;
                default:
                    break;
            }
        }

        void StartMatchMaking(ServerPlayerData player, Dictionary<byte, object> args)
        {

        }

        void CancelMatchMaking(ServerPlayerData player, Dictionary<byte, object> args)
        {

        }
    }
}
