﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Photon.SocketServer;
using System.Threading;

namespace EZServer
{
    class Battled
    {
        public static Battled Instance
        {
            get
            {
                if (m_instance == null) { m_instance = new Battled(); }
                return m_instance;
            }
        }
        private static Battled m_instance;


        private const byte FUNCTION_POINTER = 99;
        private const byte START = 1;
        private const byte COST_MOV_CARD = 2;
        private const byte COST_ATK_CARD = 3;
        private const byte COST_DEF_CARD = 4;
        private const byte MOVE_TYPE = 11;
        private const byte CREATE_ROOM = 51;
        //private const byte 
        private const byte TEST = 255;

        private Battled()
        {
            DataBase DB = DBLoader.LoadGameDB("cardData.csv");
        }


        //訊息進來先到這裡，再轉發給其他function
        public void Main(ServerPlayerData player, OperationRequest operationRequest)
        {
            object tmp = operationRequest.Parameters[FUNCTION_POINTER];
            Dictionary<byte, object> args = new Dictionary<byte, object>();
            foreach (KeyValuePair<byte, object> kvp in operationRequest.Parameters)
            {
                args.Add(kvp.Key, kvp.Value);
            }

            args.Remove(FUNCTION_POINTER);
            byte fun_pointer = Convert.ToByte(tmp);

            switch (fun_pointer)
            {
                case START:
                    Start(player, args);
                    break;
                case COST_MOV_CARD:
                    Cost_MOV_card(player, args);
                    break;
                case COST_ATK_CARD:
                    Cost_ATK_card(player, args);
                    break;
                case COST_DEF_CARD:
                    Cost_DEF_card(player, args);
                    break;
                case CREATE_ROOM:
                    CreateRoom(player, args);
                    break;
                case TEST:
                    test(player, args);
                    break;
                default:
                    break;
            }
        }

        public void CreateRoom(ServerPlayerData player, Dictionary<byte, object> args)
        {
            Battle_room room = new Battle_room();
            player.battle_room = room;
        }

        public void Start(ServerPlayerData player, Dictionary<byte, object> args)
        {
            //List<PlayerData> all_player = new List<PlayerData>();
            //PlayerData oppo = null;
            //Dictionary<byte, object> parameter = new Dictionary<byte, object>();
            //OperationResponse response = new OperationResponse(Protocol_Definition.BATTLE);


            //foreach (KeyValuePair<Guid, PlayerData> kvp in PlayerManager.Instance.playerList)
            //{
            //    all_player.Add(kvp.Value);
            //}

            //int i = 0;
            //while (true)
            //{
            //    Random rnd = new Random();
            //    int rng_num = rnd.Next(0, all_player.Count);

            //    if (all_player[rng_num] != player)
            //    {
            //        oppo = all_player[rng_num];
            //        break;
            //    }
            //    else if (i > 1000)
            //    {
            //        parameter.Add(9, "can't find player");
            //        response.SetParameters(parameter);
            //        Protocol_Daemons.SendMessage(player, response);
            //        break;
            //    }
            //    i++;
            //}

            //Battle_room battle_room = new Battle_room(player, oppo);
            //player.battle_room = battle_room;
            CreateRoom(player,args);
            
            player.battle_room.AddPlayer(player,player);
            player.battle_room.Start();
        }

        public void Cost_MOV_card(ServerPlayerData player, Dictionary<byte, object> args)
        {
            if (player.battle_room == null) { return; }
            player.battle_room.Cost_MOV_card(player, args);
        }

        public void Cost_ATK_card(ServerPlayerData player, Dictionary<byte, object> args)
        {
            if (player.battle_room == null) { return; }
            player.battle_room.Cost_ATK_card(player, args);
        }

        public void Cost_DEF_card(ServerPlayerData player, Dictionary<byte, object> args)
        {
            if (player.battle_room == null) { return; }
            player.battle_room.Cost_DEF_card(player, args);
        }

        public void test(ServerPlayerData player, Dictionary<byte, object> args)
        {
            var parameter = args;
            OperationResponse response = new OperationResponse(Protocol_Definition.BATTLE);
            response.SetParameters(parameter);
            Protocol_Daemons.SendMessage(player, response);
        }
    }
}
