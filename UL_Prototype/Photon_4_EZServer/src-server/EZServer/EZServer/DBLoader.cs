﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace EZServer
{
    class DBLoader
    {
        private readonly static string GAMEDB_PATH = "/GameDB";
        private readonly static string SERVER_PATH = "C:/Users/Funfia/Desktop/Photon_4_EZServer/src-server/EZServer/EZServer";

        public static DataBase LoadGameDB(string DB_Name)
        {
            Dictionary<string, Dictionary<string, object>> data = new Dictionary<string, Dictionary<string, object>>();

            string filePath = Path.Combine(SERVER_PATH + GAMEDB_PATH, DB_Name);
            if (File.Exists(filePath))
            {
                string fileData = File.ReadAllText(filePath);
                string[] lines = fileData.Split("\n"[0]);
                string[] row_keys = lines[0].Split(","[0]);
                string[] row_variable = lines[1].Split(","[0]);

                //在Load CSV的時候，每行最後一個字會被加上一個換行符號，必須去掉
                row_keys[row_keys.Length - 1] = row_keys[row_keys.Length - 1].TrimEnd();

                for (int j = 2; j < lines.Length; j++)
                {
                    string[] row_value = lines[j].Split(","[0]);
                    if (row_value == null || row_value.Length < row_keys.Length) { continue; }
                    Dictionary<string, object> tmp_dic = new Dictionary<string, object>();
                    for (int i = 1; i < row_keys.Length; i++)
                    {
                        if (row_value[i] == null) { continue; }

                        //在Load CSV的時候，每行最後一個字會被加上一個換行符號，必須去掉
                        if (i == row_keys.Length - 1)
                        {
                            row_variable[i] = row_variable[i].TrimEnd();
                            row_value[i] = row_value[i].TrimEnd();
                        }

                        switch (row_variable[i])
                        {
                            case "int":
                                tmp_dic.Add(row_keys[i], int.Parse(row_value[i]));
                                break;
                            case "string":
                                tmp_dic.Add(row_keys[i], row_value[i]);
                                break;
                            default:
                                break;
                        }
                    }
                    data.Add(row_value[0], tmp_dic);
                }
            }
            else
            {

            }
            DataBase DB = new DataBase(data);

            return DB;
        }


    }
}
