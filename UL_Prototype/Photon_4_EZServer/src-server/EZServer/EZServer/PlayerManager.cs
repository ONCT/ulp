﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZServer
{
    public class PlayerManager
    {
        private static PlayerManager m_instance;
        public static PlayerManager Instance {
            get {
                if (m_instance == null)
                {
                    m_instance = new PlayerManager();
                }
                return m_instance;
                }
            }
        public Dictionary<Guid, ServerPlayerData> playerList;

         public void Initial()
        {
            //m_instance = this;
            playerList = new Dictionary<Guid, ServerPlayerData>();
        }

        public ServerPlayerData AddPlayer(Guid guid, EZServerPeer peer)
        {
            ServerPlayerData playerData = new ServerPlayerData();
            playerData.peer = peer;
            playerData.guid = guid;
            playerList.Add(guid, playerData);

            return playerData;
        }

        public void RemovePlayer(Guid guid)
        {
            playerList.Remove(guid);
        }
    }
}
