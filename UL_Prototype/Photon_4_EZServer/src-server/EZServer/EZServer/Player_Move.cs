﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Photon.SocketServer;

namespace EZServer
{
    static class Player_Move
    {
        public static void Main(ServerPlayerData playerData, OperationRequest operationRequest, SendParameters sendParameters)
        {
            OperationResponse response = new OperationResponse(operationRequest.OperationCode) { ReturnCode = (short)2, DebugMessage = "Login Fail" };
            Protocol_Daemons.SendMessage(playerData, response);
        }
    }
}
