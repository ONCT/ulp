﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EZServer.DBDataBase;

namespace EZServer
{
    public enum FunType
    {
        sword,
        gun,
        shield,
        move,
        special,
        none,
        total,
    }

    public enum CardType
    {
        normalCard,
        eventCard,
    }

    public class CardData
    {
        public enum Card_Direction
        {
            top,
            bot,
        }

        public string args;
        public int card_index;
        public Card_Direction card_Direction;
        public FunType top_funType;
        public int top_Num;
        public FunType bot_funType;
        public int bot_Num;
        public CardType cardType;

        public FunType now_funType;
        public int now_Num;
        DataBase DB_CardData;

        public void Initial(string args)
        {
            this.args = args;
            DB_CardData = DataBaseManager.Instance.DB_CardData;
            string top_str = DB_CardData.GetValueByString(args, "Top_Fun");
            top_funType = (FunType)Enum.Parse(typeof(FunType), top_str);
            top_Num = DB_CardData.GetValueByInt(args, "Top_Num");
            string bot_str = DB_CardData.GetValueByString(args, "Bot_Fun");
            bot_funType = (FunType)Enum.Parse(typeof(FunType), bot_str);
            bot_Num = DB_CardData.GetValueByInt(args, "Bot_Num");

            now_funType = top_funType;
            now_Num = top_Num;
        }

    }


}
