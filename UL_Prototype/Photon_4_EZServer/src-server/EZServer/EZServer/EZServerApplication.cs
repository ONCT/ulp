﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Photon.SocketServer;
using EZServer.DBDataBase;

namespace EZServer
{
    public class EZServerApplication : ApplicationBase
    {
        private static EZServerApplication m_intance;
        public static EZServerApplication Instance
        {
            get
            {
                return m_intance;
            }
        }

        protected override PeerBase CreatePeer(InitRequest initRequest)
        {
            // 建立連線並回傳給Photon Server
            return new EZServerPeer(initRequest);
        }

        protected override void Setup()
        {
            // 初始化GameServer
            m_intance = this;
            PlayerManager.Instance.Initial();
            DataBaseManager.Instance.Initiai();
        }

        protected override void TearDown()
        {
            // 關閉GameServer並釋放資源
        }
    }
}
