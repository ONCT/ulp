﻿using Photon.SocketServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZServer
{
    abstract class Daemon
    {

        public abstract void Main(ServerPlayerData player, OperationRequest operationRequest);
        
    }
}
