﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Photon.SocketServer;
using PhotonHostRuntimeInterfaces;
using ExitGames.Logging;
using ExitGames.Concurrency.Fibers;
using EZServer.DBDataBase;

namespace EZServer
{
    public class EZServerPeer : ClientPeer
    {
        public readonly IFiber fiber;

        Guid guid;
        ServerPlayerData playerdata;
        #region 建構與解構式
        //當玩家連進Server後做的事
        public EZServerPeer(InitRequest initRequest) : base(initRequest)
        {
            this.fiber = new PoolFiber();
            this.fiber.Start();
            guid = Guid.NewGuid();
            playerdata = PlayerManager.Instance.AddPlayer(guid, this);
        }

        #endregion

        protected override void OnDisconnect(PhotonHostRuntimeInterfaces.DisconnectReason reasonCode, string reasonDetail)
        {
            // 失去連線時要處理的事項，例如釋放資源
            PlayerManager.Instance.RemovePlayer(guid);
        }

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters)
        {
            //將收到的訊息轉給deamons統一處理
            //Protocol_Daemons.OnReciveMessage(playerdata, operationRequest, sendParameters);

            //ULPDataBase.Main();
            //EZServer.DBDataBase.DataBaseManager.Instance.Initiai();
            Protocol_Daemons.OnReviceMessage(playerdata, operationRequest);
            OnSendMessage(operationRequest, sendParameters);
            #region ---old_version---
            // 取得Client端傳過來的要求並加以處理
            /*
            switch (operationRequest.OperationCode)
            {
                case 5:
                    {
                        if (operationRequest.Parameters.Count < 2) // 若參數小於2則返回錯誤
                        {
                            // 返回登入錯誤
                            OperationResponse response = new OperationResponse(operationRequest.OperationCode) { ReturnCode = (short)2, DebugMessage = "Login Fail" };
                            SendOperationResponse(response, new SendParameters());
                        }
                        else
                        {
                            var memberID = (string)operationRequest.Parameters[1];
                            var memberPW = (string)operationRequest.Parameters[2];

                            if (memberID == "test" && memberPW == "1234")
                            {
                                int Ret = 1;
                                var parameter = new Dictionary<byte, object> {
                                              { 80, Ret }, {1, memberID}, {2, memberPW}, {3, "Kyoku"} // 80代表回傳值, 3代表暱稱
                                           };


                                OperationResponse response = new OperationResponse(operationRequest.OperationCode, parameter) { ReturnCode = (short)0, DebugMessage = "" };
                                SendOperationResponse(response, new SendParameters());

                            }
                            else
                            {
                                OperationResponse response = new OperationResponse(operationRequest.OperationCode) { ReturnCode = (short)1, DebugMessage = "Wrong id or password" };
                                SendOperationResponse(response, new SendParameters());
                            }

                        }

                        break;
                    }
                case 1:
                    {
                        var parameter = new Dictionary<byte, object> { { 1, guid.ToString() } };
                        OperationResponse response = new OperationResponse(operationRequest.OperationCode, parameter);
                        SendOperationResponse(response, new SendParameters());

                        int Ret = 1;
                        parameter = new Dictionary<byte, object> {
                                              { 80, Ret }, {1, "test1"}, {2, ""}, {3, "Kyoku"} // 80代表回傳值, 3代表暱稱
                                           };

                        SendOperationResponse(response, new SendParameters());

                        var eventData = new EventData(1, new Dictionary<byte, object> { { 1, guid.ToString() } });
                        OnBroadcastMessage(eventData, sendParameters);
                        break;
                    }
            }
            */
            #endregion
        }

        public void OnSendMessage(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var parameter = new Dictionary<byte, object> { { 1, guid.ToString() } };
            OperationResponse response = new OperationResponse(operationRequest.OperationCode, parameter);
            SendOperationResponse(response, new SendParameters());

        }

        private void OnBroadcastMessage(EventData eventData, SendParameters sendParameters)
        {
            foreach (KeyValuePair<Guid, ServerPlayerData> kvp in PlayerManager.Instance.playerList)
            {
                kvp.Value.peer.SendEvent(eventData, sendParameters);
            }
        }
    }
}
