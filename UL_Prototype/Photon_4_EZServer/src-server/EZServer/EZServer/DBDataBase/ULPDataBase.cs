﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using ExitGames.Logging.Log4Net;
using log4net;
using System.IO;

namespace EZServer.DBDataBase
{
    class ULPDataBase
    {

        public static void Main()
        {
            string dbHost = "127.0.0.1";
            string dbPort = "3307";
            string dbUser = "root";
            string dbPass = "";
            string dbName = "test";

            string connStr = string.Format("server={0};port={1};uid={2};pwd={3};database={4}", dbHost, dbPort, dbUser, dbPass, dbName);

            MySqlConnection conn = new MySqlConnection(connStr);
            MySqlCommand command = conn.CreateCommand();
            //conn.Open();

            // 連線到資料庫 
            try
            {
                conn.Open();
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        Console.WriteLine("無法連線到資料庫.");
                        break;
                    case 1045:
                        Console.WriteLine("使用者帳號或密碼錯誤,請再試一次.");
                        break;
                }
            }

            #region ---nonused---
#if false
            // 進行select 
            string SQL = "select plain from yammer order by id desc limit 0,10 ";
            try
            {
                MySqlCommand cmd = new MySqlCommand(SQL, conn);
                MySqlDataReader myData = cmd.ExecuteReader();

                if (!myData.HasRows)
                {
                    // 如果沒有資料,顯示沒有資料的訊息 
                    Console.WriteLine("No data.");
                }
                else
                {
                    // 讀取資料並且顯示出來 
                    while (myData.Read())
                    {
                        Console.WriteLine("Text={0}", myData.GetString(0));
                    }
                    myData.Close();
                }
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.WriteLine("Error " + ex.Number + " : " + ex.Message);
            }
#endif
            #endregion
            //MySqlCommand cmd = conn.CreateCommand();
            string sql = @"INSERT INTO `table1` (`value`, `name`) VALUES
                           ('B', '2'),
                           ('C', '3'),
                           ('D', '4'),
                           ('E', '5'),
                           ('F', '6'),
                           ('G', '7'),
                           ('H', '8'),
                           ('I', '9'),
                           ('J', '0'),
                           ('K', '11');
";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            int index = cmd.ExecuteNonQuery();

            Console.WriteLine("Success");
        }
    }
}
