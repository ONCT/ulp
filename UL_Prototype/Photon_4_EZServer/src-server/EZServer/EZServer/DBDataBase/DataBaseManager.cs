﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZServer.DBDataBase
{
    class DataBaseManager
    {
        public static DataBaseManager Instance
        {
            get
            {
                if (m_instance == null) { m_instance = new DataBaseManager(); }
                return m_instance;
            }
        }
        private static DataBaseManager m_instance;

        public DataBase DB_CardData;
        public DataBase DB_EventCardData;
        public DataBase DB_CharcterData;
        public DataBase DB_String;
        public DataBase DB_Skill;


        public void Initiai()
        {
            //m_instance = this;
            DB_CardData = GameDBLoader.LoadGameDB("DB_CardData");
            DB_EventCardData = GameDBLoader.LoadGameDB("DB_EventCardData");
            DB_CharcterData = GameDBLoader.LoadGameDB("DB_CharacterData");
            DB_Skill = GameDBLoader.LoadGameDB("DB_SkillData");
            //DB_String = GameDBLoader.LoadGameDB("DB_String");
        }

    }
}
