﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZServer
{
    public enum CardArea
    {
        None,
        Hand,
        Attack,
        Defense,
        Move,
    }

    public enum RangeType
    {
        shortRange = 1,
        middleRange = 2,
        longRange = 3,
        none,
        all,
        s_m,
        s_l,
        m_l
    }

    class GameDefinitions
    {

    }
}
