﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EZServer.GameDefinitions;

namespace EZServer.SkillRelated
{
    public class SkillEffect
    {
        CardArea AreaCondition;
        public Character caster;
        protected Character my;
        protected Character enemy;
        public List<CardData> cards;
        public Character hitter;
        protected List<Character> my_chars = new List<Character>();
        protected List<Character> enemy_chars = new List<Character>();
        protected int num;

        public virtual SkillEffect Clone()
        {
            return (SkillEffect)this.MemberwiseClone();
        }


        public virtual int CheckAddition(SkillDetail skillDetail)
        {
            my = caster;
            //enemy = GameCore.Instance.my_playerData.enemy_character;
            cards = skillDetail.atk_cards;
            caster = skillDetail.caster;
            //hitter = skillDetail.hitter;
            num = skillDetail.damage;
            return 0;
        }

        public virtual void CheckEffect(SkillDetail skillDetail)
        {
            cards = skillDetail.atk_cards;
            if (caster == null)
            {
                caster = skillDetail.caster;
            }
            num = skillDetail.damage;
            enemy_chars = new List<Character>();
            my_chars = new List<Character>();
            /*
            if (skillDetail.hitter.ownerIndex == GameCore.Instance.playerManager.my_index)
            {
                my = caster;
                enemy = GameCore.Instance.my_playerData.my_character;
                List<Character> tmp = GameCore.Instance.my_playerData.my_charList;
                for (int i = 0; i < tmp.Count; i++)
                {
                    if (tmp[i].Now_HP > 0) { enemy_chars.Add(tmp[i]); }
                }
                tmp = GameCore.Instance.my_playerData.enemy_charList;
                for (int i = 0; i < tmp.Count; i++)
                {
                    if (tmp[i].Now_HP > 0) { my_chars.Add(tmp[i]); }
                }
            }
            else
            {
                my = caster;
                enemy = GameCore.Instance.my_playerData.enemy_character;
                List<Character> tmp = GameCore.Instance.my_playerData.enemy_charList;
                for (int i = 0; i < tmp.Count; i++)
                {
                    if (tmp[i].Now_HP > 0) { enemy_chars.Add(tmp[i]); }
                }
                tmp = GameCore.Instance.my_playerData.my_charList;
                for (int i = 0; i < tmp.Count; i++)
                {
                    if (tmp[i].Now_HP > 0) { my_chars.Add(tmp[i]); }
                }
            }
            */
        }
    }

}
