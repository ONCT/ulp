﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EZServer.SkillRelated
{
    class SkillManager
    {
        private static SkillManager m_instance;
        public static SkillManager Instance
        {
            get
            {
                if (m_instance == null)
                {
                    m_instance = new SkillManager();
                }
                return m_instance;
            }
        }

        List<SkillEffect> all_skill = new List<SkillEffect>();

        // Use this for initialization
        public void Initial()
        {
            var v = GetposterityClasses<SkillEffect>();

            all_skill.AddRange(v);
        }

        public SkillEffect GetSkill(string skill_name)
        {
            SkillEffect skill = all_skill.Find(x => x.GetType().Name == skill_name);

            SkillEffect ret = skill.Clone();
            return ret;
        }

        public static IEnumerable<T> GetposterityClasses<T>(params object[] constructorArgs)
        {
            List<T> objects = new List<T>();

            var ass = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(o => o.GetTypes())
                .Where(o => typeof(T).IsAssignableFrom(o));

            foreach (Type type in ass
                .Where(o => o.IsClass && !o.IsAbstract && o.IsSubclassOf(typeof(T))))
            {
                objects.Add((T)Activator.CreateInstance(type, constructorArgs));
            }

            return objects;
        }

    }
}
