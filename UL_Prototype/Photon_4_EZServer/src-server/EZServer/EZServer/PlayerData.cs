﻿using EZServer.SkillRelated;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static EZServer.GameDefinitions;

namespace EZServer
{
    public class ServerPlayerData
    {
        public Guid guid;
        public EZServerPeer peer;
        public CharacterDeck current_deck;
        public Battle_room battle_room;
        public BattlePlayerData battlePlayer;
    }

    public class BattlePlayerData
    {
        public int My_index;
        public List<Character> my_charList = new List<Character>();
        public Character my_character;
    }

    public class Character
    {
        public bool isOnBoard;
        public int ownerIndex;
        public string hero_name;
        public int char_index;
        public int Max_HP;
        public int Now_HP;
        public int ATK;
        public int DEF;

        public bool isSeal()
        {
            foreach (SkillBuff SB in all_buffs)
            {
                if (SB.effectType == EffectType.Seal)
                {
                    return true;
                }
            }
            return false;
        }
        public List<SkillData> all_skill = new List<SkillData>();
        public List<SkillBuff> all_buffs = new List<SkillBuff>();
        
        public void CheckBuffCount()
        {
            List<SkillBuff> tmp = all_buffs;
            for (int i = 0; i < tmp.Count; i++)
            {
                switch (tmp[i].effectType)
                {
                    case EffectType.Poison:
                        //GameCore.Instance.battleManager.Add_DirectDamage(this, tmp[i].effectNum);
                        break;
                }
                tmp[i].remainCount--;
                if (tmp[i].remainCount <= 0)
                {
                    all_buffs.RemoveAt(i);
                }
            }
            //GameCore.Instance.UI_manager.UI_stateBar.ResetBuff();
        }

        public int GetATK()
        {
            int tmp = 0;
            foreach (SkillBuff SB in all_buffs)
            {
                switch (SB.effectType)
                {
                    case EffectType.Atk_up:
                        tmp += SB.effectNum;
                        break;
                    case EffectType.Atk_down:
                        tmp -= SB.effectNum;
                        break;
                    default:
                        break;
                }
            }
            return ATK + tmp;
        }

        public int GetDEF()
        {
            int tmp = 0;
            foreach (SkillBuff SB in all_buffs)
            {
                switch (SB.effectType)
                {
                    case EffectType.Def_up:
                        tmp += SB.effectNum;
                        break;
                    case EffectType.Def_down:
                        tmp -= SB.effectNum;
                        break;
                    default:
                        break;
                }
            }
            return DEF + tmp;
        }

        public int GetMOV()
        {
            int tmp = 0;
            foreach (SkillBuff SB in all_buffs)
            {
                switch (SB.effectType)
                {
                    case EffectType.Move_up:
                        tmp += SB.effectNum;
                        break;
                    case EffectType.Move_down:
                        tmp -= SB.effectNum;
                        break;
                    default:
                        break;
                }
            }
            return tmp;
        }

        public int GetSkillBouns(Character character, List<CardData> cards, CardArea cardArea)
        {
            if (isSeal()) { return 0; }

            int tmp = 0;
            SkillDetail SD = new SkillDetail();
            SD.cardArea = cardArea;
            SD.atk_cards = cards;
            SD.caster = character;
            //SD.range = range;

            for (int i = 0; i < all_skill.Count; i++)
            {
                if (all_skill[i].isSkillSuccess(SD))
                {
                    tmp += all_skill[i].GetResult(SD);
                }
            }
            return tmp;
        }

    }
}
