﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Photon.SocketServer;
using ExitGames.Logging;
using System.IO;

namespace EZServer
{
    class Protocol_Daemons
    {
        #region ---暫時沒啥用---
        /*
        public static void OnReciveMessage(PlayerData playerData, OperationRequest operationRequest, SendParameters sendParameters)
        {
            Action<PlayerData, OperationRequest, SendParameters> fun = null;
            switch (operationRequest.OperationCode)
            {
                case Protocol_Definition.MOVE:
                    fun += Player_Move.Main;
                    break;
            }

            fun(playerData, operationRequest, sendParameters);
        }
        */
        #endregion

        //訊息統一轉到這邊來，從這邊轉給各個Function
        public static void OnReviceMessage(ServerPlayerData playerData, OperationRequest operationRequest)
        {
            Action<ServerPlayerData, OperationRequest> fun = null;
            switch (operationRequest.OperationCode)
            {
                case Protocol_Definition.BATTLE:
                    fun += Battled.Instance.Main;
                    break;
                case Protocol_Definition.MATCHMAKING:
                    fun += MatchMackingd.Instance.Main;
                    break;
                case Protocol_Definition.LOGIN:
                    fun += LoginManagerd.Main;
                    break;
                default:
                    break;
            }


            if (fun != null)
            {
                fun(playerData, operationRequest);
            }
        }

        //訊息從這邊發出去，主要是為了區分player
        public static void SendMessage(ServerPlayerData playerData, OperationResponse response)
        {
            if (playerData == null || response == null) { return; }
            playerData.peer.SendOperationResponse(response, new SendParameters());
            
        }
    }
}
