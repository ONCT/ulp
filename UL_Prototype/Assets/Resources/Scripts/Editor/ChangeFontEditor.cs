﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class ChangeFontEditor   {
    
    [MenuItem("CustomTools/變更字體")]
    static public void ChangeFont()
    {
        object[] obj = GameObject.FindSceneObjectsOfType(typeof(GameObject));
        foreach (object o in obj)
        {
            GameObject g = (GameObject)o;
            Debug.Log(g.name);
            if (g.transform.GetComponent<Text>() != null)
            {
                g.transform.GetComponent<Text>().font = Resources.Load<Font>("Font/ADOBEFANHEITISTD-BOLD");
                
            }
        }
    }
}
