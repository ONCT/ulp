﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour//MonoSingleton<AudioManager>
{

    private static AudioManager m_instance;
    public static AudioManager Instance
    {
        get
        {
            return m_instance;
        }
    }

    public const string TAG_MUSIC = "music";
    public const string TAG_SOUND = "sound";
    public const string TAG_VOICE = "voice";

    private Dictionary<string, AudioPlayer> m_players = new Dictionary<string, AudioPlayer>();

    private void Awake()
    {
        m_instance = this;
        Load(TAG_MUSIC, "");
        Load(TAG_SOUND, "");
        Load(TAG_VOICE, "");
        DontDestroyOnLoad(this);
    }

    public void Load(string _audioTag, string _folderPath = "", float _defaultVolume = 1.0f)
    {
        if (!m_players.ContainsKey(_audioTag))
        {
            GameObject audioObj = new GameObject();
            audioObj.transform.parent = this.gameObject.transform;
            audioObj.name = _audioTag;
            m_players[_audioTag] = audioObj.AddComponent<AudioPlayer>();
        }

        m_players[_audioTag].Init(_audioTag, _folderPath, _defaultVolume);
    }

    /// <summary>
    /// 播放音樂，音樂一次只能出現一首，故須停掉原本播放的
    /// </summary>
    public void PlayMusic(string _name, bool _isLoop, float _delay = 0, Action _onFinish = null)
    {
        StopAudio(TAG_MUSIC);
        PlayAudio(TAG_MUSIC, _name, _isLoop, _delay, _onFinish);
    }

    /// <summary>
    /// 播放語音，同群組後蓋前
    /// </summary>
    public void PlayVoice(string _name, string _group)
    {
        StopAudio(TAG_VOICE, _group);
        PlayAudio(TAG_VOICE, _name, false, 0, null, _group);
    }

    /// <summary>
    /// 播放語音 by AudioClip，後蓋前
    /// </summary>
    public void PlayVoice(string _name, bool _isLoop = false, float _delay = 0, bool _isCut = false, Action _onFinish = null)
    {
        if (_isCut)
        {
            StopAudio(TAG_VOICE);
        }
        PlayAudio(TAG_VOICE, _name, _isLoop, _delay, _onFinish);
    }

    /// <summary>
    /// 播放音效 by AudioClip
    /// </summary>
    public void PlaySound(AudioClip _clip, bool _isLoop, float _delay = 0, Action _onFinish = null)
    {
        PlayAudio(TAG_SOUND, _clip, _isLoop, _delay, _onFinish);
    }

    public void PlaySound(AudioClip _clip, bool _isLoop = false, Action _onFinish = null)
    {
        PlaySound(_clip, _isLoop, 0, _onFinish);
    }

    public void PlaySound(string _name, Action _onFinish = null)
    {
        PlayAudio(TAG_SOUND, _name, false, 0, _onFinish);
    }

    public void PlaySound(string _name, bool _isLoop, Action _onFinish = null)
    {
        PlayAudio(TAG_SOUND, _name, _isLoop, 0, _onFinish);
    }


    private void PlayAudio(string _audioTag, string _name, bool _isLoop, float _delay, Action _onFinish, string _group = "")
    {
        if (!m_players.ContainsKey(_audioTag))
        {
            if (_onFinish != null)
            {
                _onFinish();
            }
            return;
        }

        m_players[_audioTag].Play(_name, _isLoop, _delay, _onFinish, _group);
    }

    private void PlayAudio(string _audioTag, AudioClip _clip, bool _isLoop, float _delay, Action _onFinish, string _group = "")
    {
        if (!m_players.ContainsKey(_audioTag))
        {
            if (_onFinish != null)
            {
                _onFinish();
            }
            return;
        }

        m_players[_audioTag].Play(_clip, _isLoop, _delay, _onFinish, _group);
    }

    public void StopSound(string _name)
    {
        if (!m_players.ContainsKey(TAG_SOUND))
        {
            return;
        }

        m_players[TAG_SOUND].StopLoop(_name);
    }

    public void StopAudio(string _audioTag, string _group = "")
    {
        if (!m_players.ContainsKey(_audioTag))
        {
            return;
        }

        m_players[_audioTag].Stop(_group);
    }

    public void StopAll()
    {
        foreach (string audioTag in m_players.Keys)
        {
            m_players[audioTag].Stop();
            m_players[audioTag].ClearGroup();
        }
    }

    public void ClearAll()
    {
        m_players.Clear();
    }

    public void SetMuteStaus(string _audioTag, bool _mute)
    {
        if (!m_players.ContainsKey(_audioTag))
        {
            return;
        }

        m_players[_audioTag].Mute = _mute;
    }

    public bool GetMuteStaus(string _audioTag)
    {
        if (!m_players.ContainsKey(_audioTag))
        {
            return false;
        }

        return m_players[_audioTag].Mute;
    }

    public void SetVolume(string _audioTag, float _volume)
    {
        if (!m_players.ContainsKey(_audioTag))
        {
            return;
        }

        m_players[_audioTag].Volume = _volume;
    }

    public float GetVolume(string _audioTag)
    {
        if (!m_players.ContainsKey(_audioTag))
        {
            return 0f;
        }

        return m_players[_audioTag].Volume;
    }
}
