﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayerPrefs
{

    private const int MUTE_STATUS_OFF = 0;
    private const int MUTE_STATUS_ON = 1;

    private const string PREF_VOLUME = "volumn";

    private const string PREF_MUTE = "mute";

    public static void SetMuteStatus(string _audioTag, bool isMute)
    {
        PlayerPrefs.SetInt(GetPrefKey(_audioTag, PREF_MUTE), isMute ? MUTE_STATUS_OFF : MUTE_STATUS_ON);
    }

    public static bool GetMuteStatus(string _audioTag)
    {
        string key = GetPrefKey(_audioTag, PREF_MUTE);
        if (PlayerPrefs.HasKey(key))
        {
            return PlayerPrefs.GetInt(key) == MUTE_STATUS_OFF;
        }
        else
        {
            // 預設不靜音
            return false;
        }
    }

    public static void SetVolume(string _audioTag, float _volume)
    {
        PlayerPrefs.SetFloat(GetPrefKey(_audioTag, PREF_VOLUME), _volume);
    }

    public static float GetVolume(string _audioTag, float _default = 1.0f)
    {
        string key = GetPrefKey(_audioTag, PREF_VOLUME);
        if (PlayerPrefs.HasKey(key))
        {
            return PlayerPrefs.GetFloat(key);
        }
        else
        {
            // 預設音量
            return _default;
        }
    }

    private static string GetPrefKey(string _audioTag, string _prefTag)
    {
        return string.Format("{0}_{1}", _audioTag, _prefTag);
    }
}