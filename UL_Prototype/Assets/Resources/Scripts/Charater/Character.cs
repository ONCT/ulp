﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public bool isOnBoard;
    public int ownerIndex;
    public string hero_name;
    public int char_index;
    public int Max_HP;
    public int Now_HP;
    public int ATK;
    public int DEF;
    public GameObject unit;
    public List<SkillData> all_skill = new List<SkillData>();
    public List<SkillBuff> all_buffs = new List<SkillBuff>();

    DataBase DB_Char;

    public void Initial(string args, int ownerIndex)
    {
        DB_Char = DataBaseManager.Instance.DB_CharcterData;
        this.hero_name = args;
        this.ownerIndex = ownerIndex;

        Max_HP = DB_Char.GetValueByInt(args, "HP");
        Now_HP = Max_HP;
        ATK = DB_Char.GetValueByInt(args, "ATK");
        DEF = DB_Char.GetValueByInt(args, "DEF");

        for (int i = 1; i <= GameDefinition.Instance.CHAR_SKILL_NUM; i++)
        {
            string skill_key = DB_Char.GetValueByString(args, "Skill_" + i);
            if (skill_key == "0") { continue; }
            SkillData skill = new SkillData(skill_key);
            all_skill.Add(skill);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isOnBoard)
        {
            Debug.Log(all_buffs);
        }
    }

    public void CheckBuffCount()
    {
        List<SkillBuff> tmp = all_buffs;
        for (int i = 0; i < tmp.Count; i++)
        {
            switch (tmp[i].effectType)
            {
                case EffectType.Poison:
                    GameCore.Instance.battleManager.Add_DirectDamage(this, tmp[i].effectNum);
                    break;
            }
            tmp[i].remainCount--;
            if (tmp[i].remainCount <= 0)
            {
                all_buffs.RemoveAt(i);
            }
        }
        //GameCore.Instance.UI_manager.UI_stateBar.ResetBuff();
    }

    public int GetATK()
    {
        int tmp = 0;
        foreach (SkillBuff SB in all_buffs)
        {
            switch (SB.effectType)
            {
                case EffectType.Atk_up:
                    tmp += SB.effectNum;
                    break;
                case EffectType.Atk_down:
                    tmp -= SB.effectNum;
                    break;
                default:
                    break;
            }
        }
        return ATK + tmp;
    }

    public int GetDEF()
    {
        int tmp = 0;
        foreach (SkillBuff SB in all_buffs)
        {
            switch (SB.effectType)
            {
                case EffectType.Def_up:
                    tmp += SB.effectNum;
                    break;
                case EffectType.Def_down:
                    tmp -= SB.effectNum;
                    break;
                default:
                    break;
            }
        }
        return DEF + tmp;
    }

    public int GetMOV()
    {
        int tmp = 0;
        foreach (SkillBuff SB in all_buffs)
        {
            switch (SB.effectType)
            {
                case EffectType.Move_up:
                    tmp += SB.effectNum;
                    break;
                case EffectType.Move_down:
                    tmp -= SB.effectNum;
                    break;
                default:
                    break;
            }
        }
        return tmp;
    }

    public void AddBuff(SkillBuff skillBuff)
    {
        all_buffs.Add(skillBuff);
        Debug.Log("Add Buff: " + skillBuff.effectType);
        GameCore.Instance.UI_manager.CheckAllArgs();
    }

    public void RemoveBuff(SkillBuff skillBuff)
    {
        all_buffs.Remove(skillBuff);
        Debug.Log("Remove Buff: " + skillBuff.effectType);
        GameCore.Instance.UI_manager.CheckAllArgs();
    }

    public bool isRoot()
    {
        foreach (SkillBuff SB in all_buffs)
        {
            if (SB.effectType == EffectType.Root)
            {
                return true;
            }
        }
        return false;
    }

    public bool isSeal()
    {
        foreach (SkillBuff SB in all_buffs)
        {
            if (SB.effectType == EffectType.Seal)
            {
                return true;
            }
        }
        return false;
    }

    public Transform trans;
    public float speed = 0.1f;
    public IEnumerator MoveToTarget(Vector3 target)
    {
        Vector3 moveStartPos = trans.position;
        while ((target - moveStartPos).sqrMagnitude > (trans.position - moveStartPos).sqrMagnitude)
        {
            Vector3 move = (target - trans.position) * (Time.deltaTime + speed);
            Vector3 min_move = (target - trans.position).normalized;
            trans.position += (move + min_move);
            yield return null;
        }
        trans.position = target;
        StopCoroutine(MoveToTarget(target));
    }

    public int GetSkillBouns(Character character, List<CardData> cards, CardArea cardArea)
    {
        if (isSeal()) { return 0; }

        int tmp = 0;
        SkillDetail SD = new SkillDetail();
        SD.cardArea = cardArea;
        SD.atk_cards = cards;
        SD.caster = character;
        SD.range = GameCore.Instance.my_playerData.range;

        for (int i = 0; i < all_skill.Count; i++)
        {
            if (all_skill[i].isSkillSuccess(SD))
            {
                tmp += all_skill[i].GetResult(SD);
            }
        }
        return tmp;
    }

    /*
    public List<SkillEffect> GetSkillEffect(Character character, List<CardData> cards, CardArea cardArea)
    {
        if (isSeal()) { return new List<SkillEffect>(); }

        SkillDetail SD = new SkillDetail();
        SD.cardArea = cardArea;
        SD.atk_cards = cards;
        SD.caster = character;
        SD.hitter = GameCore.Instance.my_playerData.enemy_character;
        SD.range = GameCore.Instance.my_playerData.range;

        List<SkillEffect> tmp = new List<SkillEffect>();
        for (int i = 0; i < all_skill.Count; i++)
        {
            if (all_skill[i].isSkillSuccess(SD))
            {
                var SE = all_skill[i].GetSkillEffect();
                SE.caster = this;
                SE.hitter = GameCore.Instance.my_playerData.enemy_character;
                tmp.Add(SE);
                Debug.Log("Skill: " + all_skill[i].skill_name + " is success in " + cardArea);
            }
        }
        return tmp;
    }
    */
    public List<SkillEffect> GetSkillEffect(List<CardData> cards, CardArea cardArea)
    {
        if (isSeal()) { return new List<SkillEffect>(); }

        SkillDetail SD = new SkillDetail();
        SD.cardArea = cardArea;
        SD.atk_cards = cards;
        SD.caster = this;
        SD.hitter = GameCore.Instance.my_playerData.enemy_character;
        SD.range = GameCore.Instance.my_playerData.range;

        List<SkillEffect> tmp = new List<SkillEffect>();
        for (int i = 0; i < all_skill.Count; i++)
        {
            if (all_skill[i].isSkillSuccess(SD))
            {
                var SE = all_skill[i].GetSkillEffect();
                SE.caster = this;
                SE.hitter = GameCore.Instance.my_playerData.enemy_character;
                tmp.Add(SE);
                Debug.Log("Skill: " + all_skill[i].skill_name + " is success in " + cardArea);
            }
        }
        return tmp;
    }
    public void GetHit(int num)
    {
        Debug.Log("Enemy Get Hit: " + num);
        if (num < 0)
        {
            num = 0;
        }
        Now_HP -= num;
        CheckHP();
    }

    public void GetDirectHit(int num)
    {
        if (num < 0) { num = 0; }
        Now_HP -= num;
        CheckHP();
    }

    public void GetHeal(int num)
    {
        Now_HP += num;
        if (Now_HP > Max_HP)
        {
            Now_HP = Max_HP;
        }
        CheckHP();
    }

    void CheckHP()
    {
        if (Now_HP <= 0 && this == GameCore.Instance.my_playerData.my_character)
        {
            GameCore.Instance.my_playerData.CharacterDie(char_index);
        }
    }
}
