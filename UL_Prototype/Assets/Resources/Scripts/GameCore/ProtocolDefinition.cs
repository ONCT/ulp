﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ProtocolDefinition {

    public const byte BATTLE = 101;
    public const byte MATCHMAKING = 151;
    public const byte LOGIN = 201;
}

public static class BattleBehaviorDefinitin
{
    public const byte FUNCTION_POINTER = 99;
    public const byte START = 1;
    public const byte USE_MOV_CARD = 2;
    //public const byte RECEIVE_MOVE = 5;
    public const byte MOVE_TYPE = 11;

    public const byte P1_USECARDS = 15;
    public const byte P2_USECARDS = 16;

    public const byte RECEIVE_MOVE = 20;
    public const byte P1_MOVESTRUCT = 21;
    public const byte P2_MOVESTRUCT = 22;
    public const byte TOTAL_MOVESTRUCT = 23;
    public const byte FIRSTER = 24;

    public const byte SEND_ATK_CARD = 31;
    public const byte RECEIVE_ATK_CARD = 32;

    public const byte SEND_DEF_CARD = 41;
    public const byte RECEIVE_DEF_CARD = 42;

    public const byte SEND_CHANGE_CHAR = 51;
    public const byte RECEIVE_CHANGE_CHAR = 52;
    public const byte P1_CHANGE_CHAR_INDEX = 53;
    public const byte P2_CHANGE_CHAR_INDEX = 54;

    public const byte CREATE_ROOM = 255;
}
