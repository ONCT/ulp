﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerController : MonoBehaviour
{

    public static ServerController Instance
    {
        get
        {
            return m_instance;
        }
    }
    private static ServerController m_instance;
    [SerializeField]
    private PhotonClient photonClient;
    [SerializeField]
    private PhotonConnect photonConnect;

    private void Awake()
    {
        m_instance = this;
    }

    public void Login()
    {
        photonClient.CallConnect();
        //photonConnect.ConnectToPhoton();
    }

    public void SendRequest(byte protocal, Dictionary<byte, object> parameter)
    {
        photonClient.SendOperationRequest(protocal, parameter);
    }

    public void ReceiveResponse(byte protocal, Dictionary<byte, object> parameter)
    {
        switch (protocal)
        {
            case ProtocolDefinition.BATTLE:
                GameCore.Instance.playerManager.Receive_Request(parameter);
                break;
            default:
                break;
        }
    }

    public void TestSendStart()
    {
        Dictionary<byte, object> param = new Dictionary<byte, object>() { { 99, 1 } };
        SendRequest(ProtocolDefinition.BATTLE, param);
    }
}

