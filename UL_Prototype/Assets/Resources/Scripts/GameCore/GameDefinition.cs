﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDefinition : MonoBehaviour
{

    public float hit_chance;
    public float HIT_CHANCE { get { return hit_chance; } }

    public int max_card_count;
    public int MAX_CARD_COUNT { get { return max_card_count; } }

    float max_position = 2.5f;
    public float MAX_POSITION { get { return max_position; } }

    float min_position = 1.0f;
    public float MIN_POSITION { get { return min_position; } }

    float near_distance = 2.0f;
    public float NEAR_DISTANCE { get { return near_distance; } }

    public float middle_distance;
    public float MIDDLE_DISTANCE { get { return middle_distance; } }

    public float long_distance;
    public float LONG_DISTANCE { get { return long_distance; } }

    public int default_card_count;
    public int DEFALUT_CARD_COUNT { get { return default_card_count; } }

    public int char_skill_num;
    public int CHAR_SKILL_NUM { get { return char_skill_num; } }

    public float PHASETIME = 30.0f;

    public UI_WizzardPanel UI_wizzardPanel;

    private static GameDefinition m_instance;
    public static GameDefinition Instance
    {
        get { return m_instance; }
    }

    private void Awake()
    {
        m_instance = this;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            UI_wizzardPanel.TurnOnOff();
        }
    }

    public void SetSkillNum(Text text)
    {
        int i = int.Parse(text.text);
        char_skill_num = i;
        GameCore.Instance.UI_manager.selectCharacterPanel.ResetPanel();
    }

    public void SetDefaultCardCount(Text text)
    {
        int i = int.Parse(text.text);
        default_card_count = i;
    }

    public void SetMaxHandCardCount(Text text)
    {
        int i = int.Parse(text.text);
        max_card_count = i;
    }
}
