﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerManager : Photon.MonoBehaviour
{

    public GameObject player_prefab;
    public GameObject my_clone;
    public Dictionary<int, PlayerData> players = new Dictionary<int, PlayerData>();
    public int players_index = 0;
    public int my_index;
    //public Action<int, float, float, float> Move_callBack = null;
    // public PlayerData my_Data;

    public void Create()
    {
        Debug.Log("Self Create: " + my_index);
        my_clone = Instantiate(player_prefab);
        my_clone.name = "player_" + my_index;

        PlayerData playerData = my_clone.GetComponent<PlayerData>();
        playerData.My_index = my_index;
        GameCore.Instance.my_playerData = playerData;
        playerData.Initial();

        players.Add(my_index, playerData);

        //photonView.RPC("CallAddPlayer", PhotonTargets.Others, my_index);

        UI_Manager.Instance.loadingPanel.Close();
    }

    [PunRPC]
    public void CallAddPlayer(int viewID)
    {
        GameObject someone_clone = Instantiate(player_prefab);
        someone_clone.name = "player" + viewID;
        someone_clone.GetComponent<PlayerData>().My_index = viewID;

        players.Add(viewID, someone_clone.GetComponent<PlayerData>());
        GameCore.Instance.enemy_playerData = someone_clone.GetComponent<PlayerData>();
    }

    public void GetAllPlayers()
    {
        //photonView.RPC("SendAllPlayers", PhotonTargets.MasterClient);
    }

    [PunRPC]
    public void SendAllPlayers()
    {
        //photonView.RPC("ReciveAllPlayers", PhotonTargets.All, players.Count);
    }

    [PunRPC]
    public void ReciveAllPlayers(int count)
    {
        for (int i = 0; i < count; i++)
        {
            Debug.Log("Create already exist:" + i);
            if (players.ContainsKey(i)) { continue; }
            GameObject someone_clone = Instantiate(player_prefab);
            someone_clone.name = "player" + i;
            someone_clone.GetComponent<PlayerData>().My_index = i;

            players.Add(i, someone_clone.GetComponent<PlayerData>());
            GameCore.Instance.enemy_playerData = someone_clone.GetComponent<PlayerData>();
        }

        if (my_clone == null)
        {
            my_index = count;
            GameCore.Instance.myIndex = my_index;
            Create();
        }
    }

    public void Force_Start(List<string> heroes = null)
    {
        var enemy = GameCore.Instance.playerManager.CreateFakePlayer();
        //GameCore.Instance.my_playerData.Online_SetHero(1, new List<string> { "char_1", "char_2", "char_3" });
        if (heroes == null)
        {
            enemy.SetHero(new List<string> { "char_1", "char_2", "char_3" });
        }
        else { enemy.SetHero(heroes); }
        GameCore.Instance.my_playerData.enemy_character = enemy.my_character;
        GameCore.Instance.my_playerData.enemy_character.isOnBoard = true;
        GameCore.Instance.my_playerData.enemy_charList = GameCore.Instance.enemy_playerData.my_charList;
        GameCore.Instance.enemy_playerData.enemy_charList = GameCore.Instance.my_playerData.my_charList;

        enemy.enemy_character = GameCore.Instance.my_playerData.my_character;
        enemy.enemy_character.isOnBoard = true;
        enemy.My_index = 1;
        UI_Manager.Instance.UI_skillBar.SetEnSkillBar(enemy.my_character);
        //GameCore.Instance.GameStart();
        GameStart();
    }

    public void GameStart()
    {
        UI_Manager.Instance.selectCharacterPanel.Close();
        UI_Manager.Instance.mainPanel.Close();
        FakeServer.Instance.battleRoom.AddPlayers(GameCore.Instance.my_playerData, GameCore.Instance.enemy_playerData);
        GameCore.Instance.battleManager.StartGame(GameCore.Instance.isFake);
        //GameCore.Instance.phaseController.Init();
    }

    public PlayerData CreateFakePlayer()
    {
        GameCore.Instance.isFake = true;
        my_clone = Instantiate(player_prefab);
        my_clone.name = "player_" + 1;

        PlayerData playerData = my_clone.GetComponent<PlayerData>();
        playerData.My_index = 1;
        GameCore.Instance.enemy_playerData = playerData;
        playerData.Initial();

        players.Add(1, playerData);

        return playerData;
    }

    Dictionary<byte, object> parameter = new Dictionary<byte, object>();
    public void Send_DrawCard(int index)
    {
    }

    [PunRPC]
    public void Receive_DrawCard(int ownerIndex, int card_index)
    {
        GameCore.Instance.cardManager.Online_DrawCard(ownerIndex, card_index);
    }

    public void Send_ChangeCharacter(int char_index)
    {
        //photonView.RPC("Receive_ChangeCharacter", PhotonTargets.Others, my_index, char_index);
    }

    [PunRPC]
    public void Receive_ChangeCharacter(int ownerIndex, int char_index)
    {
        //GameCore.Instance.my_playerData.Online_ChangeCharacter(char_index);
        GameCore.Instance.battleManager.Receive_ChangeCharacter(ownerIndex, char_index);
    }

    public void Send_UseAttackCard(string card_args)
    {
        //photonView.RPC("Receive_UseAttackCard", PhotonTargets.Others, my_index, card_args);
        parameter = new Dictionary<byte, object>() { { BattleBehaviorDefinitin.SEND_ATK_CARD, card_args }, { BattleBehaviorDefinitin.FUNCTION_POINTER, BattleBehaviorDefinitin.SEND_ATK_CARD } };
        ServerController.Instance.SendRequest(ProtocolDefinition.BATTLE, parameter);
    }

    [PunRPC]
    public void Receive_UseAttackCard(int ownerIndex, string card_args)
    {
        Debug.Log("Receive AttackCard: " + card_args);
        List<CardData> use_card = new List<CardData>();
        use_card = GameCore.Instance.cardManager.GetCards(card_args);
        GameCore.Instance.battleManager.Receive_Attack(use_card);
    }

    public void Receive_UseAttackCard(string card_args)
    {
        Debug.Log("Receive AttackCard: " + card_args);
        List<CardData> use_card = new List<CardData>();
        use_card = GameCore.Instance.cardManager.GetCards(card_args);
        GameCore.Instance.battleManager.Receive_Attack(use_card);
    }

    public void Send_UseDefenseCard(string card_args)
    {
        //photonView.RPC("Receive_UseDefenseCard", PhotonTargets.Others, my_index, card_args);
        parameter = new Dictionary<byte, object>() { { BattleBehaviorDefinitin.SEND_DEF_CARD, card_args }, { BattleBehaviorDefinitin.FUNCTION_POINTER, BattleBehaviorDefinitin.SEND_DEF_CARD } };
        ServerController.Instance.SendRequest(ProtocolDefinition.BATTLE, parameter);
    }

    [PunRPC]
    public void Receive_UseDefenseCard(int ownerIndex, string card_args)
    {
        Debug.Log("Receive DefenseCard: " + card_args);
        List<CardData> use_card = new List<CardData>();
        use_card = GameCore.Instance.cardManager.GetCards(card_args);
        GameCore.Instance.battleManager.Receive_Defense(use_card);
    }

    public void Receive_UseDefenseCard(string card_args)
    {
        Debug.Log("Receive DefenseCard: " + card_args);
        List<CardData> use_card = new List<CardData>();
        use_card = GameCore.Instance.cardManager.GetCards(card_args);
        GameCore.Instance.battleManager.Receive_Defense(use_card);
    }

    public void Send_UseMoveCard(string card_args, int moveType)
    {
        parameter = new Dictionary<byte, object>() { { BattleBehaviorDefinitin.USE_MOV_CARD, card_args }, { BattleBehaviorDefinitin.MOVE_TYPE, moveType }, { BattleBehaviorDefinitin.FUNCTION_POINTER, BattleBehaviorDefinitin.USE_MOV_CARD } };
        ServerController.Instance.SendRequest(ProtocolDefinition.BATTLE, parameter);
    }

    [PunRPC]
    public void Receive_UseMoveCard(int ownerIndex, string card_args, int moveType)
    {
        Debug.Log("Receive MoveCard: " + card_args);
        List<CardData> use_card = new List<CardData>();
        use_card = GameCore.Instance.cardManager.GetCards(card_args);
        GameCore.Instance.battleManager.Receive_MoveCard(ownerIndex, use_card, moveType);
    }

    public void Receive_UseMoveCard(string card_args, int moveType)
    {
        Debug.Log("Receive MoveCard: " + card_args);
        List<CardData> use_card = new List<CardData>();
        use_card = GameCore.Instance.cardManager.GetCards(card_args);
        GameCore.Instance.battleManager.Receive_MoveCard(GameCore.Instance.enemy_playerData.My_index, use_card, moveType);
    }

    private void Receive_Move(Dictionary<byte, object> parameter)
    {
        Debug.Log("Receive WhoGoFirst");
        int firster = (int)parameter[BattleBehaviorDefinitin.FIRSTER];
        List<CardData> my_use_cards = null;
        List<CardData> en_use_cards = null;
        MoveStruct my_move_struct;
        MoveStruct en_move_struct;
        MoveStruct total_move_struct;
        if (GameCore.Instance.my_playerData.My_index == 0)
        {
            my_use_cards = GameCore.Instance.cardManager.GetCards((string)parameter[BattleBehaviorDefinitin.P1_USECARDS]);
            en_use_cards = GameCore.Instance.cardManager.GetCards((string)parameter[BattleBehaviorDefinitin.P2_USECARDS]);
            my_move_struct = StringToMovestruct((string)parameter[BattleBehaviorDefinitin.P1_MOVESTRUCT]);
            en_move_struct = StringToMovestruct((string)parameter[BattleBehaviorDefinitin.P2_MOVESTRUCT]);
        }
        else
        {
            my_use_cards = GameCore.Instance.cardManager.GetCards((string)parameter[BattleBehaviorDefinitin.P2_USECARDS]);
            en_use_cards = GameCore.Instance.cardManager.GetCards((string)parameter[BattleBehaviorDefinitin.P1_USECARDS]);
            my_move_struct = StringToMovestruct((string)parameter[BattleBehaviorDefinitin.P2_MOVESTRUCT]);
            en_move_struct = StringToMovestruct((string)parameter[BattleBehaviorDefinitin.P1_MOVESTRUCT]);
        }
        total_move_struct = StringToMovestruct((string)parameter[BattleBehaviorDefinitin.TOTAL_MOVESTRUCT]);

        GameCore.Instance.battleManager.Receive_Move(firster,my_move_struct, en_move_struct, total_move_struct, my_use_cards, en_use_cards);
        Debug.Log("Down Receive_Move");
    }

    private void Receive_ChangeCharacter(Dictionary<byte, object> parameter)
    {
        int my_char_index;
        int en_char_index;
        if (GameCore.Instance.my_playerData.My_index == 0)
        {
            my_char_index = (int)parameter[BattleBehaviorDefinitin.P1_CHANGE_CHAR_INDEX];
            en_char_index = (int)parameter[BattleBehaviorDefinitin.P2_CHANGE_CHAR_INDEX];
        }
        else
        {
            my_char_index = (int)parameter[BattleBehaviorDefinitin.P2_CHANGE_CHAR_INDEX];
            en_char_index = (int)parameter[BattleBehaviorDefinitin.P1_CHANGE_CHAR_INDEX];
        }

        GameCore.Instance.battleManager.Receive_ChangeCharacter(my_char_index, en_char_index);
    }

    public void Send_DealDamage(int num)
    {
        //photonView.RPC("Receive_DealDamage", PhotonTargets.Others, my_index, num);
    }

    [PunRPC]
    public void Receive_DealDamage(int ownerIndex, int num)
    {
        //GameCore.Instance.battleManager.Receive_DealDamage(ownerIndex, num);
        GameCore.Instance.battleManager.Receive_DealDamage(num);
    }

    public void Send_EventCard(string args)
    {
        //photonView.RPC("Receive_EventCard", PhotonTargets.Others, my_index, args);
    }

    [PunRPC]
    public void Receive_EventCard(int ownerIndex, string args)
    {
        GameCore.Instance.cardManager.Online_AddEventCards(args);
    }

    public void Send_AtkFirst(int firster)
    {
        //photonView.RPC("Receive_AtkFirst", PhotonTargets.All, firster);
    }

    [PunRPC]
    public void Receive_AtkFirst(int ownerIndex, int firster)
    {
        if (firster == my_index)
        {
            GameCore.Instance.phaseController.StartAttackPhase();
        }
        else
        {
            GameCore.Instance.phaseController.StartDefensePhase();
        }

    }

    public void Send_Restart()
    {
        //photonView.RPC("Receive_Restart", PhotonTargets.All, my_index);
    }

    [PunRPC]
    public void Receive_Restart(int ownerIndex)
    {
        GameCore.Instance.cardManager.ReStart();
        GameCore.Instance.UI_manager.selectCharacterPanel.TurnOnOff(true);
        GameCore.Instance.my_playerData.ReStart();
        GameCore.Instance.UI_manager.distanceBar.rectTranform.localPosition = new Vector3(2 * -100, 455, 0);
        GameCore.Instance.UI_manager.WinnerGO.SetActive(false);
        GameCore.Instance.my_playerData.SendCardToGrave();
    }

    public void Send_SelectHeroes(string str)
    {
        //photonView.RPC("Receive_SelectHeroes", PhotonTargets.Others, my_index, str);
    }

    [PunRPC]
    public void Receive_SelectHeroes(int ownerIndex, string args)
    {
        string[] heors = args.Split(' ');
        List<string> tmp = new List<string>();
        for (int i = 0; i < heors.Length; i++)
        {
            tmp.Add(heors[i]);
        }
        GameCore.Instance.my_playerData.Online_SetHero(ownerIndex, tmp);
    }

    public void Send_Lose()
    {
        //photonView.RPC("Receive_Lose", PhotonTargets.All, my_index);
    }

    [PunRPC]
    public void Receive_Lose(int loserIndex)
    {
        GameCore.Instance.UI_manager.SetLoser(loserIndex);
    }

    public void Receive_Request(Dictionary<byte, object> parameter)
    {
        //int pro = int.Parse( parameter[BattleBehaviorDefinitin.FUNCTION_POINTER], out );
        //int pro =0;
        var tried = Convert.ChangeType(parameter[BattleBehaviorDefinitin.FUNCTION_POINTER], typeof(int));
        int pro = (int)tried;
        switch (pro)
        {
            case BattleBehaviorDefinitin.RECEIVE_ATK_CARD:
                Receive_UseAttackCard((string)parameter[BattleBehaviorDefinitin.RECEIVE_ATK_CARD]);
                break;
            case BattleBehaviorDefinitin.RECEIVE_DEF_CARD:
                Receive_UseDefenseCard((string)parameter[BattleBehaviorDefinitin.RECEIVE_DEF_CARD]);
                break;
            case BattleBehaviorDefinitin.USE_MOV_CARD:
                Receive_UseMoveCard((string)parameter[BattleBehaviorDefinitin.USE_MOV_CARD], (int)parameter[BattleBehaviorDefinitin.MOVE_TYPE]);
                break;
            case BattleBehaviorDefinitin.RECEIVE_MOVE:
                Receive_Move(parameter);
                break;
            case BattleBehaviorDefinitin.RECEIVE_CHANGE_CHAR:
                Receive_ChangeCharacter(parameter);
                break;
            case BattleBehaviorDefinitin.CREATE_ROOM:
                break;
        }
    }

    public MoveStruct StringToMovestruct(string args)
    {
        string[] lines = args.Split(" "[0]);
        int num = int.Parse(lines[0]);
        MoveType moveType = (MoveType)(int.Parse(lines[1]));

        MoveStruct moveStruct = new MoveStruct()
        {
            num = num,
            moveType = moveType,
        };

        return moveStruct;
    }
}

public enum playerTag
{
    none,
    mine,
    enemy,
}
