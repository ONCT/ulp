﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseController : MonoBehaviour
{

    public BattlePhase battlePhase;

    public void StartGame()
    {
        GameCore.Instance.battleManager.battleMech.Initial();

        CallNextStep();
    }


    public void CallNextStep()
    {
        Debug.Log(string.Format("Change to <color=green>player{0}'s {1}</color>", GameCore.Instance.my_playerData.My_index, GameCore.Instance.my_playerData.phase_list[0].Method.Name));

        var tmp = GameCore.Instance.my_playerData.phase_list[0];
        GameCore.Instance.my_playerData.phase_list.RemoveAt(0);

        Debug.Log("<color=blue>" + tmp.Method.Name + "</color>");

        StartCoroutine(StartPhase(tmp));
        //tmp();
        //UI_Manager.Instance.RefreshAllUI();
    }

    IEnumerator StartPhase(Action targetAction)
    {
        //yield return new WaitForSeconds(1.0f);
        yield return null;

        targetAction();
        //UI_Manager.Instance.UI_stateBar.SetATKandDEFBar(battlePhase);
        //UI_Manager.Instance.RefreshAllUI();
    }

    public void StartPreparePhase()
    {
        Debug.Log("Start Prepare Phase");

        battlePhase = BattlePhase.Prepare;
        //shouldChange = false;
        //dealMoveCount = 0;

        GameCore.Instance.battleManager.battleMech.StartPreparePhase(StartDrawPhase);
    }

    public void StartDrawPhase()
    {
        Debug.Log("Start Draw Phase");

        battlePhase = BattlePhase.Draw;
        UI_Manager.Instance.ChangeBase(battlePhase);

        GameCore.Instance.battleManager.battleMech.StartDrawPhase();

        //StartMovePhase();
        //CallNextStep();
    }

    public void StartMovePhase()
    {
        Debug.Log("Start Move Phase");

        battlePhase = BattlePhase.Move;
        UI_Manager.Instance.ChangeBase(battlePhase);
        GameCore.Instance.battleManager.battleMech.StartMovePhase();

    }

    public void StartWaitingChangePhase()
    {
        Debug.Log("Start WaitingChange Phase");
        battlePhase = BattlePhase.WaitingChange;
        
        GameCore.Instance.battleManager.battleMech.StartWaitingChangeCharPhase();
    }

    public void StartChangePhase()
    {
        Debug.Log("Start Change Phase");

        GameCore.Instance.battleManager.battleMech.StartChangeCharPhase();

        /*
        if (GameCore.Instance.my_playerData.isChange)
        {
            GameCore.Instance.UI_manager.changeCharacterPanel.TurnOnOff(true);
        }
        else
        {
            if (GameCore.Instance.isFake)
            {
                GameCore.Instance.battleManager.ChangePlayer(0.1f);
                CallNextStep();
            }
            if (GameCore.Instance.my_playerData.isChange == false && GameCore.Instance.enemy_playerData.isChange == false)
            {
                //GameCore.Instance.battleManager.StartAttack(firster);
            }
        }
        */
    }

    public void StartAttackPhase()
    {
        Debug.Log("Start Attack Phase");

        battlePhase = BattlePhase.Attack;
        GameCore.Instance.battleManager.battleMech.StartAttackPhase();
    }

    public void StartDefensePhase()
    {
        Debug.Log("Start Defense Phase");

        battlePhase = BattlePhase.Defense;
        GameCore.Instance.battleManager.battleMech.StartDefensePhase();
    }

    public void StartEndPhase()
    {
        Debug.Log("Start End Phase");

        battlePhase = BattlePhase.End;
        GameCore.Instance.my_playerData.my_character.CheckBuffCount();

        GameCore.Instance.my_playerData.AddPhase(StartPreparePhase);

        if (GameCore.Instance.isFake)
        {
            GameCore.Instance.battleManager.ChangePlayer(0.1f);
        }
        CallNextStep();
    }
}
