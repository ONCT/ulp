﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCore : MonoBehaviour
{
    public DataBaseManager dataBaseManager;
    public UI_Manager UI_manager;
    public PlayerBehavior playerBehavior;
    public PlayerManager playerManager;
    public PlayerData my_playerData;
    public PlayerData enemy_playerData;
    public CardManager cardManager;
    public BattleManager battleManager;
    public PhaseController phaseController;
    public SkillManager skillManager;
    public int myIndex;
    public bool isFake = false;

    public static GameCore Instance
    {
        get { return m_instance; }
    }
    private static GameCore m_instance;

    private void Awake()
    {
        m_instance = this;
        dataBaseManager.Initiai();
        battleManager.Initial();
        skillManager.Initial();
    }
    // Use this for initialization
    void Start()
    {
        cardManager.Initial();
        UI_manager.Initial();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            playerManager.Send_Restart();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (UI_Manager.Instance.selectCharacterPanel.waiting_OB.activeInHierarchy == false) { return; }
            playerManager.Force_Start();
        }
    }

    public void GoWithSingle()
    {
        playerManager.Create();
        UI_manager.mainPanel.isFake = true;
    }

    //public void GameStart()
    //{
    //    UI_manager.selectCharacterPanel.TurnOnOff(false);
    //    UI_manager.mainPanel.Close();
    //    phaseController.Init();
    //}
}
