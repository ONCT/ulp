﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonConnect : MonoBehaviour
{
    private void Awake()
    {
        //ConnectToPhoton();
    }

    public string versionName = "0.1";

    public void ConnectToPhoton()
    {
        PhotonNetwork.ConnectUsingSettings(versionName);

        Debug.Log("Connection to photon...");
    }

    private void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(TypedLobby.Default);

        Debug.Log("We are connected to master");
    }

    private void OnJoinedLobby()
    {

        Debug.Log("On Joined Lobby");

        PhotonNetwork.JoinOrCreateRoom("123", new RoomOptions(), null);

    }

    private void OnJoinedRoom()
    {
        Debug.Log("On Joined Room");

        //GameCore.Instance.playerManager.Create();
        GameCore.Instance.playerManager.GetAllPlayers();
    }

    [PunRPC]
    public void Recive_Restart()
    {

    }


    private void OnFailedToConnectToPhoton()
    {

    }

    /*
    private void OnPlayerDisconnected(NetworkPlayer player)
    {
        Debug.Log("Someone Disconnected");
    }
    */
}
