﻿using UnityEngine;
using System.Collections;
using System.Security;
using System;
using System.Collections.Generic;
using ExitGames.Client.Photon;

public class PhotonClient : MonoBehaviour, IPhotonPeerListener
{

    public string ServerAddress = "localhost:5055";
    protected string ServerApplication = "EZServer";

    protected PhotonPeer peer;
    public bool ServerConnected;

    public string memberID = "";
    public string memberPW = "";

    public bool LoginStatus;
    public string getMemberID = "";
    public string getMemberPW = "";
    public string getNickname = "";
    public int getRet = 0;

    public string LoginResult = "";

    // Use this for initialization
    void Start()
    {
        //CallConnect();
    }

    public void CallConnect()
    {
        this.ServerConnected = false;
        this.LoginStatus = false;

        this.peer = new PhotonPeer(this, ConnectionProtocol.Udp);
        this.Connect();

    }

    internal virtual void Connect()
    {
        try
        {
            this.peer.Connect(this.ServerAddress, this.ServerApplication);
        }
        catch (SecurityException se)
        {
            this.DebugReturn(0, "Connection Failed. " + se.ToString());
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (this.peer != null)
        {
            this.peer.Service();
        }
    }

    public void DebugReturn(DebugLevel level, string message)
    {
        Debug.Log(message);
    }

    public void OnOperationResponse(OperationResponse operationResponse)
    {
        // display operationCode
        this.DebugReturn(0, string.Format("OperationResult:" + operationResponse.OperationCode.ToString()));

        Debug.Log(string.Format("Receive Response:{0}, {1}", operationResponse.OperationCode, operationResponse.Parameters));
        switch (operationResponse.OperationCode)
        {
            case ProtocolDefinition.BATTLE:
                Debug.Log("123");
                break;
            default:
                break;
        }
        ServerController.Instance.ReceiveResponse(operationResponse.OperationCode, operationResponse.Parameters);
    }

    public void SendOperationRequest(byte protocol, Dictionary<byte,object> parameter)
    {
        //parameter = new Dictionary<byte, object> {
        //                     { (byte)1, memberID },   { (byte)2, memberPW }  // parameter key memberID=1, memberPW=2
        //                };

        Debug.LogFormat("Send Request: {0}", protocol);
        this.peer.OpCustom(protocol, parameter, true);

    }

    public void OnStatusChanged(StatusCode statusCode)
    {
        this.DebugReturn(0, string.Format("PeerStatusCallback: {0}", statusCode));
        switch (statusCode)
        {
            case StatusCode.Connect:
                this.ServerConnected = true;
                break;
            case StatusCode.Disconnect:
                this.ServerConnected = false;
                break;
        }

    }

    public void OnEvent(EventData eventData)
    {
        switch (eventData.Code)
        {
            case 1:
                foreach (object obj in eventData.Parameters)
                {
                    Debug.Log(obj);
                }
                Debug.Log("Client 2 get broadcast");
                break;
        }
    }


    //void OnGUI()
    //{
    //    GUI.Label(new Rect(30, 10, 400, 20), "EZServer Unity3D Test");

    //    if (this.ServerConnected)
    //    {
    //        GUI.Label(new Rect(30, 30, 400, 20), "Connected");

    //        GUI.Label(new Rect(30, 60, 80, 20), "MemberID:");
    //        memberID = GUI.TextField(new Rect(110, 60, 100, 20), memberID, 10);

    //        GUI.Label(new Rect(30, 90, 80, 20), "MemberPW:");
    //        memberPW = GUI.TextField(new Rect(110, 90, 100, 20), memberPW, 10);

    //        if (GUI.Button(new Rect(30, 120, 100, 24), "Login"))
    //        {
    //            var parameter = new Dictionary<byte, object> {
    //                         { (byte)1, memberID },   { (byte)2, memberPW }  // parameter key memberID=1, memberPW=2
    //                    };

    //            Debug.Log("Send to Server");
    //            this.peer.OpCustom(5, parameter, true); // operationCode is 5

    //        }

    //        if (LoginStatus)
    //        {
    //            GUI.Label(new Rect(30, 150, 400, 20), "Your MemberID : " + getMemberID);
    //            GUI.Label(new Rect(30, 170, 400, 20), "Your Password : " + getMemberPW);
    //            GUI.Label(new Rect(30, 190, 400, 20), "Your Nickname : " + getNickname);
    //            GUI.Label(new Rect(30, 210, 400, 20), "Ret : " + getRet.ToString());
    //        }
    //        else
    //        {
    //            GUI.Label(new Rect(30, 150, 400, 20), "Please Login");
    //            GUI.Label(new Rect(30, 170, 400, 20), LoginResult);
    //        }


    //    }
    //    else
    //    {
    //        GUI.Label(new Rect(30, 30, 400, 20), "Disconnect");
    //    }
    //}

    public void Click_BroadCast()
    {
        Debug.Log("Send 1");
        var parameter = new Dictionary<byte, object> {
                             { (byte)1, memberID },   { (byte)2, memberPW }  // parameter key memberID=1, memberPW=2
                        };

        this.peer.OpCustom(1, parameter, true);
    }

    private void OnApplicationQuit()
    {
        if (this.peer != null)
        {
            this.peer.Disconnect();
        }
    }
}
