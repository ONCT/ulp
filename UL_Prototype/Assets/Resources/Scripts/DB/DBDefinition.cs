﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBDefinition : MonoBehaviour
{
    public const string DISTANCE = "Distance";
    public const string BUOYANCY = "Buoyancy";
    public const string GAIN = "Gain";
    public const string COST_GOLD = "Cost_gold";
    public const string FIRE_GAP = "fire_gap";


    public static DBDefinition Instance
    {
        get { return m_Instance; }
    }
    private static DBDefinition m_Instance;

    private void Awake()
    {
        m_Instance = this;
    }
}
