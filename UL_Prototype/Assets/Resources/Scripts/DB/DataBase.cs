﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBase
{

    public DataBase(Dictionary<string, Dictionary<string, object>> input_data)
    {
        data = input_data;
    }
    private Dictionary<string, Dictionary<string, object>> data = new Dictionary<string, Dictionary<string, object>>();

    public string GetValueByString(string str1, string str2)
    {
        if (str1 == null || str2 == null)
        {
            Debug.LogWarning("Given Key is null");
            return null;
        }
        if (!data.ContainsKey(str1))
        {
            Debug.LogWarning("Given key_1 " + str1 + " does not exist");
            return null;
        }
        if (!data[str1].ContainsKey(str2))
        {
            Debug.LogWarning("Given key_2 " + str2 + " does not exist");
            return null;
        }

        object obj = data[str1][str2];
        if (obj is string)
        {
            return (string)obj;
        }
        else
        {
            Debug.LogError("Not a String");
            return null;
        }
    }

    public int GetValueByInt(string str1, string str2)
    {
        if (!data.ContainsKey(str1))
        {
            Debug.LogWarning("Given key_1 " + str1 + " does not exist");
            return 0;
        }
        if (!data[str1].ContainsKey(str2))
        {
            Debug.LogWarning("Given key_2 " + str2 + " does not exist");
            return 0;
        }

        object obj = data[str1][str2];
        if (obj is int)
        {
            return (int)obj;
        }
        else
        {
            Debug.LogError("Not a Int");
            return 0;
        }
    }

    public List<string> GetKeyList()
    {
        List<string> tmp = new List<string>();

        foreach (KeyValuePair<string, Dictionary<string, object>> kvp in data)
        {
            tmp.Add(kvp.Key);
        }

        return tmp;
    }
}
