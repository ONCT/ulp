﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBaseManager : MonoBehaviour
{

    public static DataBaseManager Instance
    {
        get { return m_instance; }
    }
    private static DataBaseManager m_instance;

    public DataBase DB_CardData;
    public DataBase DB_EventCardData;
    public DataBase DB_CharcterData;
    public DataBase DB_String;
    public DataBase DB_Skill;


    public void Initiai()
    {
        m_instance = this;
        DB_CardData = GameDBLoader.LoadGameDB("DB_CardData");
        DB_EventCardData = GameDBLoader.LoadGameDB("DB_EventCardData");
        DB_CharcterData = GameDBLoader.LoadGameDB("DB_CharacterData");
        DB_Skill = GameDBLoader.LoadGameDB("DB_SkillData");
        DB_String = GameDBLoader.LoadGameDB("DB_String");
    }
}
