﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skill_19 : SkillEffect
{
    public override int CheckAddition(SkillDetail skillDetail)
    {
        return base.CheckAddition(skillDetail);
    }

    public override void CheckEffect(SkillDetail skillDetail)
    {
        base.CheckEffect(skillDetail);
        GameCore.Instance.battleManager.Add_DirectDamage(my, 2);
        for (int i = 0; i < enemy_chars.Count; i++)
        {
            GameCore.Instance.battleManager.Add_DirectDamage(enemy_chars[i], 2);
        }
    }
}
