﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skill_11 : SkillEffect
{
    public override int CheckAddition(SkillDetail skillDetail)
    {
        return base.CheckAddition(skillDetail);
    }

    public override void CheckEffect(SkillDetail skillDetail)
    {
        base.CheckEffect(skillDetail);
        if (num < 0)
        {
            GameCore.Instance.battleManager.Add_DirectDamage(enemy, -num);
        }
    }
}
