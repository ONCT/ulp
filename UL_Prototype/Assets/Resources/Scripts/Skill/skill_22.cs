﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skill_22 : SkillEffect
{
    public override int CheckAddition(SkillDetail skillDetail)
    {
        return base.CheckAddition(skillDetail);
    }

    public override void CheckEffect(SkillDetail skillDetail)
    {
        base.CheckEffect(skillDetail);
        System.Random ran = new System.Random(skillDetail.damage - skillDetail.atk_cards.Count);

        for (int i = 0; i < 3; i++)
        {
            //int rnd = Random.Range(0, enemy_chars.Count);
            int rnd = ran.Next(0, enemy_chars.Count - 1);
            GameCore.Instance.battleManager.Add_DirectDamage(enemy_chars[rnd], 1);
        }
    }
}
