﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skill_5 : SkillEffect
{
    public override int CheckAddition(SkillDetail skillDetail)
    {
        base.CheckAddition(skillDetail);

        return 0;
    }

    public override void CheckEffect(SkillDetail skillDetail)
    {
        base.CheckEffect(skillDetail);

        int tmp = 0;
        foreach (CardData card in cards)
        {
            if (card.now_funType == FunType.special)
            {
                tmp += card.now_Num;
            }
        }
        GameCore.Instance.battleManager.Add_DirectDamage(enemy, tmp);

    }
}
