﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skill_6 : SkillEffect
{
    public override int CheckAddition(SkillDetail skillDetail)
    {
        return base.CheckAddition(skillDetail);
    }

    public override void CheckEffect(SkillDetail skillDetail)
    {
        base.CheckEffect(skillDetail);
        MoveStruct moveSt = new MoveStruct()
        {
            moveType = MoveType.Back,
            num = 1
        };
        GameCore.Instance.battleManager.Skill_Move(moveSt);
    }
}
