﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skill_2 : SkillEffect
{
    public override int CheckAddition(SkillDetail skillDetail)
    {
        return base.CheckAddition(skillDetail);
    }

    public override void CheckEffect(SkillDetail skillDetail)
    {
        base.CheckEffect(skillDetail);
        SkillBuff SB = new SkillBuff()
        {
            effectType = EffectType.Atk_up,
            effectNum = 5,
            totalCount = 1,
            //remainCount = 1
        };
        GameCore.Instance.battleManager.Add_SkillBuff(my, SB);
    }
}
