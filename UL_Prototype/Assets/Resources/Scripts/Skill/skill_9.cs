﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skill_9 : SkillEffect
{
    public override int CheckAddition(SkillDetail skillDetail)
    {
        base.CheckAddition(skillDetail);

        int num = 0;
        return num;
    }

    public override void CheckEffect(SkillDetail skillDetail)
    {
        base.CheckEffect(skillDetail);
        MoveStruct moveSt = new MoveStruct()
        {
            moveType = MoveType.Forward,
            num = 1
        };
        GameCore.Instance.battleManager.Skill_Move(moveSt);
    }
}