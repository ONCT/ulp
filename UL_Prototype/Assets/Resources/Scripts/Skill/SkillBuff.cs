﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EffectType
{
    Poison,
    Root,
    Seal,
    Atk_down,
    Atk_up,
    Def_down,
    Def_up,
    Move_up,
    Move_down,
}

public class SkillBuff
{
    public EffectType effectType;
    public int effectNum;
    public int totalCount;
    public int remainCount;

    public SkillBuff()
    {
        remainCount = totalCount;
    }
}
