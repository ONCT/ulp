﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skill_12 : SkillEffect
{
    public override int CheckAddition(SkillDetail skillDetail)
    {
        return base.CheckAddition(skillDetail);
    }

    public override void CheckEffect(SkillDetail skillDetail)
    {
        base.CheckEffect(skillDetail);

        Debug.Log(skillDetail.caster);
        Debug.Log(skillDetail.caster.ownerIndex + " " + GameCore.Instance.my_playerData.my_character.ownerIndex);
        //抽兩張
        if (skillDetail.caster == GameCore.Instance.my_playerData.my_character)
        {
            GameCore.Instance.cardManager.DrawCard(2);
            Debug.Log("Draw");
        }
    }
}
