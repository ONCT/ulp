﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skill_14 : SkillEffect
{
    public override int CheckAddition(SkillDetail skillDetail)
    {
        base.CheckAddition(skillDetail);

        int num = 0;

        foreach (CardData card in cards)
        {
            if (card.now_funType == FunType.sword)
            {
                num += card.now_Num;
            }
        }
        return num;
    }

    public override void CheckEffect(SkillDetail skillDetail)
    {
        base.CheckEffect(skillDetail);
    }
}
