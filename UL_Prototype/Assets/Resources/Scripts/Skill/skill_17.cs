﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skill_17 : SkillEffect
{
    public override int CheckAddition(SkillDetail skillDetail)
    {
        return base.CheckAddition(skillDetail);
    }

    public override void CheckEffect(SkillDetail skillDetail)
    {
        base.CheckEffect(skillDetail);
        if (num > 0)
        {
            SkillBuff SB = new SkillBuff();
            SB.effectType = EffectType.Poison;
            SB.effectNum = 1;
            SB.totalCount = 3;
            SB.remainCount = 3;
            GameCore.Instance.battleManager.Add_SkillBuff(enemy, SB);

            SkillBuff SB2 = new SkillBuff();
            SB2.effectType = EffectType.Root;
            SB2.totalCount = 3;
            SB2.remainCount = 3;
            GameCore.Instance.battleManager.Add_SkillBuff(enemy, SB2);

        }
    }
}
