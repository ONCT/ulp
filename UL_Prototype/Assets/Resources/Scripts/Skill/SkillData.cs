﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SkillData
{
    public CardArea cardArea;
    public string skill_name;
    public RangeType rangeType;
    public List<SkillCondition> list_skillcond = new List<SkillCondition>();
    public int skill_num;
    const string SKILL_DATA_PATH = "Scripts/Skill/";

    DataBase DB_Skill;

    public SkillData(string args)
    {
        DB_Skill = DataBaseManager.Instance.DB_Skill;

        skill_name = args;
        skill_num = DB_Skill.GetValueByInt(args, "number");
        rangeType = (RangeType)Enum.Parse(typeof(RangeType), DB_Skill.GetValueByString(args, "range"));
        cardArea = (CardArea)Enum.Parse(typeof(CardArea), DB_Skill.GetValueByString(args, "card_area"));

        for (int i = 1; i <= 3; i++)
        {
            string con = DB_Skill.GetValueByString(args, "condition_" + i);
            if (con == "0") { continue; }
            SkillCondition skillCondition = new SkillCondition();
            FunType FT = (FunType)Enum.Parse(typeof(FunType), con);
            string req = DB_Skill.GetValueByString(args, "require_" + i);
            SkillRequire skillRequire = (SkillRequire)Enum.Parse(typeof(SkillRequire), req);
            int num = DB_Skill.GetValueByInt(args, "require_num_" + i);

            skillCondition.condition = FT;
            skillCondition.skillRequire = skillRequire;
            skillCondition.con_num = num;

            list_skillcond.Add(skillCondition);
        }
    }

    public bool isSkillSuccess(SkillDetail skillDetail)
    {
        List<CardData> cards;
        RangeType range;
        CardArea cardArea;

        cardArea = skillDetail.cardArea;
        range = skillDetail.range;
        cards = skillDetail.atk_cards;

        if (cardArea != this.cardArea) { return false; }
        switch (rangeType)
        {
            case RangeType.all:
                break;
            case RangeType.s_m:
                if (range == RangeType.longRange) { return false; }
                break;
            case RangeType.s_l:
                if (range == RangeType.middleRange) { return false; }
                break;
            case RangeType.m_l:
                if (range == RangeType.shortRange) { return false; }
                break;
            case RangeType.shortRange:
                if (range != RangeType.shortRange) { return false; }
                break;
            case RangeType.middleRange:
                if (range != RangeType.middleRange) { return false; }
                break;
            case RangeType.longRange:
                if (range != RangeType.longRange) { return false; }
                break;
        }

        List<int> compare_index = new List<int>();
        List<int> input = new List<int>();
        bool bo = false;
        foreach (SkillCondition SC in list_skillcond)
        {
            input = compare_index;
            bo = SC.GetCondition(cards, input, out compare_index);
            if (bo == false) { return false; }
        }

        return true;
    }

    public int GetResult(SkillDetail skillDetail)
    {
        int bonus = 0;
        //SkillEffect SE = Resources.Load<SkillEffect>(SKILL_DATA_PATH + skill_name);
        SkillEffect SE = GameCore.Instance.skillManager.GetSkill(skill_name);

        bonus = SE.CheckAddition(skillDetail);

        return skill_num + bonus;
    }

    public SkillEffect GetSkillEffect()
    {
        //SkillEffect SE = Resources.Load<SkillEffect>(SKILL_DATA_PATH + skill_name);
        SkillEffect SE = GameCore.Instance.skillManager.GetSkill(skill_name);

        return SE;
    }
}

public enum SkillRequire
{
    less,
    equal,
    more,
}


public class SkillCondition
{
    public FunType condition;
    public int con_num;
    public SkillRequire skillRequire;

    public bool GetCondition(List<CardData> cards, List<int> input, out List<int> output)
    {
        bool bo = false;
        List<CardData> meetcards = new List<CardData>();
        foreach (CardData CD in cards)
        {
            if (CD.now_funType == condition || (condition == FunType.none && !input.Contains(CD.card_index)))
            {
                meetcards.Add(CD);
            }
        }
        output = input;

        int total = 0;
        if (condition != FunType.none)
        {
            switch (skillRequire)
            {
                case SkillRequire.less:
                    foreach (CardData CD in meetcards)
                    {
                        total += CD.now_Num;
                    }
                    if (total <= con_num)
                    {
                        bo = true;
                    }
                    break;
                case SkillRequire.equal:
                    if (con_num == 0)
                    {
                        foreach (CardData CD in meetcards)
                        {
                            if (CD.now_Num != 0)
                            {
                                bo = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        foreach (CardData CD in meetcards)
                        {
                            if (CD.now_Num == con_num && !input.Contains(CD.card_index))
                            {
                                output.Add(CD.card_index);
                                bo = true;
                                break;
                            }
                        }
                    }
                    break;
                case SkillRequire.more:
                    foreach (CardData CD in meetcards)
                    {
                        total += CD.now_Num;
                    }
                    if (total >= con_num)
                    {
                        bo = true;
                    }
                    break;
            }
        }
        else if (condition == FunType.none)
        {
            switch (skillRequire)
            {
                case SkillRequire.less:
                    foreach (CardData CD in meetcards)
                    {
                        if (CD.now_Num <= con_num)
                        {
                            output.Add(CD.card_index);
                            bo = true;
                            break;
                        }
                    }
                    break;
                case SkillRequire.equal:
                    foreach (CardData CD in meetcards)
                    {
                        if (CD.now_Num == con_num)
                        {
                            output.Add(CD.card_index);
                            bo = true;
                            break;
                        }
                    }
                    break;
                case SkillRequire.more:
                    foreach (CardData CD in meetcards)
                    {
                        if (CD.now_Num >= con_num)
                        {
                            output.Add(CD.card_index);
                            bo = true;
                            break;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        return bo;
    }
}
