﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skill_16 : SkillEffect
{
    public override int CheckAddition(SkillDetail skillDetail)
    {
        return base.CheckAddition(skillDetail);
    }

    public override void CheckEffect(SkillDetail skillDetail)
    {
        base.CheckEffect(skillDetail);

        SkillBuff SB = new SkillBuff()
        {
            effectType = EffectType.Atk_up,
            effectNum = 10,
            totalCount = 3,
        };
        GameCore.Instance.battleManager.Add_SkillBuff(enemy, SB);

        SkillBuff SB2 = new SkillBuff()
        {
            effectType = EffectType.Def_up,
            effectNum = 10,
            totalCount = 3,
        };
        GameCore.Instance.battleManager.Add_SkillBuff(enemy, SB2);

        SkillBuff SB3 = new SkillBuff()
        {
            effectType = EffectType.Move_up,
            effectNum = 1,
            totalCount = 3,
        };
        GameCore.Instance.battleManager.Add_SkillBuff(enemy, SB3);
    }
}
