﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct SkillDetail
{
    public List<CardData> atk_cards;
    public List<CardData> Def_cards;
    public CardArea cardArea;
    public RangeType range;
    public Character caster;
    public Character hitter;
    public int damage;
}
