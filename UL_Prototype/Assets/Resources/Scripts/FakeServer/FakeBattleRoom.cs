﻿using ExitGames.Client.Photon;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeBattleRoom : MonoBehaviour
{
    private const byte BATTLE_PROTOCAL = 101;

    private const byte FUNCTION_POINTER = 99;
    private const byte START = 1;
    private const byte USE_MOV_CARD = 2;
    private const byte MOVE_TYPE = 11;

    private const byte P1_USECARDS = 15;
    private const byte P2_USECARDS = 16;

    private const byte RECEIVE_MOVE = 20;
    private const byte P1_MOVESTRUCT = 21;
    private const byte P2_MOVESTRUCT = 22;
    private const byte TOTAL_MOVESTRUCT = 23;
    private const byte FIRSTER = 24;

    private const byte SEND_ATK_CARD = 31;
    private const byte RECEIVE_ATK_CARD = 32;

    private const byte SEND_DEF_CARD = 41;
    private const byte RECEIVE_DEF_CARD = 42;

    private const byte SEND_CHANGE_CHAR = 51;
    private const byte RECEIVE_CHANGE_CHAR = 52;
    private const byte P1_CHANGE_CHAR_INDEX = 53;
    private const byte P2_CHANGE_CHAR_INDEX = 54;

    private const byte CREATE_ROOM = 255;


    //private const byte 
    private const byte TEST = 255;

    public PlayerData player1;
    public PlayerData player2;

    private List<PlayerData> all_players = new List<PlayerData>();

    //訊息進來先到這裡，再轉發給其他function
    public void Main_Daemon(PlayerData player, Dictionary<byte, object> parameter)
    {
        object tmp = parameter[FUNCTION_POINTER];
        Dictionary<byte, object> args = new Dictionary<byte, object>();
        foreach (KeyValuePair<byte, object> kvp in parameter)
        {
            args.Add(kvp.Key, kvp.Value);
        }

        args.Remove(FUNCTION_POINTER);
        byte fun_pointer = Convert.ToByte(tmp);

        switch (fun_pointer)
        {
            case USE_MOV_CARD:
                Cost_MOV_card(player, args);
                break;
            case SEND_ATK_CARD:
                Cost_ATK_card(player, args);
                break;
            case SEND_DEF_CARD:
                Cost_DEF_card(player, args);
                break;
            case SEND_CHANGE_CHAR:
                Change_Character(player,args);
                break;
            default:
                break;
        }
    }

    public List<CardData> CardDeck = new List<CardData>();
    public List<CardData> CardGrave = new List<CardData>();
    public List<CardData> All_Cards = new List<CardData>();

    Dictionary<byte, object> parameter = new Dictionary<byte, object>() { { 99, 99 } };

    public enum battle_phase
    {
        DRAW,
        MOVE,
        ATT_PHASE_1,
        ATT_PHASE_2,
    }

    public void Initial(List<CardData> cards)
    {
        //this.guid = Guid.NewGuid();
        //DB_CardData = DataBaseManager.Instance.DB_CardData;
        All_Cards.AddRange(cards);
    }

    public bool AddPlayers(PlayerData player1, PlayerData player2)
    {
        this.player1 = player1;
        this.player2 = player2;

        all_players.Add(player1);
        all_players.Add(player2);
        return true;
    }

    public void Cost_MOV_card(PlayerData player, Dictionary<byte, object> args)
    {
        Debug.Log("Fake Server Get MOV");
        List<CardData> cards = GetCards((string)args[USE_MOV_CARD]);
        GetMoveCard(player.My_index, cards, (MoveType)(int)args[MOVE_TYPE]);
    }

    public void Cost_ATK_card(PlayerData player, Dictionary<byte, object> args)
    {
        Debug.Log("Fake Server Get ATK");
        List<CardData> cards = GetCards((string)args[SEND_ATK_CARD]);

        parameter = new Dictionary<byte, object>() {
            { RECEIVE_ATK_CARD, args[SEND_ATK_CARD]},
            { FUNCTION_POINTER, RECEIVE_ATK_CARD}
        };

        SendBesideMe(player, parameter);
    }

    public void Cost_DEF_card(PlayerData player, Dictionary<byte, object> args)
    {

    }

    private void Change_Character(PlayerData player, Dictionary<byte, object> args)
    {
        Debug.Log("Fake Server Get Change");
        int char_index = (int)args[SEND_CHANGE_CHAR];
        GetChangeChar(player.My_index, char_index);
    }

    private void DrawCards(int num)
    {

    }

    private void InitialCard()
    {

    }

    private CardData GetCard(string str)
    {
        int length = str.Length;
        string c_s = str.Substring(0, length - 1);
        string d_s = str.Substring(length - 1, 1);

        int c_i = int.Parse(c_s);
        int d_i = int.Parse(d_s);

        CardData CD = All_Cards.Find(x => x.card_index == c_i);

        switch (d_i)
        {
            case 0:
                CD.now_funType = CD.top_funType;
                CD.now_Num = CD.top_Num;
                break;
            case 1:
                CD.now_funType = CD.bot_funType;
                CD.now_Num = CD.bot_Num;
                break;
        }
        return CD;
    }

    private List<CardData> GetCards(string str)
    {
        List<CardData> tmp = new List<CardData>();
        string[] lines = str.Split(" "[0]);
        for (int i = 0; i < lines.Length - 1; i++)
        {
            tmp.Add(GetCard(lines[i]));
        }

        return tmp;
    }

    #region ---移動相關---
    int P1_move_count;
    int P2_move_count;
    MoveType P1_moveType;
    MoveType P2_moveType;
    int firster;
    private int dealMoveCount;
    List<MoveStruct> all_move = new List<MoveStruct>();
    List<CardData> P1_use_cards;
    List<CardData> P2_use_cards;
    private void GetMoveCard(int index, List<CardData> cards, MoveType moveType)
    {
        Debug.LogFormat("<color=green> FakeRoom Receive MoveCard");
        if (index == 0)
        {
            P1_moveType = moveType;
            P1_use_cards = cards;
            P1_move_count = CheckMoveNum(cards, player1.my_character);

            MoveStruct moveSt = new MoveStruct()
            {
                num = P1_move_count,
                moveType = moveType,
            };
            all_move.Add(moveSt);
        }
        else
        {
            P2_moveType = moveType;

            P2_use_cards = cards;
            P2_move_count = CheckMoveNum(cards, player2.my_character);

            MoveStruct moveSt = new MoveStruct()
            {
                num = P2_move_count,
                moveType = moveType,
            };
            all_move.Add(moveSt);
        }
        dealMoveCount++;

        //收到兩份移動之後
        if (dealMoveCount >= 2)
        {
            firster = CalculateWhoFirst();
            var v = CalculateMoveDistance();

            string P1_move_str = P1_move_count + " " + ((int)P1_moveType).ToString();
            string P2_move_str = P2_move_count + " " + ((int)P2_moveType).ToString();
            string total_move_str = v.num + " " + ((int)v.moveType).ToString();
            string P1_cards_str = CardsToString(P1_use_cards);
            string P2_cards_str = CardsToString(P2_use_cards);

            parameter = new Dictionary<byte, object>() {
                { P1_MOVESTRUCT, P1_move_str } ,
                { P2_MOVESTRUCT, P2_move_str},
                { TOTAL_MOVESTRUCT, total_move_str},
                { FIRSTER, firster},
                { P1_USECARDS, P1_cards_str },
                { P2_USECARDS, P2_cards_str},
                { FUNCTION_POINTER,RECEIVE_MOVE}
            };
            dealMoveCount = 0;
            FakeServer.Instance.SendRequest(BATTLE_PROTOCAL, parameter);
        }
    }

    private int CalculateWhoFirst()
    {
        Debug.Log(string.Format("P1Count : {0}, P2Count : {1}", P1_move_count, P2_move_count));
        //if (GameCore.Instance.myIndex != 0) { return 99; }
        if (P1_move_count > P2_move_count)
        {
            return 0;
        }
        else if (P2_move_count > P1_move_count)
        {
            return 1;
        }
        else if (P1_move_count == P2_move_count)
        {
            int rnd = UnityEngine.Random.Range(0, 1);
            return rnd;
        }
        return 0;
    }

    private MoveStruct CalculateMoveDistance()
    {
        int total = 0;

        MoveStruct moveSt = new MoveStruct();
        foreach (var v in all_move)
        {
            if (v.moveType == MoveType.Forward)
            {
                total += v.num;
            }
            else if (v.moveType == MoveType.Back)
            {
                total -= v.num;
            }
        }
        if (total >= 0)
        {
            moveSt.moveType = MoveType.Forward;
            moveSt.num = total;
        }
        else if (total < 0)
        {
            moveSt.moveType = MoveType.Back;
            moveSt.num = Mathf.Abs(total);
        }

        all_move.Clear();

        return moveSt;
    }
    #endregion

    #region ---換角相關---
    int P1_charIndex;
    int P2_charIndex;
    private void GetChangeChar(int playerIndex, int charIndex)
    {
        Debug.LogFormat("<color=green> FakeRoom Receive GetChangeChar");
        if (playerIndex == 0)
        {
            P1_charIndex = charIndex;
        }
        else
        {
            P2_charIndex = charIndex;
        }
        dealMoveCount++;
        if (dealMoveCount >= 2)
        {
            parameter = new Dictionary<byte, object>() {
                { P1_CHANGE_CHAR_INDEX, P1_charIndex},
                { P2_CHANGE_CHAR_INDEX, P2_charIndex},
                { FUNCTION_POINTER,RECEIVE_CHANGE_CHAR}
            };
            dealMoveCount = 0;
            FakeServer.Instance.SendRequest(BATTLE_PROTOCAL, parameter);
        }
    }
    #endregion

    public int CheckMoveNum(List<CardData> cards, Character character)
    {
        int base_mov = character.GetMOV();
        int card_move = 0;

        foreach (CardData card in cards)
        {
            if (card.now_funType == FunType.move)
            {
                card_move += card.now_Num;
            }
        }
        int skill_move;
        skill_move = character.GetSkillBouns(character, cards, CardArea.Move);

        return base_mov + card_move + skill_move;
    }

    public int CheckAttackNum(List<CardData> cards, Character character)
    {
        int base_attack = character.GetATK();

        int card_attack = 0;
        foreach (CardData card in cards)
        {
            switch (GameCore.Instance.my_playerData.range)
            {
                case RangeType.shortRange:
                    if (card.now_funType == FunType.sword)
                    {
                        card_attack += card.now_Num;
                    }
                    break;
                case RangeType.middleRange:
                case RangeType.longRange:
                    if (card.now_funType == FunType.gun)
                    {
                        card_attack += card.now_Num;
                    }
                    break;
            }
        }
        if (card_attack == 0) { base_attack = 0; }

        int skill_attack = 0;
        skill_attack = character.GetSkillBouns(character, cards, CardArea.Attack);
        return base_attack + card_attack + skill_attack;
    }

    public int CheckDefenseNum(List<CardData> cards, Character character)
    {
        int base_def = character.GetDEF();

        int card_defense = 0;
        foreach (CardData card in cards)
        {
            if (card.now_funType == FunType.shield)
            {
                card_defense += card.now_Num;
            }
        }

        int skill_def = 0;
        skill_def = character.GetSkillBouns(character, cards, CardArea.Defense);
        return base_def + card_defense + skill_def;
    }

    private string CardsToString(List<CardData> cards)
    {
        string args = string.Empty;
        foreach (CardData card in cards)
        {
            args += card.card_index + "" + (int)card.card_Direction + " ";
        }
        return args;
    }

    private void SendToAll(Dictionary<byte, object> parameter)
    {
        foreach (var v in all_players)
        {
            FakeServer.Instance.SendRequest(BATTLE_PROTOCAL, parameter);
        }
    }

    private void SendBesideMe(PlayerData player, Dictionary<byte, object> parameter)
    {
        foreach (var v in all_players)
        {
            if (v == player) { continue; }
            FakeServer.Instance.SendRequest(BATTLE_PROTOCAL, parameter);
        }
    }
}
