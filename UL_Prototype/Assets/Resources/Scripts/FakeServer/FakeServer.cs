﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeServer : MonoBehaviour
{

    public static FakeServer Instance
    {
        get
        {
            return m_instance;
        }
    }
    private static FakeServer m_instance;

    private void Awake()
    {
        m_instance = this;
    }

    public FakeBattleRoom battleRoom;

    public void ReceiveRequest(byte protocal, Dictionary<byte, object> parameter)
    {
        battleRoom.Main_Daemon(GameCore.Instance.my_playerData, parameter);
    }

    public void SendRequest(byte protocal, Dictionary<byte, object> parameter)
    {
        //ServerController.Instance.ReceiveResponse(protocal, parameter);
        StartCoroutine(WaitAndSend(protocal, parameter));
    }

    private IEnumerator WaitAndSend(byte protocal, Dictionary<byte, object> parameter)
    {
        yield return new WaitForSeconds(1.0f);

        ServerController.Instance.ReceiveResponse(protocal, parameter);
    }
}
