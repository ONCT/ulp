﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLoadSkill : MonoBehaviour
{

    public SkillEffect SE;
    public TestFather TF;

    // Use this for initialization
    void Start()
    {
        GameObject GO = Resources.Load<GameObject>("Scripts/Skill/skill_1") as GameObject;
        SE = GO.GetComponent<SkillEffect>();

        int i = TF.Test();
        Debug.Log(i);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
