﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class TestLoadAssetBundle : MonoBehaviour
{/*

    // Use this for initialization
    void Start()
    {
        StartCoroutine(DownloadAssetBundle());
        StartCoroutine(DownloadDBBundle());
    }

    // Update is called once per frame
    void Update()
    {

    }

    //"https://firebasestorage.googleapis.com/v0/b/test-49b4c.appspot.com/o/testcube?alt=media&token=95b226a5-4e08-40e8-b4ee-4cef162929c7"
    IEnumerator DownloadAssetBundle()
    {
        Debug.Log("Start load Bundle");


        //UnityWebRequest www = UnityWebRequestAssetBundle.GetAssetBundle("https://firebasestorage.googleapis.com/v0/b/test-49b4c.appspot.com/o/testcube?alt=media&token=95b226a5-4e08-40e8-b4ee-4cef162929c7");

        WWW w = WWW.LoadFromCacheOrDownload("https://firebasestorage.googleapis.com/v0/b/test-49b4c.appspot.com/o/testcube?alt=media&token=95b226a5-4e08-40e8-b4ee-4cef162929c7", 1);

        yield return w;

        Debug.Log("Download end");
        if (w.error != null)
        {
            Debug.Log(w.error);
        }


        AssetBundle bundle = w.assetBundle;
        Debug.Log("download success");

        object[] pres = bundle.LoadAllAssets();
        string[] strs = bundle.GetAllAssetNames();
        Debug.Log(strs[0]);


        Debug.Log("load done");
    }

    IEnumerator DownloadDBBundle()
    {
        //WWW www = WWW.LoadFromCacheOrDownload("https://firebasestorage.googleapis.com/v0/b/test-49b4c.appspot.com/o/database?alt=media&token=53498588-5d71-4183-85bd-54888acb4852", 0);
        var www = UnityWebRequestAssetBundle.GetAssetBundle("https://firebasestorage.googleapis.com/v0/b/test-49b4c.appspot.com/o/testcube?alt=media&token=95b226a5-4e08-40e8-b4ee-4cef162929c7");

        yield return www.SendWebRequest();

        if (www.error != null)
        {
            Debug.Log(www.error);
        }

        //AssetBundle bundle = www.;
        AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(www);
        Debug.Log("download DB Bundle Success");

        object[] pres = bundle.LoadAllAssets();
        string[] strs = bundle.GetAllAssetNames();
        Debug.Log("DB" + strs[0]);


        Debug.Log("load done");
        
    }
    */
}
