﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;
using System.Linq;

public class TestReflection : MonoBehaviour
{
    public SkillEffect SE;
    // Use this for initialization
    void Start()
    {
        //var v = SkillManager.Instance.GetSkill("skill_1");
        //Debug.Log(v);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public static IEnumerable<T> GetposterityClasses<T>(params object[] constructorArgs)
    {
        List<T> objects = new List<T>();

        var ass = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(o => o.GetTypes()).Where(o => typeof(T).IsAssignableFrom(o));

        foreach (Type type in ass
            .Where(o => o.IsClass && !o.IsAbstract && o.IsSubclassOf(typeof(T))))
        {
            objects.Add((T)Activator.CreateInstance(type, constructorArgs));
        }

        return objects;
    }
}
