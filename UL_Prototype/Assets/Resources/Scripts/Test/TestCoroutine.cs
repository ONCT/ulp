﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCoroutine : MonoBehaviour
{
    IEnumerator GO;

    // Use this for initialization
    void Start()
    {
        GO = StartCo();

        StartCoroutine(GO);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StopCoroutine(GO);
        }
    }

    IEnumerator StartCo()
    {
        yield return new WaitForSeconds(5.0f);

        Debug.Log("GO");
    }


}
