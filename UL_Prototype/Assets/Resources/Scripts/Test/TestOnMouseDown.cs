﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TestOnMouseDown : MonoBehaviour, IPointerDownHandler {

    private void OnMouseDown()
    {
        Debug.Log("OnMouseDown");
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("OnPointerDown");
    }

    private void OnPointerDown(PointerEventData pointerEventData)
    {
    }
}
