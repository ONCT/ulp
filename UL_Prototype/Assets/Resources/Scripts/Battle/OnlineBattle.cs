﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnlineBattle : BattleStateMachine
{
    public override void Initial()
    {
        throw new NotImplementedException();
    }

    public override void StartPreparePhase(Action act)
    {
        throw new NotImplementedException();
    }

    public override void StartDrawPhase()
    {
        throw new NotImplementedException();
    }

    public override void StartMovePhase()
    {
        throw new NotImplementedException();
    }

    public override void StartWaitingChangeCharPhase()
    {
        throw new NotImplementedException();
    }

    public override void StartChangeCharPhase()
    {
        throw new NotImplementedException();
    }

    public override void StartAttackPhase()
    {
        throw new NotImplementedException();
    }

    public override void StartDefensePhase()
    {
        throw new NotImplementedException();
    }

    public override void StartEndPhase()
    {
        throw new NotImplementedException();
    }

    public override void SendMoveCard(string cards, int moveType)
    {
        throw new NotImplementedException();
    }

    public override void ReceiveMoveCommand(int firster, MoveStruct my_move, MoveStruct en_move, MoveStruct total_move, List<CardData> my_useCards, List<CardData> en_useCards)
    {
        throw new NotImplementedException();
    }

    public override void SendAtkCard(string cards)
    {
        throw new NotImplementedException();
    }

    public override void ReceiveAtkCard(List<CardData> cards)
    {
        throw new NotImplementedException();
    }

    public override void SendDefCard(string cards)
    {
        throw new NotImplementedException();
    }

    public override void ReceiveDefCard(List<CardData> cards)
    {
        throw new NotImplementedException();
    }
    public override void SendChangeChar(int change_index)
    {
        throw new NotImplementedException();
    }
    public override void ReceiveChangeChar(int my_index, int enemy_index)
    {
        throw new NotImplementedException();
    }
    public override void DrawDeafaltCard()
    {
        throw new NotImplementedException();
    }
}
