﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum TargetFlag
{
    My_char,
    En_char,
}

public enum BattlePhase
{
    Prepare,
    Draw,
    Move,
    WaitingChange,
    Change,
    Attack,
    Defense,
    Wait,
    End,
}

public class BattleManager : MonoBehaviour
{
    public List<CardData> my_use_attack_cards = new List<CardData>();
    public List<CardData> en_use_attack_cards = new List<CardData>();
    public List<CardData> my_use_defense_cards = new List<CardData>();
    public List<CardData> en_use_defense_cards = new List<CardData>();
    public int playerTurnIndex;
    public BattleStateMachine battleMech;

    IEnumerator Attack_Coroutine;
    IEnumerator Defense_Coroutine;

    public void Initial()
    {
        Attack_Coroutine = FakeAttachDamage();
        Defense_Coroutine = EmptyDefense(new List<SkillEffect>());
    }

    public void StartGame(bool isFake)
    {
        if (isFake == true) { battleMech = gameObject.AddComponent<SingleBattle>(); }
        else { battleMech = gameObject.AddComponent<OnlineBattle>(); }

        GameCore.Instance.phaseController.StartGame();
    }

    public int CheckAttackNum(List<CardData> cards, Character character, bool bo = false)
    {
        int base_attack = character.GetATK();

        int card_attack = 0;
        foreach (CardData card in cards)
        {
            switch (GameCore.Instance.my_playerData.range)
            {
                case RangeType.shortRange:
                    if (card.now_funType == FunType.sword)
                    {
                        card_attack += card.now_Num;
                    }
                    break;
                case RangeType.middleRange:
                case RangeType.longRange:
                    if (card.now_funType == FunType.gun)
                    {
                        card_attack += card.now_Num;
                    }
                    break;
            }
        }
        if (card_attack == 0) { base_attack = 0; }

        int skill_attack = 0;
        skill_attack = character.GetSkillBouns(character, cards, CardArea.Attack);
        if (bo)
        {
            if (character == GameCore.Instance.my_playerData.my_character)
            {
                my_atk_skillEffect.AddRange(character.GetSkillEffect(cards, CardArea.Attack));
            }
            else
            {
                en_atk_skillEffect.AddRange(character.GetSkillEffect(cards, CardArea.Attack));
            }
        }
        return base_attack + card_attack + skill_attack;
    }

    public int CheckDefenseNum(List<CardData> cards, Character character, bool bo = false)
    {
        int base_def = character.GetDEF();

        int card_defense = 0;
        foreach (CardData card in cards)
        {
            if (card.now_funType == FunType.shield)
            {
                card_defense += card.now_Num;
            }
        }

        int skill_def = 0;
        skill_def = character.GetSkillBouns(character, cards, CardArea.Defense);
        if (bo)
        {
            if (character == GameCore.Instance.my_playerData.my_character)
            {
                my_def_skillEffect.AddRange(character.GetSkillEffect(cards, CardArea.Defense));
            }
            else
            {
                en_def_skillEffect.AddRange(character.GetSkillEffect(cards, CardArea.Defense));
            }
        }
        return base_def + card_defense + skill_def;
    }

    public int CheckMoveNum(List<CardData> cards, Character character, bool bo = false)
    {
        int base_mov = character.GetMOV();
        int card_move = 0;

        foreach (CardData card in cards)
        {
            if (card.now_funType == FunType.move)
            {
                card_move += card.now_Num;
            }
        }
        int skill_move;
        skill_move = character.GetSkillBouns(character, cards, CardArea.Move);
        if (bo)
        {
            if (character == GameCore.Instance.my_playerData.my_character && character.ownerIndex == GameCore.Instance.myIndex)
            {
                my_move_skillEffect.AddRange(character.GetSkillEffect(cards, CardArea.Move));
            }
            else
            {
                en_move_skillEffect.AddRange(character.GetSkillEffect(cards, CardArea.Move));
            }
        }
        return base_mov + card_move + skill_move;
    }

    public int pre_atk;
    public int pre_def;

    public List<SkillEffect> my_atk_skillEffect = new List<SkillEffect>();
    public List<SkillEffect> my_def_skillEffect = new List<SkillEffect>();
    public List<SkillEffect> en_atk_skillEffect = new List<SkillEffect>();
    public List<SkillEffect> en_def_skillEffect = new List<SkillEffect>();
    public List<SkillEffect> my_move_skillEffect = new List<SkillEffect>();
    public List<SkillEffect> en_move_skillEffect = new List<SkillEffect>();
    public int CalculateDamage()
    {
        int atkCount = 0;
        for (int i = 0; i < pre_atk; i++)
        {
            int rnd = UnityEngine.Random.Range(0, 100);
            if (rnd <= GameDefinition.Instance.HIT_CHANCE) { atkCount++; }
        }
        int defCount = 0;
        for (int i = 0; i < pre_def; i++)
        {
            int rnd = UnityEngine.Random.Range(0, 100);
            if (rnd <= GameDefinition.Instance.HIT_CHANCE) { defCount++; }
        }
        Debug.Log("atckCount: " + atkCount + ", defCount: " + defCount);

        int result;
        result = atkCount - defCount;


        return result;
    }

    public void Send_Attack(List<CardData> cards)
    {
        string args = string.Empty;
        args = GameCore.Instance.cardManager.CardsToString(cards);
        battleMech.SendAtkCard(args);
        /*
        int atk = CheckAttackNum(cards, GameCore.Instance.my_playerData.my_character, true);
        GameCore.Instance.UI_manager.UI_stateBar.battleMiddleInfo.SetMyAtk(atk);
        GameCore.Instance.UI_manager.CheckAllArgs();
        my_use_attack_cards = cards;

        if (GameCore.Instance.isFake)
        {
            ChangePlayer();
            Receive_Attack(cards);
            GameCore.Instance.phaseController.CallNextStep();
        }
        else
        {
            GameCore.Instance.playerManager.Send_UseAttackCard(args);
        }
        */
    }

    public void Receive_Attack(List<CardData> cards)
    {
        battleMech.ReceiveAtkCard(cards);
        /*
        GameCore.Instance.cardManager.Online_UseCard(cards);
        int tmp = CheckAttackNum(cards, GameCore.Instance.my_playerData.enemy_character, !GameCore.Instance.isFake);
        Debug.Log("Receive_Attack : " + tmp);
        pre_atk = tmp;
        //pre_def = GameCore.Instance.my_playerData.my_character.DEF;
        GameCore.Instance.UI_manager.UI_stateBar.battleMiddleInfo.SetEnemyAtk(pre_atk);
        //GameCore.Instance.UI_manager.UI_stateBar.SetMyDef(pre_def, true);
        GameCore.Instance.UI_manager.UI_skillBar.OnlineCheckSkill(GameCore.Instance.my_playerData.enemy_character, cards, CardArea.Attack);
        //GameCore.Instance.my_playerData.enemy_character.CheckBuffCount();
        //Attack_Coroutine = AttachDamage();
        //StartCoroutine(Attack_Coroutine);
        //StopCoroutine(Defense_Coroutine);
        en_use_attack_cards = cards;
        UI_Manager.Instance.UI_Defense.can_useCard = true;
        UI_Manager.Instance.RefreshAllUI();

        /*
        if (GameCore.Instance.isFake)
        {
            GameCore.Instance.phaseController.CallNextStep();
        }
        */
        
    }

    public void Send_Defense(List<CardData> cards)
    {
        string args = string.Empty;
        args = GameCore.Instance.cardManager.CardsToString(cards);

        int tmp = CheckDefenseNum(cards, GameCore.Instance.my_playerData.my_character, true);
        pre_def = tmp;
        GameCore.Instance.UI_manager.UI_stateBar.battleMiddleInfo.SetMyDef(tmp);
        //GameCore.Instance.UI_manager.UI_skillBar.OnlineCheckSkill(GameCore.Instance.my_playerData.my_character, cards, CardArea.Defense);
        GameCore.Instance.UI_manager.CheckAllArgs();
        //GameCore.Instance.my_playerData.my_character.CheckBuffCount();
        //Defense_Coroutine = EmptyDefense(my_def_skillEffect);
        //StartCoroutine(Defense_Coroutine);
        my_use_defense_cards = cards;

        if (GameCore.Instance.isFake)
        {
            //ChangePlayer();
            StartCoroutine(FakeAttachDamage());
            Receive_Defense(cards);
        }
        else
        {
            GameCore.Instance.playerManager.Send_UseDefenseCard(args);
        }
    }

    public void Receive_Defense(List<CardData> cards)
    {
        GameCore.Instance.cardManager.Online_UseCard(cards);
        int tmp = CheckDefenseNum(cards, GameCore.Instance.my_playerData.enemy_character, !GameCore.Instance.isFake);
        GameCore.Instance.UI_manager.UI_stateBar.battleMiddleInfo.SetEnemyDef(tmp);
        GameCore.Instance.UI_manager.UI_skillBar.OnlineCheckSkill(GameCore.Instance.my_playerData.enemy_character, cards, CardArea.Defense);
        //GameCore.Instance.my_playerData.enemy_character.CheckBuffCount();
        //Defense_Coroutine = EmptyDefense(en_def_skillEffect);
        //StartCoroutine(Defense_Coroutine);
        en_use_defense_cards = cards;

        GameCore.Instance.phaseController.CallNextStep();
    }

    public void Send_Move(List<CardData> cards, MoveType moveType)
    {
        string args = string.Empty;
        args = GameCore.Instance.cardManager.CardsToString(cards);

        battleMech.SendMoveCard(args, (int)moveType);

        /*
        if (GameCore.Instance.isFake)
        {
            Receive_Move(GameCore.Instance.my_playerData.My_index, cards, (int)moveType);
        }
        else
        {
            GameCore.Instance.playerManager.Send_UseMoveCard(args, (int)moveType);
            GameCore.Instance.phaseController.GetMoveCard(GameCore.Instance.playerManager.my_index, cards, moveType);
        }
        */
    }

    public void Receive_MoveCard(int ownerIndex, List<CardData> cards, int moveType)
    {
        GameCore.Instance.cardManager.Online_UseCard(cards);
        PlayerData player = GameCore.Instance.playerManager.players[ownerIndex];
        MoveType MT = (MoveType)moveType;
        if (MT == MoveType.Change)
        {

        }
        Debug.Log("MoverIndex: " + ownerIndex);
        //GameCore.Instance.phaseController.GetMoveCard(ownerIndex, cards, MT);

        //if (GameCore.Instance.isFake)
        //{
        //    moveCount++;
        //    if (moveCount % 2 == 1)
        //    {
        //        ChangePlayer();
        //        GameCore.Instance.phaseController.CallNextStep();
        //    }
        //}
    }

    public void Receive_Move(int firster, MoveStruct my_move, MoveStruct en_move, MoveStruct total_move, List<CardData> my_useCards, List<CardData> en_useCards)
    {
        battleMech.ReceiveMoveCommand(firster, my_move, en_move, total_move, my_useCards, en_useCards);
    }

    public void Normal_Move(MoveStruct moveSt)
    {
        UI_Manager.Instance.UI_MyUsedCard.AllToTrashCan();
        UI_Manager.Instance.enemy_hand.UsedCardToTrashcan();
        var range = GameCore.Instance.my_playerData.GetMove(moveSt);
        CalculateMoveSkillEffect(moveSt);
        UI_Manager.Instance.distanceBar.SetDistance(range);
        Debug.Log(range);
        GameCore.Instance.my_playerData.range = range;
        GameCore.Instance.enemy_playerData.range = range;
    }

    public void Skill_Move(MoveStruct moveSt)
    {
        var range = GameCore.Instance.my_playerData.GetSkillMove(GameCore.Instance.playerManager.my_index, moveSt);
        UI_Manager.Instance.distanceBar.SetDistance(range);
        Debug.Log(range);
        GameCore.Instance.my_playerData.range = range;
        GameCore.Instance.enemy_playerData.range = range;
    }

    public void Send_DealDamage(int num)
    {

        if (GameCore.Instance.isFake)
        {
            //ChangePlayer();
            //Receive_DealDamage(num);
        }
        else
        {
            GameCore.Instance.playerManager.Send_DealDamage(num);
            GameCore.Instance.phaseController.CallNextStep();
        }
    }

    //public void Receive_DealDamage(int ownerIndex, int num)
    public void Receive_DealDamage(int num)
    {
        
        //GameCore.Instance.UI_manager.UI_stateBar.SetMyAtk(0, false);
        //GameCore.Instance.UI_manager.UI_stateBar.SetEnemyDef(0, false);

        GameCore.Instance.my_playerData.enemy_character.GetHit(num);
        //GameCore.Instance.UI_manager.UI_stateBar.SetGetHurt(ownerIndex, num);
        CalculateSkillEffect(GameCore.Instance.my_playerData.enemy_character, num, my_use_attack_cards, en_use_defense_cards, my_atk_skillEffect, en_def_skillEffect);

        GameCore.Instance.UI_manager.UI_stateBar.ResetArgs();
        GameCore.Instance.UI_manager.changeCharacterPanel.SetParameter();
        my_use_attack_cards.Clear();
        en_use_defense_cards.Clear();

        //GameCore.Instance.phaseController.CallNextStep();
        
    }

    public void Heal(Character character, int num)
    {
        character.GetHeal(num);
    }

    public void Add_DirectDamage(Character character, int num)
    {
        Debug.Log("ADD DirectDamage:<color=red>" + num + " </color>to: " + character);
        character.GetDirectHit(num);
        if (character == GameCore.Instance.my_playerData.my_character || character == GameCore.Instance.my_playerData.enemy_character)
        {
            GameCore.Instance.UI_manager.UI_stateBar.SetGetHurt(character.ownerIndex, num);
        }
    }

    public void Send_ChangeCharacter(int char_index)
    {
        /*
        Character my_character = GameCore.Instance.my_playerData.ChangeCharacter(char_index);
        GameCore.Instance.UI_manager.UI_skillBar.SetMySkillBar(my_character);
        GameCore.Instance.UI_manager.changeCharacterPanel.SetParameter();
        GameCore.Instance.UI_manager.UI_stateBar.SetCharacterName(my_character);
        GameCore.Instance.UI_manager.CheckAllArgs();
        GameCore.Instance.UI_manager.UI_stateBar.ClearBuffs(GameCore.Instance.playerManager.my_index);
        GameCore.Instance.UI_manager.UI_stateBar.SetGetBuff(my_character);

        if (GameCore.Instance.isFake)
        {
            ChangePlayer();
            GameCore.Instance.phaseController.CallNextStep();
            Receive_ChangeCharacter(GameCore.Instance.my_playerData.My_index, char_index);
        }
        else
        {
            GameCore.Instance.playerManager.Send_ChangeCharacter(char_index);
        }
        */
        battleMech.SendChangeChar(char_index);
    }

    /*
    public void Receive_ChangeCharacter(int ownerIndex, int char_index)
    {
        Character en_character = GameCore.Instance.my_playerData.Online_ChangeCharacter(char_index);
        GameCore.Instance.UI_manager.UI_stateBar.ClearBuffs(ownerIndex);
        GameCore.Instance.UI_manager.UI_stateBar.SetGetBuff(en_character);
        UI_Manager.Instance.UI_skillBar.SetEnSkillBar(en_character);
    }
    */
    public void Receive_ChangeCharacter(int my_char_index, int en_char_index)
    {
        battleMech.ReceiveChangeChar(my_char_index, en_char_index);
    }

    public void Add_SkillBuff(TargetFlag targetFlag, SkillBuff skillBuff)
    {
        switch (targetFlag)
        {
            case TargetFlag.My_char:
                GameCore.Instance.my_playerData.my_character.AddBuff(skillBuff);
                break;
            case TargetFlag.En_char:
                GameCore.Instance.my_playerData.enemy_character.AddBuff(skillBuff);
                break;
        }
    }

    public void Add_SkillBuff(Character character, SkillBuff skillBuff)
    {
        character.AddBuff(skillBuff);
        GameCore.Instance.UI_manager.UI_stateBar.SetGetBuff(character, skillBuff);
    }

    public void Online_Add_SkillBuff(int ownerIndex, Character character, SkillBuff skillBuff)
    {

    }

    void CalculateSkillEffect(Character character, int num, List<CardData> atk_cards, List<CardData> def_cards, List<SkillEffect> ATK_skills, List<SkillEffect> DEF_skills)
    {
        SkillDetail SD = new SkillDetail();
        SD.hitter = character;
        SD.damage = num;
        SD.atk_cards = atk_cards;
        SD.Def_cards = def_cards;
        for (int i = 0; i < ATK_skills.Count; i++)
        {
            ATK_skills[i].CheckEffect(SD);
        }
        for (int i = 0; i < DEF_skills.Count; i++)
        {
            DEF_skills[i].CheckEffect(SD);
        }
        ATK_skills.Clear();
        DEF_skills.Clear();
    }

    //void CalculateMoveSkillEffect(Character character, int num)
    void CalculateMoveSkillEffect(MoveStruct moveSt)
    {
        if (GameCore.Instance.isFake)
        {
            ChangePlayer();
        }
        for (int i = 0; i < my_move_skillEffect.Count; i++)
        {
            SkillDetail SD = new SkillDetail()
            {
                caster = my_move_skillEffect[i].caster,
                hitter = my_move_skillEffect[i].hitter,
                damage = moveSt.num,
                atk_cards = my_move_skillEffect[i].cards
            };
            if (SD.caster != GameCore.Instance.my_playerData.my_character) { Debug.Log("Wrong"); }
            my_move_skillEffect[i].CheckEffect(SD);
        }
        my_move_skillEffect.Clear();
        if (GameCore.Instance.isFake)
        {
            ChangePlayer(0.1f);
        }
        for (int i = 0; i < en_move_skillEffect.Count; i++)
        {
            SkillDetail SD = new SkillDetail()
            {
                caster = en_move_skillEffect[i].caster,
                hitter = en_move_skillEffect[i].hitter,
                damage = moveSt.num,
                atk_cards = en_move_skillEffect[i].cards
            };
            en_move_skillEffect[i].CheckEffect(SD);
        }
        en_move_skillEffect.Clear();
    }

    public IEnumerator FakeAttachDamage()
    {
        Debug.Log("Waiting for Damage");
        //yield return new WaitForSeconds(GameDefinition.Instance.ATTACK_TIME);
        yield return null;
        int dmg = CalculateDamage();
        Debug.Log("AttachDamage: " + dmg);
        CalculateSkillEffect(GameCore.Instance.my_playerData.my_character, dmg, en_use_attack_cards, my_use_defense_cards, en_atk_skillEffect, my_def_skillEffect);
        en_use_attack_cards.Clear();
        my_use_defense_cards.Clear();
        UI_Manager.Instance.UI_MyUsedCard.AllToTrashCan();
        UI_Manager.Instance.enemy_hand.UsedCardToTrashcan();

        GameCore.Instance.my_playerData.GetHit(dmg);
        GameCore.Instance.UI_manager.UI_stateBar.SetGetHurt(GameCore.Instance.playerManager.my_index, dmg);
        Send_DealDamage(dmg);

        GameCore.Instance.UI_manager.UI_stateBar.ResetArgs();
        GameCore.Instance.UI_manager.changeCharacterPanel.SetParameter();
    }

    public void AttachDamage()
    {

    }

    IEnumerator EmptyDefense(List<SkillEffect> defense_skills)
    {
        Debug.Log("Waiting for Defense");
        //yield return new WaitForSeconds(GameDefinition.Instance.DEFENSE_TIME);
        yield return null;
        CalculateSkillEffect(GameCore.Instance.my_playerData.my_character, 0, new List<CardData>(), my_use_defense_cards, new List<SkillEffect>(), defense_skills);
        my_use_defense_cards.Clear();
        GameCore.Instance.UI_manager.UI_stateBar.ResetArgs();
        GameCore.Instance.UI_manager.changeCharacterPanel.SetParameter();
    }

    public void ChangePlayer(float waitTime = 0.1f)
    {
        var tmpEN = GameCore.Instance.my_playerData;
        var tmpMy = GameCore.Instance.enemy_playerData;

        Debug.LogFormat("<color=blue> Change Player to {0}</color>", tmpMy.My_index);

        
        List<CardData> tmpMyCards = new List<CardData>();
        tmpMyCards.AddRange(GameCore.Instance.UI_manager.UI_Hand.cards);
        List<CardData> tmpEnCards = new List<CardData>();
        tmpEnCards.AddRange(GameCore.Instance.UI_manager.enemy_hand.UI_enHand.cards);

        GameCore.Instance.UI_manager.UI_Hand.ClearCards();
        GameCore.Instance.UI_manager.enemy_hand.UI_enHand.ClearCards();

        GameCore.Instance.UI_manager.enemy_hand.UI_enHand.AddCards(tmpMyCards);
        GameCore.Instance.UI_manager.UI_Hand.AddCards(tmpEnCards);

        GameCore.Instance.UI_manager.enemy_hand.UI_enHand.MoveAllCardImmediately();
        GameCore.Instance.UI_manager.enemy_hand.UI_enHand.SetAllCardsToBack();
        GameCore.Instance.UI_manager.UI_Hand.SetAllCardsToFront();

        tmpMyCards.Clear();
        tmpEnCards.Clear();

        tmpMyCards.AddRange(GameCore.Instance.UI_manager.UI_MyUsedCard.cards);
        GameCore.Instance.UI_manager.UI_MyUsedCard.ClearCards();
        GameCore.Instance.UI_manager.enemy_hand.UI_EnUsedCard.AddCards(tmpMyCards);
        GameCore.Instance.UI_manager.enemy_hand.UI_EnUsedCard.SetAllCardsToBack();
        /*
        foreach (CardData card in GameCore.Instance.my_playerData.hand_cards)
        {
            card.Card_OB.transform.SetParent(GameCore.Instance.cardManager.garbageCan.transform);
        }
        */
        GameCore.Instance.my_playerData = tmpMy;
        GameCore.Instance.enemy_playerData = tmpEN;

        //StartCoroutine(DelayChangePlayer(waitTime));
        UI_Manager.Instance.RefreshAllUI();
    }

    public void ChangePlayer(int index)
    {
        if (GameCore.Instance.my_playerData.My_index == index) { return; }
        else { ChangePlayer(); }
    }

    IEnumerator DelayChangePlayer(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);

        //UI_Manager.Instance.RefreshAllUI();
    }

    public void StartAttack(int firster)
    {
        //CalculateMoveSkillEffect();

        if (GameCore.Instance.isFake)
        {
            if (firster != GameCore.Instance.my_playerData.My_index) { ChangePlayer(0.1f); }
            GameCore.Instance.phaseController.CallNextStep();
        }
        else
        {
            GameCore.Instance.phaseController.CallNextStep();
        }
    }
}
