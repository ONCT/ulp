﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BattleStateMachine : MonoBehaviour
{
    abstract public void Initial();
    abstract public void StartPreparePhase(Action act);
    abstract public void StartDrawPhase();
    abstract public void StartMovePhase();
    abstract public void StartWaitingChangeCharPhase();
    abstract public void StartChangeCharPhase();
    abstract public void StartAttackPhase();
    abstract public void StartDefensePhase();
    abstract public void StartEndPhase();

    abstract public void SendMoveCard(string cards,int moveType);
    abstract public void ReceiveMoveCommand(int firster, MoveStruct my_move, MoveStruct en_move, MoveStruct total_move, List<CardData> my_useCards, List<CardData> en_useCards);
    abstract public void SendAtkCard(string cards);
    abstract public void ReceiveAtkCard(List<CardData> cards);
    abstract public void SendDefCard(string cards);
    abstract public void ReceiveDefCard(List<CardData> cards);
    abstract public void SendChangeChar(int change_index);
    abstract public void ReceiveChangeChar(int my_index, int enemy_index);

    abstract public void DrawDeafaltCard();
}
