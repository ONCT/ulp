﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleBattle : BattleStateMachine
{
    private IEnumerator cor_WaitandChange;
    Dictionary<byte, object> parameter = new Dictionary<byte, object>();

    public override void Initial()
    {
        GameCore.Instance.my_playerData.RefreshAllPhase();
        GameCore.Instance.enemy_playerData.RefreshAllPhase();
    }

    public override void StartPreparePhase(Action act)
    {
        GameCore.Instance.my_playerData.AddPhase(act);

        Debug.Log(string.Format("Start {0}'s preparePhase", GameCore.Instance.my_playerData.My_index));

        GameCore.Instance.battleManager.ChangePlayer();

        GameCore.Instance.phaseController.CallNextStep();
    }

    public override void StartDrawPhase()
    {
        GameCore.Instance.cardManager.DealDefaultCard();
        GameCore.Instance.cardManager.DrawEventCard();
        Debug.Log(string.Format("Start {0}'s DrawPhase", GameCore.Instance.my_playerData.My_index));

        //GameCore.Instance.my_playerData.phase_list.Add(GameCore.Instance.phaseController.StartMovePhase);
        GameCore.Instance.my_playerData.AddPhase(GameCore.Instance.phaseController.StartMovePhase);


        GameCore.Instance.battleManager.ChangePlayer();

        GameCore.Instance.phaseController.CallNextStep();
    }

    public override void StartMovePhase()
    {

    }

    public override void StartAttackPhase()
    {
        UI_Manager.Instance.ChangeBase(BattlePhase.Attack);
        UI_Manager.Instance.UI_stateBar.battleMiddleInfo.SetATKandDEFBar(BattlePhase.Attack);
        UI_Manager.Instance.UI_stateBar.battleMiddleInfo.SetMyAtk(GameCore.Instance.my_playerData.my_character.GetATK());
        UI_Manager.Instance.UI_stateBar.battleMiddleInfo.SetEnemyDef(GameCore.Instance.my_playerData.enemy_character.GetDEF());
    }

    public override void StartWaitingChangeCharPhase()
    {
        //CallNextStepWithChange();
        //SendChangeChar(GameCore.Instance.my_playerData.my_character.char_index);
        parameter = new Dictionary<byte, object>() { { BattleBehaviorDefinitin.SEND_CHANGE_CHAR, GameCore.Instance.my_playerData.my_character.char_index }, { BattleBehaviorDefinitin.FUNCTION_POINTER, BattleBehaviorDefinitin.SEND_CHANGE_CHAR } };
        StartCoroutine(WaitandSend(parameter));
    }

    public override void StartChangeCharPhase()
    {
        GameCore.Instance.UI_manager.changeCharacterPanel.TurnOnOff(true);
    }

    public override void StartDefensePhase()
    {
        UI_Manager.Instance.ChangeBase(BattlePhase.Defense);
        UI_Manager.Instance.UI_stateBar.battleMiddleInfo.SetATKandDEFBar(BattlePhase.Defense);
        UI_Manager.Instance.UI_stateBar.battleMiddleInfo.SetMyDef(GameCore.Instance.my_playerData.my_character.GetDEF());
        //UI_Manager.Instance.UI_stateBar.battleMiddleInfo.SetEnemyAtk(GameCore.Instance.my_playerData.enemy_character.GetDEF());

    }

    public override void StartEndPhase()
    {
        throw new NotImplementedException();
    }

    private int moveCount = 0;
    public override void SendMoveCard(string cards, int moveType)
    {
        Debug.Log(string.Format("Start {0}'s SendMove", GameCore.Instance.my_playerData.My_index));

        parameter = new Dictionary<byte, object>() { { BattleBehaviorDefinitin.USE_MOV_CARD, cards }, { BattleBehaviorDefinitin.FUNCTION_POINTER, BattleBehaviorDefinitin.USE_MOV_CARD }, { BattleBehaviorDefinitin.MOVE_TYPE, moveType } };


        moveCount++;
        if (moveCount % 2 == 1)
        {
            cor_WaitandChange = WaitandSendandChange(parameter);
            StartCoroutine(cor_WaitandChange);
            StartCoroutine(Cor_CallNextStep());
        }
        else
        {
            cor_WaitandChange = WaitandSend(parameter);
            StartCoroutine(cor_WaitandChange);
        }
        needChangeCount = 0;
        changeCount = 0;
    }

    int firster;
    List<int> changer = new List<int>();
    public override void ReceiveMoveCommand(int firster, MoveStruct my_move, MoveStruct en_move, MoveStruct total_move, List<CardData> my_useCards, List<CardData> en_useCards)
    {
        //GameCore.Instance.battleManager.ChangePlayer(firster);
        this.firster = firster;
        changer = new List<int>();
        Debug.LogFormat("<color=blue> ReceiveMoveCommand, firster : {0} </color>", firster);

        if (my_move.moveType != MoveType.Change && en_move.moveType == MoveType.Change)
        {
            GameCore.Instance.my_playerData.AddPhase(GameCore.Instance.phaseController.StartWaitingChangePhase);
            GameCore.Instance.enemy_playerData.AddPhase(GameCore.Instance.phaseController.StartChangePhase);
            changer.Add(GameCore.Instance.enemy_playerData.My_index);
            needChangeCount = 1;
        }
        else if (my_move.moveType == MoveType.Change && en_move.moveType != MoveType.Change)
        {
            GameCore.Instance.my_playerData.AddPhase(GameCore.Instance.phaseController.StartChangePhase);
            GameCore.Instance.enemy_playerData.AddPhase(GameCore.Instance.phaseController.StartWaitingChangePhase);
            changer.Add(GameCore.Instance.my_playerData.My_index);
            needChangeCount = 1;
        }
        else if (my_move.moveType == MoveType.Change && en_move.moveType == MoveType.Change)
        {
            GameCore.Instance.my_playerData.AddPhase(GameCore.Instance.phaseController.StartChangePhase);
            GameCore.Instance.enemy_playerData.AddPhase(GameCore.Instance.phaseController.StartChangePhase);
            changer.Add(GameCore.Instance.my_playerData.My_index);
            changer.Add(GameCore.Instance.enemy_playerData.My_index);
            needChangeCount = 2;
        }

        if (firster == GameCore.Instance.my_playerData.My_index)
        {
            GameCore.Instance.my_playerData.AddPhase(GameCore.Instance.phaseController.StartAttackPhase);
            GameCore.Instance.my_playerData.AddPhase(GameCore.Instance.phaseController.StartDefensePhase);

            GameCore.Instance.enemy_playerData.AddPhase(GameCore.Instance.phaseController.StartDefensePhase);
            GameCore.Instance.enemy_playerData.AddPhase(GameCore.Instance.phaseController.StartAttackPhase);
        }
        else
        {
            GameCore.Instance.my_playerData.AddPhase(GameCore.Instance.phaseController.StartDefensePhase);
            GameCore.Instance.my_playerData.AddPhase(GameCore.Instance.phaseController.StartAttackPhase);

            GameCore.Instance.enemy_playerData.AddPhase(GameCore.Instance.phaseController.StartAttackPhase);
            GameCore.Instance.enemy_playerData.AddPhase(GameCore.Instance.phaseController.StartDefensePhase);
        }


        var my_trigger_skill = GameCore.Instance.my_playerData.my_character.GetSkillEffect(my_useCards, CardArea.Move);
        var en_trigger_skill = GameCore.Instance.enemy_playerData.my_character.GetSkillEffect(en_useCards, CardArea.Move);


        //進戰階
        Action callNextStep = CallNextStepWithChange;
        //移動量
        Action show_move = GameCore.Instance.UI_manager.distanceBar.ShowMove;
        //丟牌效果
        Action discard = GameCore.Instance.cardManager.BothUsedCardToTrashcan;
        //技能表演
        Action showSkill = GameCore.Instance.UI_manager.UI_skillBar.ShowSkillPerformance;


        //技能表演
        GameCore.Instance.UI_manager.UI_skillBar.AddPrepareShowEffect(my_trigger_skill, en_trigger_skill, discard);
        //丟牌效果
        GameCore.Instance.cardManager.AddNextStep(show_move);
        //移動量
        GameCore.Instance.UI_manager.distanceBar.AddPrepareMove(my_move, en_move, total_move, callNextStep);

        //翻牌
        GameCore.Instance.UI_manager.enemy_hand.OpenAllUsed(showSkill);
    }

    public override void SendAtkCard(string cards)
    {
        Debug.Log(string.Format("Start {0}'s SendAtk", GameCore.Instance.my_playerData.My_index));

        parameter = new Dictionary<byte, object>() { { BattleBehaviorDefinitin.SEND_ATK_CARD, cards }, { BattleBehaviorDefinitin.FUNCTION_POINTER, BattleBehaviorDefinitin.SEND_ATK_CARD } };

        cor_WaitandChange = WaitandSendandChange(parameter);
        StartCoroutine(cor_WaitandChange);
    }

    public override void ReceiveAtkCard(List<CardData> cards)
    {
        GameCore.Instance.UI_manager.enemy_hand.HandToUsedandOpen(cards, CallNextStep);

        //StartCoroutine(Cor_CallNextStep());
    }

    public override void SendDefCard(string cards)
    {
        throw new NotImplementedException();
    }

    public override void ReceiveDefCard(List<CardData> cards)
    {
        throw new NotImplementedException();
    }

    private int needChangeCount = 0;
    private int changeCount = 0;
    public override void SendChangeChar(int change_index)
    {
        Debug.Log(string.Format("Start {0}'s Send ChangeChar", GameCore.Instance.my_playerData.My_index));

        parameter = new Dictionary<byte, object>() { { BattleBehaviorDefinitin.SEND_CHANGE_CHAR, change_index }, { BattleBehaviorDefinitin.FUNCTION_POINTER, BattleBehaviorDefinitin.SEND_CHANGE_CHAR } };

        //cor_WaitandChange = WaitandSendandChange(parameter);

        changeCount++;
        if (changeCount < needChangeCount)
        {
            StartCoroutine(WaitandSendandChange(parameter));
            StartCoroutine(Cor_CallNextStep());
        }
        else
        {
            StartCoroutine(WaitandSend(parameter));
        }
    }

    public override void ReceiveChangeChar(int my_index, int enemy_index)
    {
        Debug.Log(string.Format("Start {0}'s Receive ChangeChar", GameCore.Instance.my_playerData.My_index));

        CallNextStepWithChange();
    }

    public override void DrawDeafaltCard()
    {
        throw new NotImplementedException();
    }

    #region ---Single Only---
    private IEnumerator WaitandSendandChange(Dictionary<byte, object> parameter)
    {
        yield return new WaitForSeconds(0.5f);

        FakeServer.Instance.ReceiveRequest(ProtocolDefinition.BATTLE, parameter);

        GameCore.Instance.battleManager.ChangePlayer();
    }

    private IEnumerator WaitandSend(Dictionary<byte, object> parameter)
    {
        yield return new WaitForSeconds(1.0f);
        FakeServer.Instance.ReceiveRequest(ProtocolDefinition.BATTLE, parameter);
    }

    public void CallNextStep()
    {
        StartCoroutine(Cor_CallNextStep());
    }

    private IEnumerator Cor_CallNextStep()
    {
        yield return new WaitForSeconds(1.0f);
        GameCore.Instance.phaseController.CallNextStep();
    }

    private void CallNextStepWithChange()
    {
        if (changer.Count > 1)
        {
            Debug.LogError("Change To Changer");

            int changerIndex = changer[0];
            changer.RemoveAt(0);
            GameCore.Instance.battleManager.ChangePlayer(changerIndex);
        }
        else
        {
            Debug.LogError("Change To Firster");

            GameCore.Instance.battleManager.ChangePlayer(firster);
        }
        StartCoroutine(Cor_CallNextStep());
    }
    #endregion
}
