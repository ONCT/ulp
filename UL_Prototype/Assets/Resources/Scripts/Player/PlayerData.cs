﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RangeType
{
    shortRange = 1,
    middleRange = 2,
    longRange = 3,
    none,
    all,
    s_m,
    s_l,
    m_l
}

public class MoveDetail
{
    public int mover_index;
    public float move_speed;
    public float move_time;
    public float move_time_now;
}

public class PlayerData : MonoBehaviour
{
    public List<CardData> attack_cards = new List<CardData>();
    public List<CardData> defense_cards = new List<CardData>();
    public List<CardData> move_cards = new List<CardData>();
    public List<CardData> hand_cards = new List<CardData>();
    public int card_count;
    public int My_index;
    public List<Character> my_charList = new List<Character>();
    public Character my_character;
    public List<Character> enemy_charList = new List<Character>();
    public Character enemy_character;
    public List<GameObject> all_char_list = new List<GameObject>();
    public int distance = 2;
    public RangeType range;
    DataBase DB_Char;
    public GameObject character_prefabs;
    public float move_speed;
    public List<MoveDetail> move_list = new List<MoveDetail>();
    [SerializeField]
    public List<Action> phase_list = new List<Action>();
    public bool isChange;

    public void Initial()
    {
        //DB_Char = DataBaseManager.Instance.DB_CharcterData;
        //List<string> keys = DB_Char.GetKeyList();
        foreach (GameObject GO in all_char_list)
        {
            Destroy(GO);
        }
        all_char_list.Clear();
        my_charList.Clear();
        enemy_charList.Clear();

        range = RangeType.middleRange;
    }

    public void AddCard(CardArea area, CardData card)
    {
        switch (area)
        {
            case CardArea.None:
                break;
            case CardArea.Hand:
                hand_cards.Add(card);
                break;
            case CardArea.Attack:
                attack_cards.Add(card);
                break;
            case CardArea.Defense:
                defense_cards.Add(card);
                break;
            case CardArea.Move:
                move_cards.Add(card);
                break;
            default:
                break;
        }
    }

    public void RemoveCard(CardArea area, CardData card)
    {
        switch (area)
        {
            case CardArea.None:
                break;
            case CardArea.Hand:
                hand_cards.Remove(card);
                break;
            case CardArea.Attack:
                attack_cards.Remove(card);
                break;
            case CardArea.Defense:
                defense_cards.Remove(card);
                break;
            case CardArea.Move:
                move_cards.Remove(card);
                break;
            default:
                break;
        }
    }

    public void ClearCard(CardArea area)
    {
        switch (area)
        {
            case CardArea.None:
                break;
            case CardArea.Hand:
                hand_cards.Clear();
                break;
            case CardArea.Attack:
                attack_cards.Clear();
                break;
            case CardArea.Defense:
                defense_cards.Clear();
                break;
            case CardArea.Move:
                move_cards.Clear();
                break;
            default:
                break;
        }
    }

    public void SendCardToGrave()
    {
        GameCore.Instance.cardManager.UseCard(attack_cards);
        GameCore.Instance.cardManager.UseCard(defense_cards);
        GameCore.Instance.cardManager.UseCard(move_cards);
        GameCore.Instance.cardManager.UseCard(hand_cards);

        UI_Manager.Instance.UI_Hand.ClearCards();
        UI_Manager.Instance.UI_Move.ClearCards();
        UI_Manager.Instance.UI_Attack.ClearCards();
        UI_Manager.Instance.UI_Defense.ClearCards();

        attack_cards.Clear();
        defense_cards.Clear();
        move_cards.Clear();
        hand_cards.Clear();
    }

    public void RefreshAllPhase()
    {
        phase_list = new List<Action> { GameCore.Instance.phaseController.StartPreparePhase };
    }

    public void EndTurn()
    {
        phase_list = new List<Action> { GameCore.Instance.phaseController.StartEndPhase, GameCore.Instance.phaseController.StartPreparePhase };
    }

    public void AddPhase(Action phase)
    {
        phase_list.Add(phase);
    }

    public void ReStart()
    {
        List<string> keys = DB_Char.GetKeyList();
        foreach (GameObject GO in all_char_list)
        {
            Destroy(GO);
        }
        all_char_list.Clear();
        my_charList.Clear();
        enemy_charList.Clear();
        range = RangeType.middleRange;
    }

    public void GetHit(int num)
    {
        if (num < 0) { num = 0; }
        my_character.GetHit(num);
    }

    public int live_hero_count = 0;
    public void CharacterDie(int char_index)
    {
        live_hero_count = 0;
        for (int i = 0; i < my_charList.Count; i++)
        {
            if (my_charList[i].Now_HP > 0) { live_hero_count++; }
        }
        if (live_hero_count > 0)
        {
            //GameCore.Instance.UI_manager.UI_changeCharacterPanel.TurnOnOff(true);
            phase_list.Clear();
            GameCore.Instance.enemy_playerData.phase_list.Clear();
            AddPhase(GameCore.Instance.phaseController.StartChangePhase);
            isChange = true;
            AddPhase(GameCore.Instance.phaseController.StartEndPhase);
            //EndTurn();
            //GameCore.Instance.enemy_playerData.EndTurn();
            GameCore.Instance.enemy_playerData.phase_list.Clear();
            GameCore.Instance.enemy_playerData.AddPhase(GameCore.Instance.phaseController.StartEndPhase);

            GameCore.Instance.phaseController.CallNextStep();
        }
        else
        {
            GameCore.Instance.playerManager.Send_Lose();
        }
    }

    public Character ChangeCharacter(int char_index)
    {
        Vector3 V3 = my_character.transform.position;
        my_character.transform.position = new Vector3(999, 999, 999);
        my_character.isOnBoard = false;
        my_character = my_charList[char_index];
        my_character.transform.position = V3;
        my_character.isOnBoard = true;

        return my_character;
    }

    public Character Online_ChangeCharacter(int char_index)
    {
        Character cha = enemy_charList.Find(x => x.char_index == char_index);
        if (cha == null) { Debug.Log("Something Wrong"); return null; }
        Vector3 V3 = enemy_character.transform.position;
        enemy_character.transform.position = new Vector3(999, 999, 999);
        enemy_character.isOnBoard = false;
        enemy_character = cha;
        enemy_character.transform.position = V3;
        enemy_character.isOnBoard = true;
        GameCore.Instance.UI_manager.UI_stateBar.SetCharacterName(enemy_character);

        return enemy_character;
    }

    public RangeType GetMove(MoveStruct moveSt)
    {
        int move_dis = 0;
        var moveType = moveSt.moveType;
        switch (moveType)
        {
            case MoveType.Forward:
                move_dis = moveSt.num > distance - (int)RangeType.shortRange ? distance - (int)RangeType.shortRange : moveSt.num;
                distance = distance - move_dis;
                break;
            case MoveType.Back:
                move_dis = moveSt.num > (int)RangeType.longRange - distance ? (int)RangeType.longRange - distance : moveSt.num;
                distance = distance + move_dis;
                break;
        }

        StartCoroutine(CharacterMove());

        return (RangeType)distance;
    }

    public RangeType GetSkillMove(int ownerIndex,MoveStruct moveSt)
    {
        int move_dis = 0;
        var moveType = moveSt.moveType;
        switch (moveType)
        {
            case MoveType.Forward:
                move_dis = moveSt.num > distance - (int)RangeType.shortRange ? distance - (int)RangeType.shortRange : moveSt.num;
                distance = distance - move_dis;
                break;
            case MoveType.Back:
                move_dis = moveSt.num > (int)RangeType.longRange - distance ? (int)RangeType.longRange - distance : moveSt.num;
                distance = distance + move_dis;
                break;
        }

        StartCoroutine(CharacterMove());

        return (RangeType)distance;
    }

    public void MoveCharacter(MoveType moveType, int num)
    {

    }

    public void SetDistance(RangeType range)
    {
        if (this.range != range)
        {
            this.range = range;
            GameCore.Instance.UI_manager.CheckAllArgs();
        }
    }

    public void SetHero(List<string> heros)
    {
        for (int i = 0; i < heros.Count; i++)
        {
            GameObject clone = Instantiate(character_prefabs);
            Character cha = clone.GetComponent<Character>();
            clone.name = "Character_" + heros[i];
            clone.transform.position = new Vector3(999, 999, 999);
            cha.Initial(heros[i], this.My_index);
            cha.char_index = i;
            my_charList.Add(cha);
            all_char_list.Add(clone);
        }

        my_character = my_charList[0];
        my_character.isOnBoard = true;
        GameCore.Instance.UI_manager.UI_stateBar.myStateBar.my_char_OB = my_character.gameObject;
        GameCore.Instance.UI_manager.UI_stateBar.SetCharacterName(my_character);
        GameCore.Instance.UI_manager.UI_skillBar.SetMySkillBar(my_character);
        GameCore.Instance.UI_manager.changeCharacterPanel.SetParameter();
        my_character.transform.position = new Vector3(-1.75f, 0, 0);

        string tmp = "";
        for (int i = 0; i < heros.Count; i++)
        {
            tmp += heros[i];
            if (i != heros.Count - 1) { tmp += " "; }
        }
        GameCore.Instance.playerManager.Send_SelectHeroes(tmp);
        live_hero_count = my_charList.Count;

        if (my_character != null && enemy_character != null)
        {
            //GameCore.Instance.GameStart();
            GameCore.Instance.playerManager.GameStart();
        }
    }

    public void Online_SetHero(int ownerIndex, List<string> heroes)
    {
        for (int i = 0; i < heroes.Count; i++)
        {
            GameObject clone = Instantiate(character_prefabs);
            Character cha = clone.GetComponent<Character>();
            clone.name = "Character_" + heroes[i];
            clone.transform.position = new Vector3(999, 999, 999);
            cha.Initial(heroes[i], ownerIndex);
            cha.char_index = i;
            enemy_charList.Add(cha);
            all_char_list.Add(clone);
        }

        enemy_character = enemy_charList[0];
        enemy_character.isOnBoard = true;
        GameCore.Instance.UI_manager.UI_stateBar.en_char_OB = enemy_character.gameObject;
        GameCore.Instance.UI_manager.UI_stateBar.SetCharacterName(enemy_character);
        GameCore.Instance.UI_manager.UI_skillBar.SetEnSkillBar(enemy_character);
        enemy_character.transform.GetComponent<MeshRenderer>().material = Resources.Load<Material>("Material/PureRed") as Material;
        enemy_character.transform.position = new Vector3(1.75f, 0, 0);

        if (my_character != null && enemy_character != null)
        {
            //GameCore.Instance.enemy_playerData = this;
            //GameCore.Instance.GameStart();
            GameCore.Instance.playerManager.GameStart();
        }
    }

    IEnumerator CharacterMove()
    {
        while (my_character.transform.position != new Vector3(distance, 0, 0))
        {
            yield return null;
            my_character.transform.position = Vector3.MoveTowards(my_character.transform.position, new Vector3(-distance, 0, 0), 10.0f);
            enemy_character.transform.position = Vector3.MoveTowards(enemy_character.transform.position, new Vector3(distance, 0, 0), 10.0f);
        }
    }
}
