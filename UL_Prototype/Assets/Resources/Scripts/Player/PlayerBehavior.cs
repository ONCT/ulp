﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum MontageFlag
{
    none,
    card,
}

public class PlayerBehavior : MonoBehaviour
{

    public MontageFlag montageFlag;
    public CardData selectedCard;
    public CardArea remainberCardArea;
    public List<CardData> all_owner_cards = new List<CardData>();
    public UI_CardBase selectArea;
    public CardArea selectedCardArea;

    public UI_HandBase handBase;
    public UI_AttackBase attackBase;
    public UI_DefenseBase defenseBase;
    public UI_MoveBase moveBase;

    private const float TURN_DISTANCE = 5.0f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            switch (montageFlag)
            {
                case MontageFlag.none:
                    break;
                case MontageFlag.card:
                    GetMousePosition();
                    break;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            switch (montageFlag)
            {
                case MontageFlag.none:
                    break;
                case MontageFlag.card:
                    PutDownCard();
                    break;
            }
        }
    }

    public void DrawCard(CardData card)
    {
        GameCore.Instance.my_playerData.AddCard(CardArea.Hand, card);
        UI_Manager.Instance.UI_Hand.AddCard(card);
        card.ChangeBelone(CardArea.Hand);
    }

    Ray ray;
    RaycastHit hit;
    Vector2 mouse_pos;
    public List<RaycastResult> hitObjects = new List<RaycastResult>();
    void GetMousePosition()
    {
        mouse_pos = Input.mousePosition;
        if (selectedCard == null) { return; }
        selectedCard.transform.position = mouse_pos;

        var pointer = new PointerEventData(EventSystem.current);
        pointer.position = Input.mousePosition;

        EventSystem.current.RaycastAll(pointer, hitObjects);
        if (hitObjects.Count <= 0)
        {
            return;
        }
        else
        {
            foreach (RaycastResult RR in hitObjects)
            {
                UI_CardBase UI_CB = RR.gameObject.transform.GetComponent<UI_CardBase>();
                if (UI_CB != null)
                {
                    selectArea = UI_CB;
                    selectedCardArea = UI_CB.cardArea;
                    break;
                }
            }
        }
    }

    public void GetInCardArea(UI_CardBase CB)
    {
        this.selectArea = CB;
        selectedCardArea = CB.cardArea;
    }

    public void ClickCard(CardData card)
    {
        //selectedCard = all_owner_cards.Find(card);
        remainberCardArea = card.beloneArea;
        montageFlag = MontageFlag.card;
        selectedCard = card;
    }

    Vector2 old_mouse_pos;
    public void TurnCard(CardData card)
    {
        old_mouse_pos = Input.mousePosition;
        float distance = Vector2.Distance(old_mouse_pos, mouse_pos);
        if (distance < TURN_DISTANCE)
        {
            card.TurnCard();
        }
    }

    public void PutDownCard()
    {
        GameCore.Instance.my_playerData.RemoveCard(remainberCardArea, selectedCard);
        if (remainberCardArea != selectedCardArea)
        {
            switch (remainberCardArea)
            {
                case CardArea.Hand:
                    UI_Manager.Instance.UI_Hand.RemoveCard(selectedCard);
                    break;
                case CardArea.Attack:
                    UI_Manager.Instance.UI_Attack.RemoveCard(selectedCard);
                    break;
                case CardArea.Defense:
                    UI_Manager.Instance.UI_Defense.RemoveCard(selectedCard);
                    break;
                case CardArea.Move:
                    UI_Manager.Instance.UI_Move.RemoveCard(selectedCard);
                    break;
                default:
                    Debug.Log("Something Wrong: ReaminArea: " + remainberCardArea);
                    break;
            }

            GameCore.Instance.my_playerData.AddCard(selectedCardArea, selectedCard);
            switch (selectedCardArea)
            {
                case CardArea.Hand:
                    UI_Manager.Instance.UI_Hand.AddCard(selectedCard);
                    break;
                case CardArea.Attack:
                    UI_Manager.Instance.UI_Attack.AddCard(selectedCard);
                    break;
                case CardArea.Defense:
                    UI_Manager.Instance.UI_Defense.AddCard(selectedCard);
                    break;
                case CardArea.Move:
                    UI_Manager.Instance.UI_Move.AddCard(selectedCard);
                    break;
                default:
                    Debug.Log("Something Wrong");
                    break;
            }
        }
        else
        {
            UI_Manager.Instance.UI_Hand.Refresh();
            UI_Manager.Instance.UI_Attack.Refresh();
            UI_Manager.Instance.UI_Defense.Refresh();
            UI_Manager.Instance.UI_Move.Refresh();
        }

        selectedCard.ChangeBelone(selectedCardArea);
        selectedCard = null;
        selectedCardArea = CardArea.None;
        montageFlag = MontageFlag.none;
        CheckAllState();
    }

    public void AutoPutCard(CardData card, CardArea cardArea)
    {
        GameCore.Instance.my_playerData.RemoveCard(CardArea.Hand, card);
        UI_Manager.Instance.UI_Hand.RemoveCard(card);
        //hand_cards.Remove(card);
        //handBase.RemoveCard(card);

        GameCore.Instance.my_playerData.AddCard(cardArea, card);
        switch (cardArea)
        {
            case CardArea.Hand:
                //hand_cards.Add(card);
                UI_Manager.Instance.UI_Hand.AddCard(selectedCard);
                break;
            case CardArea.Attack:
                //attack_cards.Add(card);
                UI_Manager.Instance.UI_Attack.AddCard(selectedCard);
                break;
            case CardArea.Defense:
                //defense_cards.Add(card);
                UI_Manager.Instance.UI_Defense.AddCard(selectedCard);
                break;
            case CardArea.Move:
                //move_cards.Add(card);
                UI_Manager.Instance.UI_Move.AddCard(selectedCard);
                break;
            default:
                Debug.Log("Something Wrong");
                break;
        }
        card.ChangeBelone(cardArea);
        CheckAllState();

    }

    public void CheckAllState()
    {
        //CheckAttack();
        //CheckDefense();
        //CheckMove();
    }


    public void UseCard(CardArea cardArea)
    {
        GameCore.Instance.my_playerData.ClearCard(cardArea);
        switch (cardArea)
        {
            case CardArea.Attack:
                //attack_cards.Clear();
                UI_Manager.Instance.UI_Attack.CheckArgs();
                break;
            case CardArea.Defense:
                //defense_cards.Clear();
                UI_Manager.Instance.UI_Defense.CheckArgs();
                break;
            case CardArea.Move:
                //move_cards.Clear();
                UI_Manager.Instance.UI_Move.CheckArgs();
                break;
            default:
                Debug.Log("Something Wrong");
                break;
        }
    }
}
