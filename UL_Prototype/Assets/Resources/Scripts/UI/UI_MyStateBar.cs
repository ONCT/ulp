﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_MyStateBar : MonoBehaviour {

    public Text my_name_text;
    public GameObject my_char_OB;
    public Text my_HP_text;

    public List<UI_BattleCharacterDetail> my_back_char = new List<UI_BattleCharacterDetail>();

    public void SetCharacterName(Character character)
    {
        int k = 0;
        my_name_text.text = DataBaseManager.Instance.DB_String.GetValueByString(character.hero_name, "word");
        my_char_OB = character.gameObject;
        for (int i = 0; i < GameCore.Instance.my_playerData.my_charList.Count; i++)
        {
            if (GameCore.Instance.my_playerData.my_charList[i] == character) { continue; }
            my_back_char[k].Initial(GameCore.Instance.my_playerData.my_charList[i]);
            k++;
        }
    }

    public void ResetArgs()
    {
        my_HP_text.text = GameCore.Instance.my_playerData.my_character.Now_HP.ToString();

    }
}
