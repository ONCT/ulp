﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ChangeCharacterPanel : MonoBehaviour
{

    public List<UI_SelectCharDetail> UI_SelectCharDetail;

    public void SetParameter()
    {
        var tmp = GameCore.Instance.my_playerData.my_charList;
        int count = 0;
        for (int i = 0; i < tmp.Count; i++)
        {
            if (tmp[i] == GameCore.Instance.my_playerData.my_character)
            {
                continue;
            }
            else
            {
                UI_SelectCharDetail[count].Initial(tmp[i]);
                count++;
            }
        }
    }

    public void TurnOnOff(bool bo)
    {
        gameObject.SetActive(bo);
    }

    public void OnSelectCharacter(string args)
    {
        Character cha = GameCore.Instance.my_playerData.my_charList.Find(x => x.hero_name == args);
        if (cha.Now_HP <= 0)
        {
            cha = GameCore.Instance.my_playerData.my_charList.Find(x => x.Now_HP > 0);
        }
        int index = cha.char_index;
        //GameCore.Instance.my_playerData.ChangeCharacter(index);
        TurnOnOff(false);
        GameCore.Instance.battleManager.Send_ChangeCharacter(index);
    }

    public void OnClickCancle()
    {
        OnSelectCharacter(GameCore.Instance.my_playerData.my_character.hero_name);
    }
}
