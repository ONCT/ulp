﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Manager : MonoBehaviour
{
    public static UI_Manager Instance
    {
        get { return m_instance; }
    }
    private static UI_Manager m_instance;

    public UI_StateBar UI_stateBar;
    public UI_CardBase UI_Hand;
    public UI_CardBase UI_Attack;
    public UI_CardBase UI_Defense;
    public UI_CardBase UI_Move;
    public UI_UsedCardBase UI_MyUsedCard;
    public UI_EnemyHand enemy_hand;
    public UI_SkillBar UI_skillBar;
    public UI_Deck UI_deck;
    public UI_DistanceBar distanceBar;
    public UI_ChangeCharacterPanel changeCharacterPanel;
    public UI_SelectCharacterPanel selectCharacterPanel;
    public UI_SelectCardPanel selectCardPanel;
    public UI_MainPanel mainPanel;
    public UI_LoadingPanel loadingPanel;

    public void Initial()
    {
        m_instance = this;
        UI_skillBar.Initial();
        selectCharacterPanel.Initial();
    }

    public void CheckAllArgs()
    {
        UI_Attack.CheckArgs();
        UI_Defense.CheckArgs();
        UI_Move.CheckArgs();
    }

    public GameObject WinnerGO;
    public Text Winner_Label;

    public void ChangeBase(BattlePhase phase)
    {
        UI_Attack.Close();
        UI_Defense.Close();
        UI_Move.Close();

        switch (phase)
        {
            case BattlePhase.Draw:
                UI_Move.Show();
                break;
            case BattlePhase.Move:
                UI_Move.Show();
                UI_Move.can_useCard = true;
                break;
            case BattlePhase.Attack:
                UI_Attack.Show();
                UI_Attack.can_useCard = true;
                break;
            case BattlePhase.Defense:
                UI_Defense.Show();
                UI_Defense.can_useCard = true;
                break;
            case BattlePhase.Wait:
                break;
            default:
                break;
        }
        CheckAllArgs();
    }

    public void RefreshAllUI()
    {
        Debug.Log("RefreshAllUI");
        UI_Hand.ClearCards();
        foreach (var card in GameCore.Instance.my_playerData.hand_cards)
        {
            UI_Hand.AddCard(card);
        }
        //UI_Hand.MoveAllCard();
        CheckAllArgs();
        UI_skillBar.SetMySkillBar(GameCore.Instance.my_playerData.my_character);
        UI_skillBar.SetEnSkillBar(GameCore.Instance.my_playerData.enemy_character);
        changeCharacterPanel.SetParameter();

        //刷新中間使用牌區
        var tmp = UI_MyUsedCard.cards;
        UI_MyUsedCard.ClearCards();
        foreach (var v in tmp)
        {
            //UI_EnUsedCard.AddCard(v);
        }

        UI_stateBar.ResetArgs();
    }

    public void SetLoser(int loserIndex)
    {
        WinnerGO.SetActive(true);
        if (loserIndex == GameCore.Instance.playerManager.my_index)
        {
            Winner_Label.text = "You Lose";
        }
        else
        {
            Winner_Label.text = "You Win";
        }
    }
}
