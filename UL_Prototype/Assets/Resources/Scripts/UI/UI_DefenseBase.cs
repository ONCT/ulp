﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UI_DefenseBase : UI_CardBase
{

    // Use this for initialization
    void Start()
    {
        cardArea = CardArea.Defense;
    }

    public override void OnClickUseCard()
    {
        if (can_useCard == false) { return; }
        GameCore.Instance.playerBehavior.UseCard(CardArea.Defense);
        List<CardData> tmp = new List<CardData>();
        tmp.AddRange(cards);
        base.OnClickUseCard();
        GameCore.Instance.battleManager.Send_Defense(tmp);
    }

    public override void CheckArgs()
    {
        base.CheckArgs();
        text_num = GameCore.Instance.battleManager.CheckDefenseNum(cards, GameCore.Instance.my_playerData.my_character);
        if (GameCore.Instance.phaseController.battlePhase == BattlePhase.Defense)
        {
            UI_Manager.Instance.UI_stateBar.battleMiddleInfo.SetMyDef(text_num);
        }
        button_text.text = "DEF\n" + text_num.ToString();
    }

}
