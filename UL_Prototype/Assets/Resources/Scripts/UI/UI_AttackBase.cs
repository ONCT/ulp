﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_AttackBase : UI_CardBase
{
    public Image backImage;
    // Use this for initialization
    void Start()
    {
        cardArea = CardArea.Attack;
    }

    public override void Show()
    {
        gameObject.SetActive(true);
        if (GameCore.Instance.my_playerData == null) { return; }
        if (GameCore.Instance.my_playerData.range == RangeType.middleRange || GameCore.Instance.my_playerData.range == RangeType.longRange)
        {
            backImage.color = Color.green;
        }
        else
        {
            backImage.color = Color.red;
        }
    }

    public override void OnClickUseCard()
    {
        if (/*cards.Count == 0 || */can_useCard == false) { return; }
        GameCore.Instance.playerBehavior.UseCard(CardArea.Attack);
        List<CardData> tmp = new List<CardData>();
        tmp.AddRange(cards);
        base.OnClickUseCard();
        GameCore.Instance.battleManager.Send_Attack(tmp);
    }

    public override void CheckArgs()
    {
        base.CheckArgs();
        text_num = GameCore.Instance.battleManager.CheckAttackNum(cards, GameCore.Instance.my_playerData.my_character);
        if (GameCore.Instance.phaseController.battlePhase == BattlePhase.Attack)
        {
            UI_Manager.Instance.UI_stateBar.battleMiddleInfo.SetMyAtk(text_num);
        }
        button_text.text = "ATK\n" + text_num.ToString();
    }
}
