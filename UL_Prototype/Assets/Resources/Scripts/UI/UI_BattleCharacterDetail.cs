﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BattleCharacterDetail : MonoBehaviour {

    public Transform target;

    public string args;
    public Text name_text;
    public Text hp_text;
    public Text atk_text;
    public Text def_text;
    public List<UI_SkillObject> UI_SOs = new List<UI_SkillObject>();

    DataBase DB_Char;

    public void Initial(string args)
    {
        DB_Char = DataBaseManager.Instance.DB_CharcterData;
        this.args = args;
        hp_text.text = DB_Char.GetValueByInt(args, "HP").ToString();
        atk_text.text = DB_Char.GetValueByInt(args, "ATK").ToString();
        def_text.text = DB_Char.GetValueByInt(args, "DEF").ToString();
        name_text.text = DataBaseManager.Instance.DB_String.GetValueByString(args, "word");

        int i = 0;
        for (i = 0; i < GameDefinition.Instance.CHAR_SKILL_NUM; i++)
        {
            string skill_args = DB_Char.GetValueByString(args, "Skill_" + (i + 1));
            UI_SOs[i].Initial(skill_args);
            UI_SOs[i].gameObject.SetActive(true);
        }
        for (int j = i; j < UI_SOs.Count; j++)
        {
            UI_SOs[j].gameObject.SetActive(false);
        }

    }

    public void Initial(Character character)
    {
        this.args = character.hero_name;
        hp_text.text = character.Now_HP.ToString();
        if (character.Now_HP < character.Max_HP)
        {
            hp_text.color = Color.red;
        }
        else
        {
            hp_text.color = Color.black;
        }
        atk_text.text = character.ATK.ToString();
        def_text.text = character.DEF.ToString();
        name_text.text = DataBaseManager.Instance.DB_String.GetValueByString(args, "word");

        for (int i = 0; i < UI_SOs.Count; i++)
        {
            string skill_args = DB_Char.GetValueByString(args, "Skill_" + (i + 1));
            UI_SOs[i].Initial(skill_args);
        }
    }

    public void OnClick()
    {
        //UI_SCP.OnSelectCharacter(args);
        target.transform.SendMessage("OnSelectCharacter", args);
    }
}
