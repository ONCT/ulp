﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas))]
[RequireComponent(typeof(CanvasScaler))]
public class CanvasAutoAdjust : MonoBehaviour {

	public bool adjustOnAwake = true;

	private void Awake() {

		if(adjustOnAwake)
		{
			adjust();
		}
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void adjust()
	{
		Canvas c = GetComponent<Canvas>();

		if(c.renderMode == RenderMode.WorldSpace)
		{
			CanvasScaler cs = GetComponent<CanvasScaler> ();
			float nativeResolution = cs.referenceResolution.x / cs.referenceResolution.y;
			float targetResolution = (float)Screen.width / (float)Screen.height;
			if (targetResolution > nativeResolution) {
				// wider, let it be
			} else if (targetResolution < nativeResolution) {
				// higher, shrink Y scale
				transform.localScale = transform.localScale*(targetResolution/nativeResolution);
			}
		}
		else if(c.renderMode == RenderMode.ScreenSpaceOverlay)
		{
			CanvasScaler cs = GetComponent<CanvasScaler> ();
			float nativeResolution = cs.referenceResolution.x / cs.referenceResolution.y;
			float targetResolution = (float)Screen.width / (float)Screen.height;
			if (targetResolution > nativeResolution) {
				// wider, match height
				cs.matchWidthOrHeight = 1f;

			} else if (targetResolution < nativeResolution) {
				// higher, shrink Y scale
				cs.matchWidthOrHeight = 0f;
			}
		}
	}
}
