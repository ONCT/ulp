﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Hurt : MonoBehaviour
{
    public Text text;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(WaitAndDead());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator WaitAndDead()
    {
        yield return new WaitForSeconds(1.5f);
        StopCoroutine(WaitAndDead());
        Destroy(gameObject);
    }
}
