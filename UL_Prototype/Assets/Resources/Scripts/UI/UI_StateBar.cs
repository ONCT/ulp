﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_StateBar : MonoBehaviour
{
    public UI_MyStateBar myStateBar;
    public UI_EnemyStateBar enemyStateBar;
    public UI_BattleMiddleInfo battleMiddleInfo;

    public Text en_name_text;
    public GameObject en_char_OB;
    public Text en_HP_text;

    public GameObject hurt_OB;
    public GameObject buff_OB;
    public List<UI_Buff> my_list_buff = new List<UI_Buff>();
    public List<UI_Buff> en_list_buff = new List<UI_Buff>();

    //public List<UI_BattleCharacterDetail> my_back_char = new List<UI_BattleCharacterDetail>();
    public List<UI_BattleCharacterDetail> en_back_char = new List<UI_BattleCharacterDetail>();

    public void SetCharacterName(Character character)
    {
        if (character.ownerIndex == GameCore.Instance.my_playerData.My_index)
        {
            myStateBar.SetCharacterName(character);
        }
        else
        {
            int k = 0;
            en_name_text.text = DataBaseManager.Instance.DB_String.GetValueByString(character.hero_name, "word");
            en_char_OB = character.gameObject;
            for (int i = 0; i < GameCore.Instance.my_playerData.enemy_charList.Count; i++)
            {
                if (GameCore.Instance.my_playerData.enemy_charList[i] == character) { continue; }
                en_back_char[k].Initial(GameCore.Instance.my_playerData.enemy_charList[i]);
                k++;
            }

        }
        foreach (SkillBuff SB in character.all_buffs)
        {
            SetGetBuff(character, SB);
        }
    }

    public void ResetArgs()
    {
        //名稱跟後排的資訊
        Character character = GameCore.Instance.my_playerData.my_character;
        SetCharacterName(character);

        character = GameCore.Instance.my_playerData.enemy_character;
        SetCharacterName(character);

        //my_HP_text.text = GameCore.Instance.my_playerData.my_character.Now_HP.ToString();
        myStateBar.ResetArgs();
        en_HP_text.text = GameCore.Instance.my_playerData.enemy_character.Now_HP.ToString();


        var tmp = my_list_buff;
        my_list_buff = en_list_buff;
        en_list_buff = tmp;

        if (GameCore.Instance.isFake)
        {
            ClearBuffs(GameCore.Instance.my_playerData.My_index);
            ClearBuffs(GameCore.Instance.enemy_playerData.My_index);

            SetGetBuff(GameCore.Instance.my_playerData.my_character);
            SetGetBuff(GameCore.Instance.my_playerData.enemy_character);
        }
    }

    public void SetGetHurt(int index, int num)
    {
        GameObject clone = Instantiate(hurt_OB);
        clone.transform.SetParent(transform);
        UI_Hurt ui = clone.GetComponent<UI_Hurt>();
        if (num < 0) { num = 0; }
        ui.text.text = "- " + num.ToString();

        Vector3 v3;
        if (index == GameCore.Instance.playerManager.my_index)
        {
            //v3 = Camera.main.WorldToScreenPoint(my_char_OB.transform.position);
        }
        else
        {
            //v3 = Camera.main.WorldToScreenPoint(en_char_OB.transform.position);
        }
        //clone.transform.position = v3;
    }

    public void SetGetBuff(Character character, SkillBuff skillBuff)
    {
        GameObject clone = Instantiate(buff_OB);
        clone.transform.SetParent(transform);
        UI_Buff ui = clone.GetComponent<UI_Buff>();
        ui.Initial(skillBuff);

        Vector3 v3;
        if (character.ownerIndex == GameCore.Instance.playerManager.my_index)
        {
            //v3 = my_HP_text.transform.position + new Vector3(-15, 100 * (my_list_buff.Count + 1));
            //clone.transform.SetParent();
            my_list_buff.Add(ui);
        }
        else
        {
            //v3 = en_HP_text.transform.position + new Vector3(-15, 100 * (en_list_buff.Count + 1));
            en_list_buff.Add(ui);
        }
        //clone.transform.position = v3;
    }

    public void SetGetBuff(Character character)
    {
        for (int i = 0; i < character.all_buffs.Count; i++)
        {
            SetGetBuff(character, character.all_buffs[i]);
        }
    }

    public void ClearBuff(UI_Buff buff)
    {
        if (my_list_buff.Contains(buff))
        {
            my_list_buff.Remove(buff);
            Destroy(buff.buff_ob);
        }
        else if (en_list_buff.Contains(buff))
        {
            en_list_buff.Remove(buff);
            Destroy(buff.buff_ob);
        }

        if (buff == null) { Debug.LogWarning("Something Wrong"); }

    }

    public void ClearBuffs(int index)
    {
        if (index == GameCore.Instance.playerManager.my_index)
        {
            int count = my_list_buff.Count;
            for (int i = 0; i < count; i++)
            {
                Destroy(my_list_buff[i].buff_ob);
            }
            my_list_buff.Clear();
        }
        else
        {
            int count = en_list_buff.Count;
            for (int i = 0; i < count; i++)
            {
                Destroy(en_list_buff[i].buff_ob);
            }
            en_list_buff.Clear();
        }
    }

    public void ResetBuff()
    {
        int i = 0;
        for (i = 0; i < GameCore.Instance.my_playerData.my_character.all_buffs.Count; i++)
        {
            my_list_buff[i].Initial(GameCore.Instance.my_playerData.my_character.all_buffs[i]);
        }
        for (int j = i; j < my_list_buff.Count; j++)
        {
            Destroy(my_list_buff[j].buff_ob);
        }

        i = 0;
        for (i = 0; i < GameCore.Instance.my_playerData.enemy_character.all_buffs.Count; i++)
        {
            en_list_buff[i].Initial(GameCore.Instance.my_playerData.enemy_character.all_buffs[i]);
        }
        for (int j = i; j < en_list_buff.Count; j++)
        {
            Destroy(en_list_buff[j].buff_ob);
        }
    }

    public GameObject enemy_skill_OB;
    public Text enemy_skill_text;
    public GameObject my_skill_OB;
    public Text my_skill_text;

    public void SetSkillPanel(Character character, string word, bool bo)
    {
        if (character.ownerIndex == GameCore.Instance.playerManager.my_index)
        {
            my_skill_text.text = word;
            my_skill_OB.SetActive(bo);
        }
        else
        {
            enemy_skill_text.text = word;
            enemy_skill_OB.SetActive(bo);
        }
    }
}
