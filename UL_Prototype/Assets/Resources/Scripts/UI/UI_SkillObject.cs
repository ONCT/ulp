﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UI_SkillObject : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public UI_SkillBar UI_skillBar;
    public string args;
    public Image image;
    public Text text;
    DataBase DB_String;
    DataBase DB_Skill;
    public List<GameObject> condition_GO;
    public List<Image> condition_image;
    public List<Text> condition_text;
    public Image back_image;
    public Image success_image;

    public void Initial(string args)
    {
        DB_String = DataBaseManager.Instance.DB_String;
        DB_Skill = DataBaseManager.Instance.DB_Skill;
        this.args = args;

        var cardArea = DB_Skill.GetValueByString(args, "card_area");
        success_image.material = Resources.Load<Material>("Material/UI_PureAlpha");
        //switch (cardArea)
        //{
        //    case "Attack":
        //        back_image.material = Resources.Load<Material>("Material/UI_PureRed");
        //        break;
        //    case "Defense":
        //        back_image.material = Resources.Load<Material>("Material/UI_PureBlue");
        //        break;
        //    case "Move":
        //        back_image.material = Resources.Load<Material>("Material/UI_PurePurple");
        //        break;
        //}

        if (args != "")
        {
            text.text = DB_String.GetValueByString(args, "word");
            image.material = UI_skillBar.Gray;
        }
        else
        {
            text.text = "***";
            image.material = UI_skillBar.Gray;
        }
        for (int i = 0; i < 3; i++)
        {
            string tmp = DB_Skill.GetValueByString(args, "condition_" + (i + 1));
            if (tmp == "0" || tmp == null)
            {
                condition_GO[i].SetActive(false);
            }
            else
            {
                condition_GO[i].SetActive(true);
                switch (tmp)
                {
                    case "gun":
                        condition_image[i].material = Resources.Load<Material>("Material/UI_PureGreen");
                        break;
                    case "sword":
                        condition_image[i].material = Resources.Load<Material>("Material/UI_PureRed");
                        break;
                    case "shield":
                        condition_image[i].material = Resources.Load<Material>("Material/UI_PureBlue");
                        break;
                    case "move":
                        condition_image[i].material = Resources.Load<Material>("Material/UI_PurePurple");
                        break;
                    case "special":
                        condition_image[i].material = Resources.Load<Material>("Material/UI_PureYellow");
                        break;
                    case "none":
                        condition_image[i].material = Resources.Load<Material>("Material/UI_PureWhite");
                        break;
                    default:
                        Debug.LogWarning("Something Wrong , key: " + tmp);
                        break;
                }
                tmp = DB_Skill.GetValueByString(args, "require_" + (i + 1));
                string numText = DB_Skill.GetValueByInt(args, "require_num_" + (i + 1)).ToString();
                switch (tmp)
                {
                    case "more":
                        numText += " ↑";
                        break;
                    case "less":
                        numText += " ↓";
                        break;
                    case "equal":
                        numText += " =";
                        break;
                }
                condition_text[i].text = numText;
            }
        }
    }

    public void SetColor(Material ma)
    {
        //transform.GetComponent<Image>().material = ma;
        success_image.material = ma;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        UI_skillBar.ShowDetail(args, true, transform.position);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        UI_skillBar.ShowDetail("", false, transform.position);
    }
}
