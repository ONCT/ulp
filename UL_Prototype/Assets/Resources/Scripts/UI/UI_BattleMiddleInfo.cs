﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BattleMiddleInfo : MonoBehaviour {

    public GameObject atk_def_panel;
    public Image my_num_back;
    public Text my_num_text;
    public Image en_num_back;
    public Text en_num_text;

    private Color32 color_atk_short = Color.red;
    private Color32 color_atk_m_l = Color.green;
    private Color32 color_def = new Color32(74, 200, 241, 255);

    public void SetATKandDEFBar(BattlePhase phase)
    {

        switch (phase)
        {
            case BattlePhase.Prepare:
            case BattlePhase.Draw:
            case BattlePhase.Wait:
            case BattlePhase.End:
            case BattlePhase.Change:
                atk_def_panel.SetActive(false);
                break;
            case BattlePhase.Move:
                atk_def_panel.SetActive(false);
                //my_num_back.color = new Color32(191,80,167,255);
                //en_num_back.color = new Color32(191, 80, 167, 255);
                break;
            case BattlePhase.Attack:
                switch (GameCore.Instance.my_playerData.range)
                {
                    case RangeType.shortRange:
                        my_num_back.color = color_atk_short;
                        en_num_back.color = Color.white;
                        break;
                    case RangeType.middleRange:
                    case RangeType.longRange:
                        my_num_back.color = color_atk_m_l;
                        en_num_back.color = Color.white;
                        break;
                }
                atk_def_panel.SetActive(true);
                break;
            case BattlePhase.Defense:
                my_num_back.color = color_def;
                atk_def_panel.SetActive(true);
                break;
            default:
                break;
        }
    }

    public void SetMyAtk(int num)
    {
        my_num_text.text = "x " + num.ToString();
        //en_num_back.color = Color.white;
    }

    public void SetMyDef(int num)
    {
        Debug.Log("SetMyDef");
        my_num_text.text = "x " + num.ToString();
    }

    public void SetEnemyAtk(int num)
    {
        switch (GameCore.Instance.my_playerData.range)
        {
            case RangeType.shortRange:
                en_num_back.color = color_atk_short;
                break;
            case RangeType.middleRange:
            case RangeType.longRange:
                en_num_back.color = color_atk_m_l;
                break;
        }
        en_num_text.text = "x " + num.ToString();
    }

    public void SetEnemyDef(int num)
    {
        //my_num_back.color = color_def;
        en_num_text.text = "x " + num.ToString();
    }

}
