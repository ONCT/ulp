﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_EnemyHand : UI_Base {

    Action callBack;
    public UI_CardBase UI_enHand;
    public UI_UsedCardBase UI_EnUsedCard;

    public void UsedCardToTrashcan()
    {
        UI_EnUsedCard.AllToTrashCan();
    }

    public void HandToUsedandOpen(List<CardData> cards, Action callBack = null)
    {
        Debug.Log("HandToUsedandOpen");
        this.callBack = callBack;
        StartCoroutine(cor_HandToUsed(cards, true));
    }

    public void OpenAllUsed(Action callBack = null)
    {
        Debug.Log("OpenAllUsed, card count : " + UI_EnUsedCard.cards.Count);
        this.callBack = callBack;
        StartCoroutine(cor_OpenUsedCard());
    }

    IEnumerator cor_HandToUsed(List<CardData> cards, bool isOpen = false)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            UI_enHand.RemoveCard(cards[i]);
            UI_EnUsedCard.AddCard(cards[i]);
            yield return new WaitForSeconds(0.1f);
        }

        yield return new WaitForSeconds(1.0f);

        if (isOpen == true)
        {
            StartCoroutine(cor_OpenUsedCard());
        }
        else
        {
            CallBack();
        }
    }

    IEnumerator cor_OpenUsedCard()
    {
        for (int i = 0; i < UI_EnUsedCard.cards.Count; i++)
        {
            yield return new WaitForSeconds(0.1f);

            UI_EnUsedCard.cards[i].TurnToFront();
        }

        yield return new WaitForSeconds(1.0f);

        CallBack();
    }

    private void CallBack()
    {
        callBack();
    }
}
