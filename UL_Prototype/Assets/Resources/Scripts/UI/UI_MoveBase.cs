﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum MoveType
{
    None,
    Back,
    Rest,
    Change,
    Forward,
}

public class UI_MoveBase : UI_CardBase
{
    public Text moveBackButton_text;
    public Text changeButton_text;
    public MoveType moveType;

    // Use this for initialization
    void Start()
    {
        cardArea = CardArea.Move;
    }

    public override void OnClickUseCard()
    {
        //GameCore.Instance.playerBehavior.UseCard(CardArea.Move);
        //GameCore.Instance.battleManager.Send_Move(cards, moveType);
        base.OnClickUseCard();
    }

    public void OnClickUseCardWithDirection(string str)
    {
        if (/*cards.Count == 0 || */can_useCard == false) { return; }
        List<CardData> tmp = new List<CardData>();
        tmp.AddRange(cards);
        switch (str)
        {
            case "forward":
                GameCore.Instance.playerBehavior.UseCard(CardArea.Move);
                OnClickUseCard();
                GameCore.Instance.battleManager.Send_Move(tmp, MoveType.Forward);
                break;
            case "change":
                GameCore.Instance.playerBehavior.UseCard(CardArea.Move);
                OnClickUseCard();
                GameCore.Instance.battleManager.Send_Move(tmp, MoveType.Change);
                //GameCore.Instance.UI_manager.UI_changeCharacterPanel.TurnOnOff(true);
                break;
            case "back":
                GameCore.Instance.playerBehavior.UseCard(CardArea.Move);
                OnClickUseCard();
                GameCore.Instance.battleManager.Send_Move(tmp, MoveType.Back);
                break;
        }

    }

    public override void CheckArgs()
    {
        base.CheckArgs();
        text_num = GameCore.Instance.battleManager.CheckMoveNum(cards, GameCore.Instance.my_playerData.my_character);
        #region ---暫時廢棄--
        button_text.text = "MOV\n" + text_num.ToString();
        #endregion
        #region ---暫時用---
        button_text.text = "前進\n" + text_num.ToString();
        changeButton_text.text = "換角";
        moveBackButton_text.text = "後退\n" + text_num.ToString();
        #endregion
    }

    public void OnClickMove()
    {
        switch (moveType)
        {
            case MoveType.None:
            case MoveType.Back:
                //moveType = MoveType.rest;
                //moveButton_text.text = "休息";
                moveType = MoveType.Forward;
                moveBackButton_text.text = "前進";
                break;
            //case MoveType.rest:
            //    moveType = MoveType.change;
            //    moveButton_text.text = "換角";
            //    break;
            //case MoveType.change:
            //    moveType = MoveType.forward;
            //    moveButton_text.text = "前進";
            //    break;
            case MoveType.Forward:
                moveType = MoveType.Back;
                moveBackButton_text.text = "後退";
                break;
        }
    }
}
