﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SkillDetail : MonoBehaviour
{

    public Text text;
    DataBase DB_String;
    DataBase DB_Skill;
    public string word;

    public void Initial()
    {
        DB_String = DataBaseManager.Instance.DB_String;
        DB_Skill = DataBaseManager.Instance.DB_Skill;
    }

    public void SetArgs(string args, bool bo, Vector3 pos)
    {
        gameObject.SetActive(bo);
        transform.position = pos;

        if (bo == true)
        {
            word = DB_String.GetValueByString(DB_Skill.GetValueByString(args, "card_area"), "word");
            word += "\n";
            word += DB_String.GetValueByString(DB_Skill.GetValueByString(args, "range"), "word");
            word += "\n";

            for (int i = 1; i <= 3; i++)
            {
                string con = DB_String.GetValueByString(DB_Skill.GetValueByString(args, "condition_" + i), "word");
                if (con == "0" || con == "" || con == null) { continue; }
                string req = DB_Skill.GetValueByString(args, "require_" + i);
                int req_num = DB_Skill.GetValueByInt(args, "require_num_" + i);

                word += con;

                switch (req)
                {
                    case "more":
                    case "less":
                        word += "合計";
                        break;
                }
                word += req_num.ToString();
                switch (req)
                {
                    case "more":
                        word += "以上";
                        break;
                    case "less":
                        word += "以下";
                        break;
                }
                word += "\n";
            }

            word += "<color=yellow>";
            string tmp = DB_Skill.GetValueByString(args, "card_area");
            if (DB_Skill.GetValueByInt(args, "number") != 0)
            {
                switch (tmp)
                {
                    case "Attack":
                        word += "攻擊力 + ";
                        break;
                    case "Defense":
                        word += "防禦力 + ";
                        break;
                    case "Move":
                        word += "移動力 + ";
                        break;
                }
                word += DB_Skill.GetValueByInt(args, "number").ToString();
                word += "\n";
            }
            //word += DB_String.GetValueByString(DB_Skill.GetValueByString(args, "descirbe_word"), "word");
            word += DB_String.GetValueByString(DB_Skill.GetValueByString(args, "descirbe_word"), "word");
            word += "</color>";
            text.text = word;
        }
    }
}
