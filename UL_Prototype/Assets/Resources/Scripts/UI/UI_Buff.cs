﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Buff : MonoBehaviour
{
    public Text text;
    public GameObject buff_ob;

    public void Initial(SkillBuff skillBuff)
    {
        string str = string.Empty;
        str += skillBuff.effectType.ToString();
        switch (skillBuff.effectType)
        {
            case EffectType.Atk_up:
            case EffectType.Def_up:
            case EffectType.Move_up:
                str += "+";
                str += skillBuff.effectNum.ToString();
                break;
        }
        str += "(";
        str += skillBuff.remainCount.ToString();
        str += ")";
        text.text = str;
        //text.text = skillBuff.effectType.ToString() + "+" + skillBuff.effectNum + "x" + skillBuff.remainCount.ToString();
        buff_ob = gameObject;
        //StartCoroutine(WaitAndDead(skillBuff.RemainTime));
    }

    IEnumerator WaitAndDead(float f)
    {
        yield return new WaitForSeconds(f);

        GameCore.Instance.UI_manager.UI_stateBar.ClearBuff(this);
    }
}
