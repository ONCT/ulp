﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_SelectCardPanel : MonoBehaviour
{
    public MontageFlag montageFlag;
    public CardData selectedCard;
    DataBase DB_EventCard;
    public GameObject card_prefab;
    public GameObject arrayPoint;
    public GameObject deckPoint;
    public List<CardData> eventCards = new List<CardData>();
    public GameObject gameCore;
    public List<CardData> target_SelectedCards = new List<CardData>();
    public List<CardData> Selected_Cards = new List<CardData>();
    public List<CardData> fake_SelectedCards = new List<CardData>();
    public int count = 1;
    public List<List<string>> defaultCards = new List<List<string>>();
    bool isAddCard = true;
    Action OnClickCard;
    public Text text_AddorDel;
    public bool isFake;

    // Use this for initialization
    void Start()
    {
        DB_EventCard = DataBaseManager.Instance.DB_EventCardData;
        List<string> keys = DB_EventCard.GetKeyList();

        for (int i = 0; i < keys.Count; i++)
        {
            GameObject clone = Instantiate(card_prefab);
            CardData CD = clone.GetComponent<CardData>();
            clone.transform.SetParent(arrayPoint.transform);
            clone.transform.localPosition = new Vector2(225 * (i / 2), -250 * (i % 2));
            clone.transform.localScale = Vector3.one;
            CD.Initial(keys[i]);
            eventCards.Add(CD);
        }
        var PB = gameCore.GetComponent<PlayerBehavior>();
        PB.enabled = false;
        OnClickCard = ClickAddCard;
        defaultCards.Add(de1);
        defaultCards.Add(de2);
        defaultCards.Add(de3);
        defaultCards.Add(de4);
        defaultCards.Add(de5);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnClickCard();
        }
    }

    //Vector2 mouse_pos;
    public List<RaycastResult> hitObjects = new List<RaycastResult>();

    void ClickAddCard()
    {
        var pointer = new PointerEventData(EventSystem.current);
        pointer.position = Input.mousePosition;

        EventSystem.current.RaycastAll(pointer, hitObjects);
        if (hitObjects.Count <= 0)
        {
            return;
        }
        else
        {
            foreach (RaycastResult RR in hitObjects)
            {
                selectedCard = RR.gameObject.transform.GetComponent<CardData>();
                if (selectedCard != null)
                {
                    break;
                }
            }
        }
        if (selectedCard == null) { return; }
        AddCard(selectedCard.args);
    }

    void ClickRemoveCard()
    {
        var pointer = new PointerEventData(EventSystem.current);
        pointer.position = Input.mousePosition;

        EventSystem.current.RaycastAll(pointer, hitObjects);
        if (hitObjects.Count <= 0)
        {
            return;
        }
        else
        {
            foreach (RaycastResult RR in hitObjects)
            {
                selectedCard = RR.gameObject.transform.GetComponent<CardData>();
                if (selectedCard != null)
                {
                    break;
                }
            }
        }
        if (selectedCard == null) { return; }
        RemoveCard(selectedCard.card_index);
    }

    void AddCard(string str)
    {
        if (target_SelectedCards.Count >= 15) { return; }
        GameObject clone = Instantiate(card_prefab);
        CardData CD = clone.GetComponent<CardData>();
        clone.transform.SetParent(deckPoint.transform);
        clone.transform.localScale = Vector3.one;
        CD.Initial(str);
        target_SelectedCards.Add(CD);
        SetCardPos();
    }

    void RemoveCard(int index)
    {
        if (index == 0) { return; }
        Destroy(target_SelectedCards[index - 1].gameObject);
        target_SelectedCards.RemoveAt(index - 1);
        SetCardPos();
    }

    void SetCardPos()
    {
        for (int i = 0; i < target_SelectedCards.Count; i++)
        {
            target_SelectedCards[i].transform.localPosition = new Vector2(160 * (i % 5), -210 * (i / 5));

            target_SelectedCards[i].card_index = (i + 1);
        }
    }

    public void OnClickDefaultCard(int num)
    {
        int j = target_SelectedCards.Count;
        for (int i = 0; i < j; i++)
        {
            RemoveCard(1);
        }
        for (int i = 0; i < defaultCards[num].Count; i++)
        {
            AddCard(defaultCards[num][i]);
        }
    }

    public void OnClickEnter()
    {
        string args = string.Empty;
        foreach (CardData card in target_SelectedCards)
        {
            args += card.args + " ";
        }
        switch (isFake)
        {
            case false:
                Selected_Cards = target_SelectedCards;
                GameCore.Instance.cardManager.AddEventCards(args);
                GameCore.Instance.playerManager.Send_EventCard(args);
                break;
            case true:
                fake_SelectedCards = target_SelectedCards;
                GameCore.Instance.cardManager.Online_AddEventCards(args);
                break;
        }
        int j = target_SelectedCards.Count;
        for (int i = 0; i < j; i++)
        {
            RemoveCard(1);
        }
        TurnOnOff(false, isFake);
    }

    public void TurnOnOff(bool bo, bool isFake)
    {
        this.isFake = isFake;
        switch (isFake)
        {
            case true:
                target_SelectedCards = fake_SelectedCards;
                break;
            case false:
                target_SelectedCards = Selected_Cards;
                break;
        }
        switch (bo)
        {
            case true:
                gameCore.GetComponent<PlayerBehavior>().enabled = false;
                break;
            case false:
                gameCore.GetComponent<PlayerBehavior>().enabled = true;
                break;
        }
        for (int i = 0; i < eventCards.Count; i++)
        {
            eventCards[i].transform.localPosition = new Vector2(225 * (i / 2), -250 * (i % 2));
        }
        gameObject.SetActive(bo);
        SetCardPos();
        GameCore.Instance.playerBehavior.montageFlag = MontageFlag.none;
    }

    public void OnClick_AddOrDel()
    {
        bool bo = !isAddCard;

        switch (bo)
        {
            case true:
                OnClickCard = ClickAddCard;
                text_AddorDel.text = "移除";
                break;
            case false:
                OnClickCard = ClickRemoveCard;
                text_AddorDel.text = "加入";
                break;
        }
        isAddCard = bo;
    }

    List<string> de1 = new List<string> { "card_16", "card_16", "card_16", "card_16", "card_16",
                                          "card_16","card_16","card_16","card_16","card_16",
                                          "card_16","card_16","card_16","card_16","card_16"};

    List<string> de2 = new List<string> { "card_17", "card_17", "card_17", "card_17", "card_17",
                                          "card_17","card_17","card_17","card_17","card_17",
                                          "card_17","card_17","card_17","card_17","card_17"};

    List<string> de3 = new List<string> { "card_22", "card_22", "card_22", "card_22", "card_22",
                                          "card_22","card_22","card_22","card_22","card_22",
                                          "card_22","card_22","card_22","card_22","card_22"};

    List<string> de4 = new List<string> { "card_47", "card_47", "card_47", "card_47", "card_47",
                                          "card_47","card_47","card_47","card_47","card_47",
                                          "card_47","card_47","card_47","card_47","card_47"};

    List<string> de5 = new List<string> { "card_3", "card_3", "card_3", "card_3", "card_3",
                                          "card_3","card_3","card_3","card_3","card_3",
                                          "card_3","card_3","card_3","card_3","card_3"};
}
