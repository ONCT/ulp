﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_WizzardPanel : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public bool OnOff;
    public void TurnOnOff()
    {
        switch (OnOff)
        {
            case true:
                OnOff = false;
                break;
            case false:
                OnOff = true;
                break;
        }
        gameObject.SetActive(OnOff);
    }

    public void OnClick(Text text)
    {
        Debug.Log(text.text);
    }
}
