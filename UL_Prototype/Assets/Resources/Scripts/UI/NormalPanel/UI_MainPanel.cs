﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_MainPanel : UI_PanelBase
{

    public GameObject waiting_OB;
    public bool isFake = false;

    public void OnClick_Start()
    {
        if (UI_Manager.Instance.selectCharacterPanel.heros.Count >= 3 && isFake == false)
        {
            GameCore.Instance.my_playerData.SetHero(UI_Manager.Instance.selectCharacterPanel.heros);
            SetWaiting(true);
            UI_Manager.Instance.selectCharacterPanel.heros.Clear();
            //count = 0;
        }
        else if (UI_Manager.Instance.selectCharacterPanel.fake_heros.Count >= 3)
        {
            GameCore.Instance.my_playerData.SetHero(UI_Manager.Instance.selectCharacterPanel.heros);
            GameCore.Instance.playerManager.Force_Start(UI_Manager.Instance.selectCharacterPanel.fake_heros);
            isFake = false;
        }
    }

    public void SetWaiting(bool bo)
    {
        waiting_OB.SetActive(bo);
    }

    public void OnClick_SelectionChar()
    {
        Close();
        UI_Manager.Instance.selectCharacterPanel.Show();
    }
}
