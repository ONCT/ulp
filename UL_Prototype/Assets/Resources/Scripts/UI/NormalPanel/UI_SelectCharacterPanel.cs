﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_SelectCharacterPanel : UI_PanelBase
{

    DataBase DB_Char;
    public GameObject detail_OB;
    public List<string> heros = new List<string>() { "char_1", "char_2", "char_3" };
    public List<string> fake_heros = new List<string>() { "char_1", "char_2", "char_3" };
    public GameObject waiting_OB;
    public Transform content;
    List<UI_SelectCharDetail> UI_SC_list = new List<UI_SelectCharDetail>();
    public bool isFake = false;
    public List<UI_SelectCharDetail> selected_heroes = new List<UI_SelectCharDetail>();

    // Use this for initialization
    public override void Initial()
    {
        DB_Char = DataBaseManager.Instance.DB_CharcterData;

        List<string> keys = DB_Char.GetKeyList();
        for (int i = 0; i < keys.Count; i++)
        {
            GameObject clone = Instantiate(detail_OB);
            clone.transform.SetParent(content);
            clone.transform.localPosition = new Vector2(0, 700 - (200 * i));
            clone.transform.localScale = Vector3.one;
            UI_SelectCharDetail UI_SC = clone.GetComponent<UI_SelectCharDetail>();
            UI_SC.Initial(keys[i]);
            UI_SC_list.Add(UI_SC);
        }
        SetSelectedChar();
        detail_OB.SetActive(false);
    }

    public void ResetPanel()
    {
        List<string> keys = DB_Char.GetKeyList();
        for (int i = 0; i < UI_SC_list.Count; i++)
        {
            UI_SC_list[i].Initial(keys[i]);
        }
    }

    public void TurnOnOff(bool bo)
    {
        gameObject.SetActive(bo);
        waiting_OB.SetActive(false);
        heros = new List<string>();
        fake_heros = new List<string>();
    }

    public int count = 0;

    public void OnSelectCharacter(string args)
    {
        if (isFake == true)
        {
            OnSelectFakeCharacter(args);
        }
        else
        {
            OnSelectMyCharacter(args);
        }
    }

    public void OnSelectMyCharacter(string args)
    {
        if (heros.Contains(args) || count >= 3) { return; }
        heros.Add(args);
        count++;
        SetSelectedChar();
    }

    public void OnSelectFakeCharacter(string args)
    {
        if (fake_heros.Contains(args) || fake_heros.Count >= 3) { return; }
        fake_heros.Add(args);
        SetSelectedChar();

        /*
        if (fake_heros.Count >= 3)
        {
            GameCore.Instance.playerManager.Force_Start(fake_heros);
            isFake = false;
        }
        */
    }

    List<string> target_hero;
    private void SetSelectedChar()
    {
        if (isFake == false) { target_hero = heros; }
        else { target_hero = fake_heros; }

        for (int i = 0; i < target_hero.Count; i++)
        {
            selected_heroes[i].Initial(target_hero[i]);
            selected_heroes[i].gameObject.SetActive(true);
        }

        for (int i = target_hero.Count; i < selected_heroes.Count; i++)
        {
            selected_heroes[i].gameObject.SetActive(false);
        }
    }

    public void SetFakeBattle()
    {
        isFake = true;
        SetSelectedChar();
    }

    public void SetWaiting(bool bo)
    {
        waiting_OB.SetActive(bo);
    }

    public void OnClickStart()
    {
        if (heros.Count >= 3 && isFake == false)
        {
            GameCore.Instance.my_playerData.SetHero(heros);
            SetWaiting(true);
            heros.Clear();
            //count = 0;
        }
        else if (fake_heros.Count >= 3)
        {
            GameCore.Instance.playerManager.Force_Start(fake_heros);
            isFake = false;
        }
    }

    public void OnClickSelectCard()
    {
        GameCore.Instance.UI_manager.selectCardPanel.TurnOnOff(true, isFake);
    }

    public void OnClick_Return()
    {
        Close();
        UI_Manager.Instance.mainPanel.Show();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            heros.Clear();
        }
    }
}
