﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_PanelBase : MonoBehaviour {

    public Animator animator;

    public virtual void Initial()
    {

    }

    public virtual void SetData()
    {

    }

    public virtual void Show()
    {
        if (animator != null)
        {
            animator.SetTrigger("Show");
        }
        gameObject.SetActive(true);
    }

    public virtual void Close()
    {
        if (animator != null)
        {
            animator.SetTrigger("Hide");
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public virtual void CloseOver()
    {

    }

}
