﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SkillBar : UI_Base
{

    public List<UI_SkillObject> my_skill_OB;
    public List<UI_SkillObject> en_skill_OB;

    public Material Gray;
    public Material Yellow;
    public Material Alpha;
    public UI_SkillDetail detail_Bar;
    DataBase DB_String;

    public void Initial()
    {
        detail_Bar.Initial();
        DB_String = DataBaseManager.Instance.DB_String;
    }

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < my_skill_OB.Count; i++)
        {
            my_skill_OB[i].Initial("");
        }
        for (int i = 0; i < en_skill_OB.Count; i++)
        {
            en_skill_OB[i].Initial("");
        }
    }

    public void SetMySkillBar(Character character)
    {
        int i = 0;
        for (i = 0; i < character.all_skill.Count; i++)
        {
            my_skill_OB[i].Initial(character.all_skill[i].skill_name);
        }
        for (int k = i; k < my_skill_OB.Count; k++)
        {
            my_skill_OB[k].Initial("");
        }
    }

    public void SetEnSkillBar(Character character)
    {
        int i = 0;
        for (i = 0; i < character.all_skill.Count; i++)
        {
            en_skill_OB[i].Initial(character.all_skill[i].skill_name);
        }
        for (int k = i; k < en_skill_OB.Count; k++)
        {
            en_skill_OB[k].Initial("");
        }
    }

    public void SetSkillColor(int num, bool bo)
    {
        switch (bo)
        {
            case true:
                my_skill_OB[num].SetColor(Yellow);
                break;
            case false:
                my_skill_OB[num].SetColor(Alpha);
                break;
        }
    }

    public void SetEnemySkillColor(int num, bool bo)
    {
        switch (bo)
        {
            case true:
                en_skill_OB[num].SetColor(Yellow);
                break;
            case false:
                en_skill_OB[num].SetColor(Alpha);
                break;
        }
    }

    public void CheckSkill(Character character, List<CardData> cards, CardArea cardArea)
    {
        SkillDetail SD = new SkillDetail();
        SD.cardArea = cardArea;
        SD.atk_cards = cards;
        SD.caster = character;
        SD.range = GameCore.Instance.my_playerData.range;


        for (int i = 0; i < character.all_skill.Count; i++)
        {
            if (character.all_skill[i].isSkillSuccess(SD) == true)
            {
                if (character.all_skill[i].cardArea == cardArea)
                {
                    SetSkillColor(i, true);
                }
            }
            else
            {
                if (character.all_skill[i].cardArea == cardArea)
                {
                    SetSkillColor(i, false);
                }
            }
        }
    }

    public void OnlineCheckSkill(Character character, List<CardData> cards, CardArea cardArea)
    {
        SkillDetail SD = new SkillDetail();
        SD.cardArea = cardArea;
        SD.atk_cards = cards;
        SD.caster = character;
        SD.range = GameCore.Instance.my_playerData.range;

        for (int i = 0; i < character.all_skill.Count; i++)
        {
            //if (character.all_skill[i].isSkillSuccess(SD) == true)
            //{
            //    GameCore.Instance.UI_manager.UI_stateBar.SetSkillPanel(character, DB_String.GetValueByString(character.all_skill[i].skill_name, "word"), true);
            //}
            if (character.all_skill[i].isSkillSuccess(SD) == true)
            {
                if (character.all_skill[i].cardArea == cardArea)
                {
                    SetEnemySkillColor(i, true);
                }
            }
            else
            {
                if (character.all_skill[i].cardArea == cardArea)
                {
                    SetEnemySkillColor(i, false);
                }
            }
        }
    }

    public void ShowDetail(string args, bool bo, Vector3 V3)
    {
        detail_Bar.SetArgs(args, bo, V3);
    }

    private List<SkillEffect> my_skills;
    private List<SkillEffect> en_skills;
    private Action callBack;
    public void AddPrepareShowEffect(List<SkillEffect> my_skills, List<SkillEffect> en_skills, Action callBack)
    {
        this.my_skills = my_skills;
        this.en_skills = en_skills;
        this.callBack = callBack;
    }

    public void ShowSkillPerformance()
    {
        callBack();
    }
}
