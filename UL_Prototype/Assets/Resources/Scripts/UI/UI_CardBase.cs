﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum CardArea
{
    None,
    Hand,
    Attack,
    Defense,
    Move,
}

public class UI_CardBase : UI_Base, IPointerEnterHandler
{

    public List<CardData> cards = new List<CardData>();
    public CardArea cardArea;
    public Transform trans;
    public Text button_text;
    public int text_num = 0;
    public bool can_useCard = true;
    public List<RectTransform> mask = new List<RectTransform>();

    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Close()
    {
        gameObject.SetActive(false);
    }

    public void AddCard(CardData card)
    {
        cards.Add(card);
        card.Card_OB.transform.SetParent(trans);
        card.Card_OB.transform.localScale = Vector3.one;
        Refresh();
    }

    public void AddCards(List< CardData> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            AddCard(cards[i]);
        }
    }

    public void RemoveCard(CardData card)
    {
        cards.Remove(card);
        Refresh();
    }

    public void RemoveCards(List<CardData> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            this.cards.Remove(cards[i]);
        }
        Refresh();
    }

    public void ClearCards()
    {
        cards.Clear();
        Refresh();
    }

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        GameCore.Instance.playerBehavior.GetInCardArea(this);
    }

    public void MoveAllCard()
    {
        for (int i = 0; i < cards.Count; i++)
        {
            Vector2 targetV2 = new Vector2(120 * i, 0);
            cards[i].MoveCard(targetV2);
        }
    }

    public void MoveAllCardImmediately()
    {
        for (int i = 0; i < cards.Count; i++)
        {
            Vector2 targetV2 = new Vector2(120 * i, 0);
            cards[i].MoveToTargetImmediately(targetV2);
        }
    }

    public void SetAllCardsToBack()
    {
        for (int i = 0; i < cards.Count; i++)
        {
            cards[i].SetToCardBack();
        }
    }

    public void SetAllCardsToFront()
    {
        for (int i = 0; i < cards.Count; i++)
        {
            cards[i].TurnAtHalf();
        }
    }

    public virtual void OnClickUseCard()
    {
        if (can_useCard == true)
        {
            can_useCard = false;
            //use_coolDown_now = 0;
            GameCore.Instance.cardManager.UseCard(cards);
            cards.Clear();
        }
    }

    public void Refresh()
    {
        MoveAllCard();
        CheckArgs();
    }

    public virtual void ToTrashCan(CardData card)
    {
        GameCore.Instance.cardManager.ToTrashCan(card);
        Refresh();
    }

    public virtual void AllToTrashCan()
    {
        foreach (CardData card in cards)
        {
            GameCore.Instance.cardManager.ToTrashCan(card);
        }
        ClearCards();
    }

    public virtual void CheckArgs()
    {
        if (GameCore.Instance.my_playerData == null) { return; }
        GameCore.Instance.UI_manager.UI_skillBar.CheckSkill(GameCore.Instance.my_playerData.my_character, cards, cardArea);
        if (can_useCard == false)
        {
            foreach (var v in mask) { v.gameObject.SetActive(true); }
        }
        else
        {
            foreach (var v in mask) { v.gameObject.SetActive(false); }
        }
    }
}
