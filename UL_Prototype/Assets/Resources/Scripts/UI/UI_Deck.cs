﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Deck : MonoBehaviour
{
    public RectTransform top;
    float height;
    public float heightDelta;
    public Text now_cardCount;
    public Text max_cardCount;

    // Use this for initialization
    void Start()
    {
        height = top.sizeDelta.y;
    }

    /*
    // Update is called once per frame
    void Update()
    {
        if (GameCore.Instance.cardManager.can_deal == false) { return; }
        heightDelta = (GameDefinition.Instance.DRAW_CARD_TIME - GameCore.Instance.cardManager.draw_time_now) / GameDefinition.Instance.DRAW_CARD_TIME;
        if (heightDelta > 1) { heightDelta = 1; }
        if (heightDelta < 0) { heightDelta = 0; }
        top.sizeDelta = new Vector2(top.sizeDelta.x, height * heightDelta);

        now_cardCount.text = GameCore.Instance.my_playerData.card_count.ToString();
        max_cardCount.text = (GameDefinition.Instance.MAX_CARD_COUNT + (GameCore.Instance.my_playerData.my_charList.Count - GameCore.Instance.my_playerData.live_hero_count)).ToString();
    }
    */
}
