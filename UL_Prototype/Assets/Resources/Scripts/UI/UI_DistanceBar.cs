﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DistanceBar : MonoBehaviour
{

    public Transform probe;
    public RectTransform rectTranform;
    public const string SHORT_WORD = "近距離";
    public const string MIDDLE_WORD = "中距離";
    public const string LONG_WORD = "遠距離";
    public RangeType rangeType;
    public Text range_text;

    public void SetDistance(RangeType rangeType)
    {
        this.rangeType = rangeType;
        switch (rangeType)
        {
            case RangeType.shortRange:
                range_text.color = Color.red;
                range_text.text = SHORT_WORD;
                break;
            case RangeType.middleRange:
                range_text.color = Color.green;
                range_text.text = MIDDLE_WORD;
                break;
            case RangeType.longRange:
                range_text.color = Color.green;
                range_text.text = LONG_WORD;
                break;
        }
    }

    private Action callBack;
    public void AddNextStep(Action callBack)
    {
        this.callBack = callBack;
    }

    MoveStruct my_move;
    MoveStruct en_move;
    MoveStruct total_move;
    public void AddPrepareMove(MoveStruct my_move, MoveStruct en_move, MoveStruct total_move, Action callBack)
    {
        this.my_move = my_move;
        this.en_move = en_move;
        this.total_move = total_move;
        this.callBack = callBack;
    }

    public void ShowMove()
    {
        GameCore.Instance.battleManager.Normal_Move(total_move);

        callBack();
    }
}
