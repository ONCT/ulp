﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public enum FunType
{
    sword,
    gun,
    shield,
    move,
    special,
    none,
    total,
}

public enum CardType
{
    normalCard,
    eventCard,
}

public class CardData : MonoBehaviour, IPointerDownHandler
{
    public enum Card_Direction
    {
        top,
        bot,
    }

    public string args;
    public int card_index;
    public Card_Direction card_Direction;
    public FunType top_funType;
    public int top_Num;
    public FunType bot_funType;
    public int bot_Num;
    public GameObject Card_OB;
    public CardArea beloneArea;
    public CardType cardType;

    public FunType now_funType;
    public int now_Num;
    DataBase DB_CardData;
    DataBase DB_String;
    public Text card_text;
    public Image image;
    public Sprite sp;
    public Sprite cardBack;
    public Animator anim;

    public void Initial(string args)
    {
        this.args = args;
        DB_CardData = DataBaseManager.Instance.DB_CardData;
        DB_String = DataBaseManager.Instance.DB_String;
        string top_str = DB_CardData.GetValueByString(args, "Top_Fun");
        top_funType = (FunType)Enum.Parse(typeof(FunType), top_str);
        top_Num = DB_CardData.GetValueByInt(args, "Top_Num");
        string bot_str = DB_CardData.GetValueByString(args, "Bot_Fun");
        bot_funType = (FunType)Enum.Parse(typeof(FunType), bot_str);
        bot_Num = DB_CardData.GetValueByInt(args, "Bot_Num");

        now_funType = top_funType;
        now_Num = top_Num;
        //card_text.text = DB_String.GetValueByString(top_str, "word") + " " + top_Num + "\n" + DB_String.GetValueByString(bot_str, "word") + " " + bot_Num;
        card_text.text = "";
        sp = Resources.Load<Sprite>("Material/Card_Picture/" + DB_CardData.GetValueByString(args, "card_image")) as Sprite;
        image.sprite = sp;
    }

    Vector2 old_pos;
    Vector2 new_pos;
    private const float TURN_DISTANCE = 50.0f;

    public void OnClick()
    {
        new_pos = Input.mousePosition;
        float dis = Vector2.Distance(old_pos, new_pos);
        if (dis < TURN_DISTANCE)
        {
            GameCore.Instance.playerBehavior.TurnCard(this);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        old_pos = Input.mousePosition;
        //target.CreateBuilding(function_args);
        GameCore.Instance.playerBehavior.ClickCard(this);
    }

    public void TurnCard()
    {
        switch (card_Direction)
        {
            case Card_Direction.top:
                now_funType = bot_funType;
                now_Num = bot_Num;
                card_Direction = Card_Direction.bot;
                gameObject.transform.Rotate(new Vector3(0, 0, 180));
                break;
            case Card_Direction.bot:
                now_funType = top_funType;
                now_Num = top_Num;
                card_Direction = Card_Direction.top;
                gameObject.transform.Rotate(new Vector3(0, 0, 180));
                break;
        }
    }

    public void ChangeBelone(CardArea cardArea)
    {
        beloneArea = cardArea;
    }

    public void MoveCard(Vector2 target)
    {
        if (gameObject.GetActive())
        {
            StopAllCoroutines();
            StartCoroutine(MoveToTarget(target));
        }
    }

    public void TurnToFront()
    {
        anim.SetTrigger("TurnToFront");
    }

    public void TurnAtHalf()
    {
        //sp = Resources.Load<Sprite>("Material/Card_Picture/" + DB_CardData.GetValueByString(args, "card_image")) as Sprite;
        image.sprite = sp;
    }

    public RectTransform rectTransform;
    public float speed;
    public IEnumerator MoveToTarget(Vector2 target, Action callBack = null)
    {
        Vector2 moveStartPos = rectTransform.anchoredPosition;
        while ((target - moveStartPos).sqrMagnitude > (rectTransform.anchoredPosition - moveStartPos).sqrMagnitude)
        {
            Vector2 move = (target - rectTransform.anchoredPosition) * (Time.deltaTime + speed);
            Vector2 min_move = (target - rectTransform.anchoredPosition).normalized;
            rectTransform.anchoredPosition += (move + min_move);
            yield return null;
        }
        rectTransform.anchoredPosition = target;
        if (rectTransform.anchoredPosition == target)
        {
            StopCoroutine(MoveToTarget(target));
        }

        if (callBack != null)
        {
            callBack.Invoke();
        }
    }

    public void MoveToTargetImmediately(Vector2 target)
    {
        rectTransform.anchoredPosition = target;
    }

    public void SetToCardBack()
    {
        image.sprite = cardBack;
    }

    Action<CardData> callBack;
    public void ToTrashCan(Transform trashcan, Action<CardData> callBack)
    {
        StartCoroutine(MoveToTarget(trashcan.position, MoveDown));
        this.callBack = callBack;
    }

    private void MoveDown()
    {
        callBack(this);
    }
}
