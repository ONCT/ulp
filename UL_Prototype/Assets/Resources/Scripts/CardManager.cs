﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CardManager : MonoBehaviour
{

    DataBase DB_CardData;
    public List<CardData> DeckCards = new List<CardData>();
    public List<CardData> GraveCards = new List<CardData>();
    public List<CardData> All_Cards = new List<CardData>();
    public List<CardData> My_EventCards = new List<CardData>();
    public List<CardData> Enemy_EventCards = new List<CardData>();

    public float draw_time_now;
    public float drawEventCard_time_now;
    public GameObject card_prefab;
    public GameObject garbageCan;
    public Transform cards_parents;
    public Transform deck_trans;
    int k = 0;

    public void Initial()
    {
        DeckCards.Clear();
        GraveCards.Clear();
        All_Cards.Clear();

        DB_CardData = DataBaseManager.Instance.DB_CardData;
        List<string> keys = DB_CardData.GetKeyList();
        List<CardData> tmp = new List<CardData>();
        for (int i = 0; i < keys.Count; i++)
        {
            int count = DB_CardData.GetValueByInt(keys[i], "Count");
            for (int j = 0; j < count; j++)
            {
                GameObject clone = Instantiate(card_prefab);
                CardData CD = clone.GetComponent<CardData>();
                clone.transform.SetParent(deck_trans);
                clone.transform.position = deck_trans.position;
                //clone.transform.localScale = Vector3.one;
                CD.Initial(keys[i]);
                CD.card_index = k;
                CD.cardType = CardType.normalCard;
                k++;
                tmp.Add(CD);
            }
        }
        List<CardData> shuffle = new List<CardData>();
        shuffle = RandomSort(tmp);

        DeckCards.AddRange(shuffle);
        All_Cards.AddRange(shuffle);
        FakeServer.Instance.battleRoom.Initial(shuffle);
    }

    public void AddEventCards(string str)
    {
        foreach (CardData card in My_EventCards)
        {
            Destroy(card.gameObject);
            All_Cards.Remove(card);
        }
        My_EventCards.Clear();
        string[] lines = str.Split(" "[0]);


        List<CardData> tmp = new List<CardData>();
        for (int i = 0; i < lines.Length - 1; i++)
        {
            GameObject clone = Instantiate(card_prefab);
            CardData CD = clone.GetComponent<CardData>();
            clone.transform.SetParent(deck_trans);
            clone.transform.position = deck_trans.position;
            CD.Initial(lines[i]);
            CD.card_index = k;
            CD.cardType = CardType.eventCard;
            k++;
            tmp.Add(CD);
        }
        List<CardData> shuffle = new List<CardData>();
        shuffle = RandomSort(tmp);

        My_EventCards.AddRange(shuffle);
        All_Cards.AddRange(shuffle);
    }

    public void Online_AddEventCards(string str)
    {
        foreach (CardData card in Enemy_EventCards)
        {
            Destroy(card.gameObject);
            All_Cards.Remove(card);
        }
        Enemy_EventCards.Clear();
        string[] lines = str.Split(" "[0]);

        List<CardData> tmp = new List<CardData>();
        for (int i = 0; i < lines.Length - 1; i++)
        {
            GameObject clone = Instantiate(card_prefab);
            CardData CD = clone.GetComponent<CardData>();
            clone.transform.SetParent(deck_trans);
            clone.transform.position = deck_trans.position;
            CD.Initial(lines[i]);
            CD.card_index = k;
            CD.cardType = CardType.eventCard;
            k++;
            tmp.Add(CD);
        }
        List<CardData> shuffle = new List<CardData>();
        shuffle = RandomSort(tmp);

        Enemy_EventCards.AddRange(shuffle);
        All_Cards.AddRange(shuffle);

    }

    public void DrawCard(int num)
    {
        for (int i = 0; i < num; i++)
        {
            DrawCard();
        }
    }

    public void DrawCard()
    {
        CardData card = null;
        switch (GameCore.Instance.playerManager.my_index)
        {
            case 0:
                card = DeckCards[0];
                card.Card_OB.transform.SetParent(cards_parents);

                GameCore.Instance.playerBehavior.DrawCard(card);
                GameCore.Instance.playerManager.Send_DrawCard(card.card_index);

                DeckCards.Remove(card);
                break;
            case 1:
                card = DeckCards[DeckCards.Count - 1];
                card.Card_OB.transform.SetParent(cards_parents);

                GameCore.Instance.playerBehavior.DrawCard(card);
                GameCore.Instance.playerManager.Send_DrawCard(card.card_index);

                DeckCards.Remove(card);
                break;
            default:
                Debug.Log("Something Wrong");
                break;
        }
        if (DeckCards.Count <= 2)
        {
            Shuffle();
        }
    }

    public void DrawEventCard()
    {
        if (GameCore.Instance.my_playerData.My_index == GameCore.Instance.myIndex)
        {
            if (My_EventCards.Count <= 0) { return; }
            CardData card = null;
            card = My_EventCards[0];
            card.Card_OB.transform.SetParent(cards_parents);

            GameCore.Instance.playerBehavior.DrawCard(card);

            My_EventCards.Remove(card);
        }
        else if (GameCore.Instance.isFake && GameCore.Instance.my_playerData.My_index != GameCore.Instance.myIndex)
        {
            if (Enemy_EventCards.Count <= 0) { return; }
            CardData card = null;
            card = Enemy_EventCards[0];
            card.Card_OB.transform.SetParent(cards_parents);

            GameCore.Instance.playerBehavior.DrawCard(card);

            Enemy_EventCards.Remove(card);
        }
    }

    public void Online_DrawCard(int ownerIndex, int card_index)
    {
        CardData card = All_Cards.Find(x => x.card_index == card_index);
        //card.Card_OB.transform.SetParent(cards_parents);

        DeckCards.Remove(card);

        if (DeckCards.Count <= 2)
        {
            Shuffle();
        }
    }

    public void UseCard(List<CardData> cards)
    {
        
        foreach (CardData card in cards)
        {
            //card.Card_OB.transform.SetParent(garbageCan.transform);
            //card.Card_OB.transform.position = deck_trans.position;
            switch (card.cardType)
            {
                case CardType.normalCard:
                    GraveCards.Add(card);
                    break;
                case CardType.eventCard:
                    break;
            }
        }
        foreach (CardData card in cards)
        {
            UI_Manager.Instance.UI_MyUsedCard.AddCard(card);
        }
    }

    public void Online_UseCard(List<CardData> cards)
    {
        foreach (CardData card in cards)
        {
            //card.Card_OB.transform.SetParent(garbageCan.transform);
            //card.Card_OB.transform.position = deck_trans.position;
            switch (card.cardType)
            {
                case CardType.normalCard:
                    GraveCards.Add(card);
                    break;
                case CardType.eventCard:
                    break;
            }
        }
        foreach (CardData card in cards)
        {
            //UI_Manager.Instance.UI_EnUsedCard.AddCard(card);
        }
    }

    private void Shuffle()
    {
        DeckCards.AddRange(GraveCards);
        GraveCards.Clear();
    }

    public void ToTrashCan(CardData card)
    {
        card.ToTrashCan(garbageCan.transform,ToTrashCanDown);
    }

    private void ToTrashCanDown(CardData card)
    {
        card.Card_OB.transform.SetParent(garbageCan.transform);
        card.Card_OB.transform.position = deck_trans.position;
    }

    private Action CallBack;
    public void AddNextStep(Action CallBack)
    {
        this.CallBack = CallBack;
    }

    public void BothUsedCardToTrashcan()
    {
        GameCore.Instance.UI_manager.UI_MyUsedCard.AllToTrashCan();
        GameCore.Instance.UI_manager.enemy_hand.UsedCardToTrashcan();

        CallBack();
    }

    public CardData GetCard(string str)
    {
        int length = str.Length;
        string c_s = str.Substring(0, length - 1);
        string d_s = str.Substring(length - 1, 1);

        int c_i = int.Parse(c_s);
        int d_i = int.Parse(d_s);

        CardData CD = All_Cards.Find(x => x.card_index == c_i);
        if (CD == null) { Debug.Log("Something Wrong"); }

        switch (d_i)
        {
            case 0:
                CD.now_funType = CD.top_funType;
                CD.now_Num = CD.top_Num;
                break;
            case 1:
                CD.now_funType = CD.bot_funType;
                CD.now_Num = CD.bot_Num;
                break;
        }
        return CD;
    }

    public List<CardData> GetCards(string str)
    {
        List<CardData> tmp = new List<CardData>();
        string[] lines = str.Split(" "[0]);
        for (int i = 0; i < lines.Length - 1; i++)
        {
            tmp.Add(GetCard(lines[i]));
        }

        return tmp;
    }

    public string CardsToString(List<CardData> cards)
    {
        string args = string.Empty;
        foreach (CardData card in cards)
        {
            args += card.card_index + "" + (int)card.card_Direction + " ";
        }
        return args;
    }

    List<CardData> RandomSort(List<CardData> cards)
    {
        var random = new System.Random(DateTime.Now.Minute);
        var newList = new List<CardData>();
        foreach (var item in cards)
        {
            newList.Insert(random.Next(newList.Count), item);
        }
        return newList;
    }

    public void ReStart()
    {
        DeckCards.AddRange(GraveCards);
        GraveCards.Clear();
    }

    public void DealDefaultCard()
    {
        for (int i = GameCore.Instance.my_playerData.hand_cards.Count; i < GameDefinition.Instance.DEFALUT_CARD_COUNT; i++)
        {
            DrawCard();
        }
    }
}
